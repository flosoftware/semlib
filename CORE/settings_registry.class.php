<?php
/*! 
 * \file settings_registry.class.php
 */


/// @cond TEST_CLASS_EXIST 
//if (!class_exists('Registry_Exception', false)) {
/// @endcond  
	
/*! 
 * \class Registry_Exception
 * 
 */


	class Registry_Exception extends Exception{
	  public $backtrace;		
	  public function __construct($message, $code = 0) {
	  print "CLASS USAGE ERROR: ";
	  parent::__construct($message, $code);
	  	$this->backtrace = debug_backtrace();
 		 }
	
	public function __toString() {
	   return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
  	}
	 
	public function customFunction() { 
 
   
    }
    

	}
 
 
 
 
/*! 
 * \class Registry
 * @brief Generic singleton class registry for storing 'global' objects/values
 * 
 */		 
 class Registry
{

    private static $registry;

    static function __set($name, $value = null){
        if (is_null($value)) {
            if (isset(self::$registry[$name])) {
                unset(self::$registry[$name]);
            }
        }
        else {
            self::$registry[$name] = $value;
        }
                if (!is_string($name)) {
            throw new Registry_Exception('To register $name must be a string, or null.');
        }


    }

    static function __get($name){
    	    	        if (!array_key_exists($name, self::$registry)) {
           throw new Registry_Exception("Key \"$name\" does not exist in registry.");
        }	
	    	if(!isset(self::$registry[$name])){
	    		return null;
	   		 	}else{
	    		return self::$registry[$name];
    	}

    
    }
        /**
     * Returns TRUE if the $name is a named object in the
     * registry, or FALSE if $name was not found in the registry.
     *
     * @param  string $name
     * @return boolean
     */
    static public function isRegistered($name)
    {
        return isset(self::$registry[$name]);
    }

    
    
} // end of class

//} // end of test if (!class_exists...)
 
?>