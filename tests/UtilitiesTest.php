<?php
require_once dirname ( __DIR__ ) . '/utilities.php';
class UtilitiesTest extends PHPUnit_Framework_TestCase{
	public function testBankHolidays(){
		$bankHols = bank_holidays(2009);
		$expected = array('20090101', '20090410', '20090413', '20090504', '20090525', '20090831', '20091225', '20091228');
		foreach($expected as $hol){
			$this->assertTrue(in_array($hol,$bankHols), $hol.' not found:');
		}
		$bankHols = bank_holidays(2010);
		var_dump($bankHols);
		$expected = array('20100101', '20100402', '20100405', '20100503', '20100531', '20100830', '20101227', '20101228');
		foreach($expected as $hol){
			$this->assertTrue(in_array($hol,$bankHols), $hol.' not found:');
		}
		$bankHols = bank_holidays(2011);
		var_dump($bankHols);
		$expected = array('20110103', '20110422', '20110425', '20110502', '20110530', '20110829', '20111226', '20111227');
		foreach($expected as $hol){
			$this->assertTrue(in_array($hol,$bankHols), $hol.' not found:');
		}
	}

	public function testConvertingRealToWorkingDays(){
		$this->assertEquals(9,real_to_working_days(7,strtotime('20090107')));
		$this->assertEquals(20,real_to_working_days(14,strtotime('20090406')));
	}
}