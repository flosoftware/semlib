<?php
/**
 * AllTests
 *
 * @category Robot
 * @package UnitTests
 * @version $Id: TestSuite.php 396 2009-05-27 15:18:56Z nickp $
 */

/**
 * Test helper
 */
require_once 'TestHelper.php';

class TestSuite extends PHPUnit_Framework_TestSuite
{

	/**
	 * Constructs the test suite handler.
	 */
	public function __construct () {
		$this->setName ( 'TestSuite' );

		$this->addTestSuite( 'UtilitiesTest' );
		$this->addTestSuite( 'Semlib_File_TestSuite' );
		$this->addTestSuite( 'Semlib_DateTimeTest' );
		$this->addTestSuite( 'Semlib_ProcessTest' );
		$this->addTestSuite( 'Semlib_ProcessManagerTest' );
	}

	/**
	 * Creates the suite.
	 */
	public static function suite ()
	{
		return new self ( );
	}

}
?>