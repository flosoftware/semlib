<?php
/**
 * TestHelper
 *
 * @category Robot
 * @package UnitTests
 * @version $Id: TestHelper.php 384 2009-05-20 10:16:17Z nickp $
 */
set_include_path(dirname(dirname(__DIR__)).':'.dirname(dirname(__DIR__)).'/semlib:'.__DIR__.':'.get_include_path());
require_once 'Zend/Loader.php';

class TestHelper {

	public static function main() {
		Zend_Loader::registerAutoload();
	}

}

TestHelper::main ();