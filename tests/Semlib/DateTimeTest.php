<?php

require_once 'Semlib/DateTime.php';

/**
 * Semlib_DateTime test case.
 */
class Semlib_DateTimeTest extends PHPUnit_Framework_TestCase {

	/**
	 * Tests Semlib_DateTime::isDay()
	 */
	public function testIsDay() {
		// TODO Auto-generated Semlib_DateTimeTest::testIsDay()
		$this->markTestIncomplete ( "isDay test not implemented" );

		Semlib_DateTime::isDay(/* parameters */);

	}

	/**
	 * Tests Semlib_DateTime::dayName()
	 */
	public function testDayName() {
		// TODO Auto-generated Semlib_DateTimeTest::testDayName()
		$this->markTestIncomplete ( "dayName test not implemented" );

		Semlib_DateTime::dayName(/* parameters */);

	}

	/**
	 * Tests Semlib_DateTime::getDateOfNext()
	 */
	public function testGetDateOfNext() {
		$today = Semlib_DateTime::today();
		$day = date('w',$today);
		for ($i = 0; $i < 7; $i++){
			$newDay = ($day + $i) % 7;
			$days = $i;
			if($days == 0){
				$days = 7;
			}
			$date = $today + ($days * DAY_IN_SECONDS);
			$newDate = Semlib_DateTime::getDateOfNext($newDay);
			$this->assertEquals($date,$newDate,'Didn\'t get expected day: '.date('D (d M)', $date).' != '.date('D (d M)', $newDate));
		}
	}

	/**
	 * Tests Semlib_DateTime::realToWorkingDays()
	 */
	public function testRealToWorkingDays() {
		// TODO Auto-generated Semlib_DateTimeTest::testRealToWorkingDays()
		$this->markTestIncomplete ( "realToWorkingDays test not implemented" );

		Semlib_DateTime::realToWorkingDays(/* parameters */);

	}

	/**
	 * Tests Semlib_DateTime::workingDaysBetween()
	 */
	public function testWorkingDaysBetween() {
		// TODO Auto-generated Semlib_DateTimeTest::testWorkingDaysBetween()
		$this->markTestIncomplete ( "workingDaysBetween test not implemented" );

		Semlib_DateTime::workingDaysBetween(/* parameters */);

	}

	/**
	 * Tests Semlib_DateTime::bankHolidays()
	 */
	public function testBankHolidays() {
		// TODO Auto-generated Semlib_DateTimeTest::testBankHolidays()
		$this->markTestIncomplete ( "bankHolidays test not implemented" );

		Semlib_DateTime::bankHolidays(/* parameters */);

	}

	/**
	 * Tests Semlib_DateTime::newYearBankHoliday()
	 */
	public function testNewYearBankHoliday() {
		// TODO Auto-generated Semlib_DateTimeTest::testNewYearBankHoliday()
		$this->markTestIncomplete ( "newYearBankHoliday test not implemented" );

		Semlib_DateTime::newYearBankHoliday(/* parameters */);

	}

	/**
	 * Tests Semlib_DateTime::easterSunday()
	 */
	public function testEasterSunday() {
		// TODO Auto-generated Semlib_DateTimeTest::testEasterSunday()
		$this->markTestIncomplete ( "easterSunday test not implemented" );

		Semlib_DateTime::easterSunday(/* parameters */);

	}

	/**
	 * Tests Semlib_DateTime::goodFriday()
	 */
	public function testGoodFriday() {
		// TODO Auto-generated Semlib_DateTimeTest::testGoodFriday()
		$this->markTestIncomplete ( "goodFriday test not implemented" );

		Semlib_DateTime::goodFriday(/* parameters */);

	}

	/**
	 * Tests Semlib_DateTime::easterMonday()
	 */
	public function testEasterMonday() {
		// TODO Auto-generated Semlib_DateTimeTest::testEasterMonday()
		$this->markTestIncomplete ( "easterMonday test not implemented" );

		Semlib_DateTime::easterMonday(/* parameters */);

	}

	/**
	 * Tests Semlib_DateTime::mayDayBankHoliday()
	 */
	public function testMayDayBankHoliday() {
		// TODO Auto-generated Semlib_DateTimeTest::testMayDayBankHoliday()
		$this->markTestIncomplete ( "mayDayBankHoliday test not implemented" );

		Semlib_DateTime::mayDayBankHoliday(/* parameters */);

	}

	/**
	 * Tests Semlib_DateTime::springBankHoliday()
	 */
	public function testSpringBankHoliday() {
		$bankHoliday2010 =  Semlib_DateTime::springBankHoliday(2010);
		$this->assertEquals('2010-05-31', date('Y-m-d', $bankHoliday2010));
		$bankHoliday2011 =  Semlib_DateTime::springBankHoliday(2011);
		$this->assertEquals('2011-05-30', date('Y-m-d', $bankHoliday2011));
		$bankHoliday2012 =  Semlib_DateTime::springBankHoliday(2012);
		$this->assertEquals('2012-06-04', date('Y-m-d', $bankHoliday2012));

	}

	public function testAdditional2011BankHolidayExists(){
		$this->assertTrue(Semlib_DateTime::isBankHoliday('2011-04-29'));
	}

	public function testAdditional2012BankHolidayExists(){
		$this->assertTrue(Semlib_DateTime::isBankHoliday('2012-06-05'));
	}


	/**
	 * Tests Semlib_DateTime::summerBankHoliday()
	 */
	public function testSummerBankHoliday() {
		// TODO Auto-generated Semlib_DateTimeTest::testSummerBankHoliday()
		$this->markTestIncomplete ( "summerBankHoliday test not implemented" );

		Semlib_DateTime::summerBankHoliday(/* parameters */);

	}

	/**
	 * Tests Semlib_DateTime::christmasBankHoliday()
	 */
	public function testChristmasBankHoliday() {
		// TODO Auto-generated Semlib_DateTimeTest::testChristmasBankHoliday()
		$this->markTestIncomplete ( "christmasBankHoliday test not implemented" );

		Semlib_DateTime::christmasBankHoliday(/* parameters */);

	}

	/**
	 * Tests Semlib_DateTime::boxingDayBankHoliday()
	 */
	public function testBoxingDayBankHoliday() {
		// TODO Auto-generated Semlib_DateTimeTest::testBoxingDayBankHoliday()
		$this->markTestIncomplete ( "boxingDayBankHoliday test not implemented" );

		Semlib_DateTime::boxingDayBankHoliday(/* parameters */);

	}

	/**
	 * Tests Semlib_DateTime::today()
	 */
	public function testToday() {
		// TODO Auto-generated Semlib_DateTimeTest::testToday()
		$this->markTestIncomplete ( "today test not implemented" );

		Semlib_DateTime::today(/* parameters */);

	}

	/**
	 * Tests Semlib_DateTime::secondsToString()
	 */
	public function testSecondsToString() {
		// TODO Auto-generated Semlib_DateTimeTest::testSecondsToString()
		$this->markTestIncomplete ( "secondsToString test not implemented" );

		Semlib_DateTime::secondsToString(/* parameters */);

	}

}

