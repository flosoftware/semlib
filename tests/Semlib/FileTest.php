<?php
abstract class Semlib_FileTest extends PHPUnit_Framework_TestCase {
	protected $_filename;

	protected function setUp(){
		$this->_filename = null;
	}

	protected function tearDown(){
		if(!is_null($this->_filename)){
			@unlink($this->_filename);
		}
	}
}