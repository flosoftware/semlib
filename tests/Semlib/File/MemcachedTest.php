<?php
class Semlib_File_MemcachedTest extends Semlib_FileTest {
	protected $_db;
	protected function setUp(){
		$this->_db = new DB_Mysql('root','','127.0.0.1','test');
		$this->_db->execute('CREATE TABLE IF NOT EXISTS `files` (
							  `id` varchar(100) NOT NULL,
							  `path` varchar(255) NOT NULL default "",
							  `filename` varchar(255) NOT NULL default "",
							  `contents` LONGTEXT NOT NULL,
							  PRIMARY KEY  (`id`)
							) TYPE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin');
		$this->_filename = null;
	}

	protected function _removeFileFromMemcached($filename, $server = array(array('127.0.0.1', '11211'))){
		$file = new Semlib_File_Memached($filename,$server);
		$this->assertTrue($file->delete());
	}

	public function testCanWriteFileBackedByDB(){
		$filename = '/tmp/test.txt';
		$data = <<<EOT
"InvoiceID","InvoiceDate","Branch Name","ClientCode","JobType","CandidateName","Hours","Net","NominalCode"
"307504R","2010-03-14","Inter-Bakery"," ","LGV C & E","Karl  Crocker","8.0000","115.28","AL"
"307504R","2010-03-14","Inter-Bakery"," ","LGV C & E","Christopher Amesbury","52.7500","645.31","AL"
"307504R","2010-03-14","Inter-Bakery"," ","LGV C & E","Lyndon Mustoe","41.5000","526.70","AL"
"307504R","2010-03-14","Inter-Bakery"," ","LGV C & E","Malcolm  Morgan","10.0000","144.10","AL"
"307504R","2010-03-14","Inter-Bakery"," ","LGV C & E","Julian Maddocks","20.7500","241.64","AL"
"307504R","2010-03-14","Inter-Bakery"," ","LGV C & E","Neil Houlson","9.0000","129.69","AL"
"307504R","2010-03-14","Inter-Bakery"," ","LGV C & E","John Anthony  Harris ","19.5000","233.53","AL"
"307504R","2010-03-14","Inter-Bakery"," ","LGV C & E","Stephen  Turpitt","9.7500","120.39","AL"
"307504R","2010-03-14","Inter-Bakery"," ","LGV C & E","Gareth Glave ","10.2500","132.33","AL"
"307504R","2010-03-14","Inter-Bakery"," ","LGV C & E","Mark Stafford","8.0000","91.28","AL"
EOT;
		$dbFile = new Semlib_File_Database($filename, $this->_db, 'files');
		$file = new Semlib_File_Memached($filename,array(array('127.0.0.1', '11211')), $dbFile);
		$file->write($data);
		$result = $this->_db->execute('SELECT * FROM files WHERE id = "'.md5($filename).'"');
		$row = $result->fetch_assoc();
		$this->assertEquals($data, gzuncompress($row['contents']), 'File contents in database did not match data inserted');
		$this->assertEquals(dirname($filename), $row['path'], 'Path in database did not match data inserted');
		$this->assertEquals(basename($filename), $row['filename'], 'Filename in database did not match data inserted');
		$savedData = $file->read();
		$this->assertEquals($data, $savedData,'Data read from object was not what was saved');
		if(class_exists('Memcached', false)){
			$file = new Semlib_File_Memached($filename,array(array('127.0.0.1', '11211')));
			$savedData = $file->read();
			$this->assertEquals($data, $savedData,'Data in memcached is not what was saved');
			$this->_removeFileFromMemcached($filename);
		}
		$dbFile = new Semlib_File_Database($filename, $this->_db, 'files');
		$file = new Semlib_File_Memached($filename,array(array('127.0.0.1', '11211')), $dbFile);
		$savedData = $file->read();
		$this->assertEquals($data, $savedData,'Data read from file was not what was saved');
	}

	public function testCanWriteFileBackedByFileSystem(){
		$filename = '/tmp/test.txt';
		$this->_filename = $filename;
		$data = 'This is testing data';
		$FSFile = new Semlib_File_FileSystem($filename);
		$file = new Semlib_File_Memached($filename,array(array('127.0.0.1', '11211')), $FSFile);
		$file->write($data);
		$this->assertTrue(file_exists($filename), 'Failed to create file');
		$savedData = file_get_contents($filename);
		$this->assertEquals($data, $savedData,'Data on filesystem was not what was saved');
		$savedData = $file->read();
		$this->assertEquals($data, $savedData,'Data read from file was not what was saved');
	}

	protected function tearDown(){
		if(!is_null($this->_filename)){
			@unlink($this->_filename);
		}
		//$this->_db->execute('DELETE FROM `files`');
	}
}