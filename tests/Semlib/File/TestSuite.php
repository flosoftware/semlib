<?php
/**
 * AllTests
 *
 * @category Robot
 * @package UnitTests
 * @version $Id: TestSuite.php 396 2009-05-27 15:18:56Z nickp $
 */

/**
 * Test helper
 */
require_once dirname(dirname(__DIR__)) . '/TestHelper.php';

class Semlib_File_TestSuite extends PHPUnit_Framework_TestSuite{
	/**
	 * Constructs the test suite handler.
	 */
	public function __construct () {
		$this->setName ( 'Semlib_File_TestSuite' );
		$this->addTestSuite( 'Semlib_File_DatabaseTest' );
		$this->addTestSuite( 'Semlib_File_FileSystemTest' );
		$this->addTestSuite( 'Semlib_File_MemcachedTest' );
	}

	/**
	 * Creates the suite.
	 */
	public static function suite (){
		return new self ( );
	}

}