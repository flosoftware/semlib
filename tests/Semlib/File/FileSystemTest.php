<?php
class Semlib_File_FileSystemTest extends Semlib_FileTest {
	public function testCanWriteFile(){
		$filename = '/tmp/test.txt';
		$this->_filename = $filename;
		$data = 'This is testing data';
		$file = new Semlib_File_FileSystem($filename);
		$file->write($data);
		$this->assertTrue(file_exists($filename), 'Failed to create file');
		$savedData = file_get_contents($filename);
		$this->assertEquals($data, $savedData,'Data on filesystem was not what was saved');
		$savedData = $file->read();
		$this->assertEquals($data, $savedData,'Data read from file was not what was saved');
	}
}