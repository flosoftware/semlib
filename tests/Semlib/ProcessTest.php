<?php

require_once 'Semlib/Process.php';

/**
 * Semlib_DateTime test case.
 */
class Semlib_ProcessTest extends PHPUnit_Framework_TestCase {
	const TEST_CMD = 'echo 1; sleep 2; echo 2; sleep 2; echo 3; sleep 2; echo 4; sleep 2;';
	public function testCanConstructProcess(){
		$process = new Semlib_Process(self::TEST_CMD);
	}

	public function testCanRunProcess(){
		$process = new Semlib_Process(self::TEST_CMD);
		$process->run();
		$this->assertEquals(true, $process->isRunning());
	}

	public function testCanGetOutput(){
		$process = new Semlib_Process('echo -n 1234');
		$process->run();
		sleep(10);
		$output = $process->getOutput();
		var_dump($output);
		$this->assertEquals('1234',$output);
	}
}