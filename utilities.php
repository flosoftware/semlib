<?php
define('DAY_IN_SECONDS',60 * 60 * 24);
function include_dir($path){
	/* @var $fileinfo DirectoryIterator */
	$dir = new DirectoryIterator($path);
	foreach ($dir as $fileinfo) {
	    if ($fileinfo->isFile() && $fileinfo->isReadable()) {
	        include($fileinfo->getPathname());
	    }
	}
}
include_dir(__DIR__ . '/utilities');

function generate_password($length) {

	// initialise
	$pass = "";
	$char = 0;

	// allowed characters (ignoring letters/numbers that look similar)
	$allowed = 'abcdefghijkmnpqrtuvwxyz2346789';
	$maxIndex = strlen($allowed) - 1;

	//create password of length defined by $length
	for($i = 0; $i < $length; $i++) {
		$index = rand(0, $maxIndex);
		$pass .= $allowed[$index];
	}
	return $pass;
}

function call_user_func_array_by_ref($function, Array $params){
	$args = array();
	foreach($params as $k => &$arg){
	    $args[$k] = &$arg;
	}
	return call_user_func_array($function, $args);
}

function basic_email_validation($email, $checkMx = false){
/*
	$validator = new Zend_Validate_EmailAddress(Zend_Validate_Hostname::ALLOW_DNS, $checkMx);
	return $validator->isValid($email);
*/
	if(strpos($email, '@') === false || strpos($email, '.') === false){
		return false;
	}else{
		return true;
	}
}
/**
 *
 * Parses a string containing sql and runs the statement as a mysql_multi query.
 * Runs queries based on foreign key dependancies
 *
 * Currently unusable due to http://bugs.mysql.com/bug.php?id=48024
 *
 * @param mysqli $db
 * @param string $sql
 * @param string $dbname
 * @param bool $drop
 */
function multi_query_dependant(mysqli $db, $sql, $dbname = null, $drop = false){
	$sql_dependancies = array(	'non_table' => array(),
								array());
	$sql = explode ( ';', $sql );
	foreach ( $sql as $sql_stmt ) {
		$sql_stmt = trim($sql_stmt);
		if (!empty($sql_stmt)) {
			if(preg_match('/^CREATE\s+TABLE[^`]+`([^`]+)`/i',$sql_stmt,$table)){
				$table = $table[1];
				if(preg_match_all('/REFERENCES\s+`([^`]+)`/i',$sql_stmt, $references)){
					$maxlevel = 0;
					$found = false;
					//For each reference to a table, look for referenced table in dependencies starting
					//at the previous highest found level (no need to look for lower dependencies)
					foreach($references[1] as $reference){
						$level = $maxlevel;
						do{
							$maxlevel = ++$level + 1;
							if(!isset($sql_dependancies[$level])){
								if(!$found){
									throw new Exception('Table '.$table.' create statement references '.$reference.', which is not found');
								}else{
									//Dependancy not found, but we've found a previous dependancy.
									//Possibility dependancy doesn't exist but, for speed, we haven't
									//check levels lower than previously found dependancy, so we're
									//assuming it's there.
									break;
								}
							}elseif(array_key_exists($reference, $sql_dependancies[$level])){
								$found = true;
								break;
							}
						}while(true);
					}
					//Add statement to level
					if(!isset($sql_dependancies[$maxlevel])){
						$sql_dependancies[$maxlevel] = array();
					}
					$sql_dependancies[$maxlevel][$table] = $sql_stmt;
				}else{
					$sql_dependancies[0][$table] = $sql_stmt;
				}
			}else{
				$sql_dependancies['non_table'][] = $sql_stmt;
			}
		}
	}
	$db->query('SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0');
	$db->query('SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0');
	$db->query('SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE="TRADITIONAL"');
	if(!is_null($dbname)){
		if($drop){
			$db->query('DROP SCHEMA IF EXISTS `'.$dbname.'`');
			$db->query('CREATE SCHEMA IF NOT EXISTS `'.$dbname.'`');
		}
		$db->query('USE `'.$dbname.'`');
	}
	foreach($sql_dependancies as $queries){
		if ($db->multi_query(r_implode(";\n",$queries))) {
		   do {
		       /* store first result set */
		       if ($result = $db->store_result()) {
		           //do nothing since there's nothing to handle
		           $result->close();
		       }
		   } while ($db->next_result());
		}
		if ($db->errno) {
			$err = "Stopped while retrieving result : ".$db->error;
			$result = $db->query('SHOW ENGINE INNODB STATUS');
			$status = $result->fetch_assoc();
			$err .= "\n".$status['Status'];
		  	throw new Exception($err);
		}
	}
	$db->query('SET FOREIGN_KEY_CHECKS=1');
	$db->query('SET SQL_MODE=@OLD_SQL_MODE');
	$db->query('SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS');
	$db->query('SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS');
}

function lazy_load(&$var, $class, $construct = true/*, $registry = false*/){
	$registry = false;
	if (!($var instanceof $class)){
		if ($registry){
			if(Zend_Registry::isRegistered($class)){
				$cache = Zend_Registry::get($class);
				if(array_key_exists($var, $cache)){
					return $cache[$var];
				}
			}
			$key = $var;
		}
		if ($construct){
			$var = new $class($var);
		}else{
			$func = array($class,'load');
			$numArgs = func_num_args();
			if ($numArgs <= 3){
				$var = call_user_func($func,$var);
			}else{
				$args = array($var);
				for($i = 3; $i < $numArgs; $i++){
					$args[] = func_get_arg($i);
				}
				$var = call_user_func_array($func,$args);
			}
			if(!$var){
				throw new Exception('<'.$class.'::load()> not found');
			}
		}
		if ($registry &&
			$var instanceof $class){
			if(!isset($cache)){
				Zend_Registry::set($class, array());
			}
			$registry = Zend_Registry::getInstance();
			$registry[$class][$key] = $var;
		}
	}
	return $var;
}

/*
 * Function to return whether a variable value is a positive integer
 * This is useful to test if a param could be a valid ID or TIMESTAMP
 */
function is_positive_integer($v){
	if($v != 0 && !is_bool($v)){
		return ctype_digit(strval($v));
	}
}

/*
 * Redirect function
 */
function redirect($url) {
    if (!headers_sent()) {
        header("Location: $url");
        exit;
    } else {
        echo '<script type="text/javascript">';
        echo 'window.location.href="'.$url.'";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
        echo '</noscript>';
    }
}

/*
 * Form field helper
 */
function formData($key, $arrayOfValues) {
	if(isset($arrayOfValues[$key])) {
		return htmlspecialchars($arrayOfValues[$key]);
	}
	return '';
}

/**
 * Return a display date in the format "d M Y".
 *
 * @param $string display date
 */
function getDisplayDateFromString($string) {

	if(empty($string)) {
		return $string;
	}

	try {
		$date = new DateTime($string);
		return $date->format('d M Y');
	}catch(Exception $e) {
		return $string;
	}
}

function getUKPostcodeRegex($caseSensitive = false) {
	return '/[A-Z]{1,2}[0-9R][0-9A-Z]? ?[0-9][ABD-HJLNP-UW-Z]{2}/'.
	   ($caseSensitive?'':'i');
}

function smartImplode($pieces, $glue, $removeNulls = false) {
	if($removeNulls) {
	    return preg_replace('(['.preg_quote($glue).']{2,})',$glue,implode($glue,$pieces));
	}
	else {
		return implode($glue,$pieces);
	}
}

function cmp($a, $b) {

	if($a instanceof ArrayObject) {
		$a = end($a);
		$b = end($b);
	}

	if ($a == $b) {
        return 0;
    }

    return ($a < $b) ? -1 : 1;
}

/**
 * Return a string filesize in a human-readable format.
 *
 * @param numeric $filesize
 */
function getDisplayFilesize($filesize) {

    if(is_numeric($filesize)) {
	    $decr = 1024; $step = 0;
	    $prefix = array('Byte','KB','MB','GB','TB','PB');

	    while(($filesize / $decr) > 0.9) {
	        $filesize = $filesize / $decr;
	        $step++;
	    }
	    return round($filesize,2).' '.$prefix[$step];
    } else {
	    return 'Unknown';
    }
}

function getDisplayMaxFilesize() {
	if (ini_get('UPLOAD_MAX_FILESIZE')) return str_replace('M', ' MB', ini_get('UPLOAD_MAX_FILESIZE'));
	else if (ini_get('upload_max_filesize')) return str_replace('M', ' MB', ini_get('upload_max_filesize'));
	else return 'Unknown';
}

function return_bytes($val) {
    $val = trim($val);
    $last = strtolower($val[strlen($val)-1]);
    switch($last) {
        // The 'G' modifier is available since PHP 5.1.0
        case 'g':
            $val *= 1024;
        case 'm':
            $val *= 1024;
        case 'k':
            $val *= 1024;
    }

    return $val;
}

function implode_with_keys($glue, $array, $valwrap='') {
    foreach($array AS $key => $value) {
        $ret[] = $key."=".$valwrap.$value.$valwrap;
    }
    return implode($glue, $ret);
}

function getNumberOfWeeks($datefrom, $dateto) {

	$datefrom = strtotime($datefrom, 0);
    $dateto = strtotime($dateto, 0);

    $difference = $dateto - $datefrom; // Difference in seconds

    // Number of full weeks
    return floor($difference / 604800);
}

// need this because stripslashes doesnt work when magic_quotes_gpc is turned off
function noMagicQuotesStripslashes($query) {
	$data = explode("\\",$query);
	$cleaned = implode("",$data);
	return $cleaned;
}

function getIdArrayFromObjectArray(array $objectArray) {
	$idArray = array();
	foreach($objectArray as $object) {
		$idArray[] = $object->getId();
	}
	return $idArray;
}

/**
 * Remove all characters that do not fit regex antipattern.
 *
 * @param string $string
 * @param string $antiPattern
 */
function stripInvalidStringChars($string, $antiPattern) {
    return preg_replace('/ {2,}/',' ',preg_replace($antiPattern, '', $string));
}

function idExistsInObjectArray($array, $id) {

	$flag = false;

    foreach($array as $object) {
        if(!is_object($object)) {
            return false;
        }

        if($id == $object->getId()) {
            $flag = true;
        }
    }

    return $flag;
}

function strToCamelCase($string) {
	$filter = new Zend_Filter_Word_SeparatorToCamelCase();
	return lcfirst(str_replace(" ","",$filter->filter($string)));
}

function strToPascalCase($string) {
	return ucfirst(strToCamelCase($string));
}

function getDebugBacktrace($NL = "<BR>") {
    $dbgTrace = debug_backtrace();
    $dbgMsg = $NL."START BACKTRACE $NL";
    foreach($dbgTrace as $dbgIndex => $dbgInfo) {
        $dbgMsg .= "\t at $dbgIndex  ".$dbgInfo['file']." (line {$dbgInfo['line']}) -> {$dbgInfo['function']}(".join(",",$dbgInfo['args']).")$NL";
    }
    $dbgMsg .= "END BACKTRACE".$NL;
    return $dbgMsg;
}

/**
 *
 * @param array $array
 */
function array_negative(array $array) {
	foreach($array as $key=>$val) {
		if(is_numeric($val)) {
		    $array[$key] = -$val;
		} elseif(is_bool($val)) {
            $array[$key] = !$val;
		}
	}
	return $array;
}

function compareDates($a,$b) {
	return date_create($a)>date_create($b);
}

function printAndLog($msg, $logFileHandle) {
	echo $msg;
	fwrite($logFileHandle, $msg);
}

function wasLastDateTimeValid() {
	$lastErrors = DateTime::getLastErrors();
	return $lastErrors['warning_count']==0 && $lastErrors['error_count']==0;
}

function isAJAXRequest() {
	return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
}

function floatvalue($value) {
     return floatval(preg_replace('#^([-]*[0-9\.,\' ]+?)((\.|,){1}([0-9-]{1,2}))*$#e', "str_replace(array('.', ',', \"'\", ' '), '', '\\1') . '.\\4'", $value));
}