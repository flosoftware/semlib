<?php
interface Semlib_Process_Interface{
	public function addToProcessManager(Semlib_ProcessManager $processManager);
	public function attachContainer(Semlib_ProcessContainer $container);
	public function notifyClosed();
}