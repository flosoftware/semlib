<?php
class Semlib_Process_PhpScript extends Semlib_Process{
	protected $_scriptTitle;

	public function __construct($script, $descriptorspec = null, $cwd = null, $env = null, $scriptTitle = null, $cmdExtraParams = null){
		if(is_null($scriptTitle)){
			$scriptTitle = 'PHPScript: '.$script;
		}
		$this->_scriptTitle = $scriptTitle;
		$cmd = '/usr/local/zend/bin/php '.$script.' -e '.getenv('environment');
		if (!empty($cmdExtraParams)) {
			$cmd .= ' '.$cmdExtraParams;
		}
		parent::__construct($cmd, $descriptorspec, $cwd, $env);
	}

	public function run($nonBlockingStreams = true){
		return parent::run($nonBlockingStreams);
		usleep(200);
	}

	public function getOutput(){
		$output = parent::getOutput();
		if(!empty($output)){
			$output = "\nScript <".$this->_scriptTitle."> output:\n".$output;
		}
		return $output;
	}

	public function getErrors(){
		$errors = parent::getErrors();
		if(!empty($errors)){
			$errors = "\nScript <".$this->_scriptTitle."> errors:\n".$errors;
		}
		return $errors;
	}

	public function getName(){
		return $this->_scriptTitle;
	}
}