<?php
abstract class Semlib_Graph{
	const FONT = FF_VERDANA;
	const LEGEND_FONT_SIZE = 9;
	const FONT_SIZE = 10;
	const FONT_SIZE_TITLE = 12;
	protected $_graph;

	public function __construct($width, $height, $scale){
		$this->_graph = new Graph($width, $height);
		$this->_graph->SetScale($scale);
		$this->_graph->xaxis->SetFont(Semlib_Graph::FONT,FS_NORMAL,Semlib_Graph::FONT_SIZE);
		$this->_graph->yaxis->SetFont(Semlib_Graph::FONT,FS_NORMAL,Semlib_Graph::FONT_SIZE);
		$this->_graph->legend->SetFont(Semlib_Graph::FONT,FS_NORMAL,Semlib_Graph::LEGEND_FONT_SIZE);
		$this->_graph->img->SetAntiAliasing();
	}

	public function setXTickInterval($interval){
		$this->_graph->xaxis->scale->ticks->Set($interval);
		return $this;
	}

	public function setYTickInterval($interval){
		$this->_graph->yaxis->scale->ticks->Set($interval);
		return $this;
	}

	public function &__get($name){
		return $this->_graph->$name;
	}

	public function __set($name, $value){
		$this->_graph->$name = $value;
	}

	public function __call($name, $arguments){
		return call_user_func_array(array($this->_graph,$name), $arguments);
	}
}
require_once 'jpgraph/jpgraph.php';
require_once 'jpgraph/jpgraph_plotline.php';
require_once 'jpgraph/jpgraph_date.php';