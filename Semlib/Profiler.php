<?php
class Semlib_Profiler {
	protected static $_timers = array();
	protected static $_defaultTimer;

	protected function __construct(){}
	protected function __clone(){}

	protected static function _getTimer($label, $initialise = false){
		/* @var $timer Semlib_Profiler_Timer */
		if (is_null($label)){
			if(is_null(self::$_defaultTimer)){
				self::$_defaultTimer = new Semlib_Profiler_Timer();
			}
			$timer = self::$_defaultTimer;
		}else{
			if (!array_key_exists($label, self::$_timers)){
				self::$_timers[$label] = new Semlib_Profiler_Timer();
			}
			$timer = self::$_timers[$label];
		}
		if($initialise){
			$timer->initialise();
		}
		return $timer;
	}

	public static function start($label = null){
		/* @var $timer Semlib_Profiler_Timer */
		$timer = self::_getTimer($label, true);
		return $timer->start();
	}

	public static function stop($label = null){
		/* @var $timer Semlib_Profiler_Timer */
		$timer = self::_getTimer($label);
		return $timer->stop();
	}

	public static function difference($label = null){
		/* @var $timer Semlib_Profiler_Timer */
		$timer = self::_getTimer($label);
		return $timer->difference();
	}

	public static function getAverage($label = null){
		/* @var $timer Semlib_Profiler_Timer */
		$timer = self::_getTimer($label);
		return $timer->getAverage();
	}

	public static function getTotal($label = null){
		/* @var $timer Semlib_Profiler_Timer */
		$timer = self::_getTimer($label);
		return $timer->getTotal();
	}
}
?>