<?php
class Semlib_Process implements Semlib_Process_Interface{
	protected $_cmd;
	protected $_descriptorspec;
	protected $_cwd;
	protected $_env;
	protected $_handle;
	protected $_pipes = array();
	protected $_processManager;
	protected $_onCloseProcesses = array();
	protected $_timer;
	protected $_status;
	protected $_excessiveOutput = array();
	protected $_stringOutput;

	protected $_container;

	public function __construct($cmd, $descriptorspec = null, $cwd = null, $env = null){
		$this->_cmd = $cmd;
		if(is_null($descriptorspec)){
			$this->_descriptorspec = array(	0 => array("pipe", "r"),  // stdin
											1 => array("pipe", "w"),  // stdout
											2 => array("pipe", "w")); // stderr
		}
		$this->_cwd = $cwd;
		$this->_env = $env;
		$this->_timer = new Semlib_Profiler_Timer();
		$this->_stringOutput = '';
	}

	public function attachContainer(Semlib_ProcessContainer $container){
		$this->_container = $container;
	}

	public function addToProcessManager(Semlib_ProcessManager $processManager){
		$processManager->addProcess($this);
	}

	public function setProcessManager(Semlib_ProcessManager $processManager){
		$this->_processManager = $processManager;
	}

	public function addOnCloseProcess(Semlib_Process_Interface $process){
		$this->_onCloseProcesses[] = $process;
	}

	public function run($nonBlockingStreams = true){
		$this->_timer->start();
		$this->_handle = proc_open($this->_cmd, $this->_descriptorspec, $this->_pipes, $this->_cwd, $this->_env);
		if($nonBlockingStreams){
			foreach ($this->_pipes as $id => $pipe){
				$this->_excessiveOutput[$id] = false;
				stream_set_blocking($pipe, 0);
			}
		}
	}

	public function isRunning(){
		$status = $this->getStatus();
		if(!is_array($status) || !$status['running']){
			if($status['exitcode'] != 0){
				$this->_onExitError($status);
			}
			return false;
		}
		return true;
	}

	protected function _onExitError($status){
		throw new Exception("\n".$this->getName()." failed\nExited with error code <".$status['exitcode'].">\nCommand being executed was: \n<".$this->_cmd.">\nOutput was: \n-----\n".preg_replace("/^Array/", "", str_replace("-!-", "\n-!-", $this->_stringOutput))."\n-----\n");
	}

	protected function _readPipe($pipe){
		@$str = stream_get_contents($this->_pipes[$pipe]);
		if ($str != "") {
			$this->_stringOutput .= "\nPIPE ".$pipe.' - '.var_export($str, true);
		}
		if(strlen($str) > 1000000 && !$this->_excessiveOutput[$pipe]){
			$this->_excessiveOutput[$pipe] = true;
			$lines = explode("\n",$str);
			$last20lines = implode("\n",array_slice($lines,-20));
			if(strlen($last20lines) > 5000){
				$last20lines = substr($last20lines, -5000);
			}
			$subj = $this->getName().' is producing exessive output on pipe <'.$pipe.'>';
			$msg = $this->getName()." is producing excessive output\nLast 20 lines: \n".$last20lines;
			mail('systems@depoel.co.uk', $subj, $msg);
		}
		return $str;
	}

	public function getOutput(){
		return $this->_readPipe(1);
	}

	public function getErrors(){
		return $this->_readPipe(2);
	}

	public function close(){
		$this->_timer->stop();
		$this->getOutput();
		$this->getErrors();
		foreach($this->_pipes as $pipe){
			@fclose($pipe);
		}
		@proc_close($this->_handle);
		foreach($this->_onCloseProcesses as $process){
			$process->addToProcessManager($this->_processManager);
		}
		$this->notifyClosed();
	}

	public function notifyClosed(){
		if(!is_null($this->_container)){
			$this->_container->notifyClosed($this);
		}
	}

	public function getTime(){
		return $this->_timer->getElapsed();
	}

	public function getName(){
		return $this->_cmd;
	}

	public function getStatus($live = true){
		if(!$live){
			return $this->_status;
		}
		$handle = $this->_handle;
		if(is_resource($handle)){
			$status = proc_get_status($handle);
			$this->_status = $status;
			return $status;
		}
		return false;
	}
}