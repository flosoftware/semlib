<?php

class Semlib_Email_Message_iCalendarEvent {

	protected $_organiser = array();
	protected $_recipients = array();
	protected $_recipientsBCC = array();
	protected $_startTimestamp;
	protected $_endTimestamp;
	protected $_description;
	protected $_summary;
	protected $_subject;
	protected $_location;
	
	protected $_attachment;

	protected $_errors;

	/**
	 * Set the organiser of the event
	 *
	 * @param string $email
	 * @param string $name
	 */
	public function setOrganiser($email, $name=null) {
		$this->_organiser = array(
			'email' => $email,
			'name' => $name
		);
	}

	/**
	 * Adds a recipient to the event
	 *
	 * @param string $email
	 * @param string $name
	 */
	public function addRecipient($email, $name=null) {
		$this->_recipients[$email] = $name;
	}

    /**
     * Adds a recipient to the event
     *
     * @param string $email
     */
    public function addBCC($email) {
        $this->_recipientsBCC[] = $email;
    }

	/**
	 * Add multiple recipients to the event
	 *
	 * @param array $recipients - in the format of $recipient[email] = name
	 */
	public function addRecipients($recipients) {
		$this->_recipients = $recipients + $this->_recipients;
	}

	/**
	 * @param int $startTimestamp - the Start Timestamp to set
	 */
	public function setStartTimestamp($startTimestamp) {
		$this->_startTimestamp = $startTimestamp;
	}

	/**
	 * @param int $endTimestamp - the End Timestamp to set
	 */
	public function setEndTimestamp($endTimestamp) {
		$this->_endTimestamp = $endTimestamp;
	}

	/**
	 * @param string $description - the Description to set
	 */
	public function setDescription($description) {
		$this->_description = $description;
	}

	/**
	 * @param string $summary - the Summary to set
	 */
	public function setSummary($summary) {
		$this->_summary = $summary;
	}
	
	public function getAttachment() {
		if(is_null($this->_attachment)) {
			$this->_generateAttachmentText();
		}
		return $this->_attachment;
	}

    /**
     * @param string $subject - the Subject to set
     */
    public function setSubject($subject) {
        $this->_subject = $subject;
    }

	/**
	 * @param string $location - the Location to set
	 */
	public function setLocation($location) {
		$this->_location = $location;
	}

	/**
	 * Sends the appointment to the recipients
	 */
	public function send($throwExceptions = false) {
		$this->_validate();

		if (count($this->_errors)>0) {
			return "Error during validation:<br/>\n".join("<br/>\n", $this->_errors);
		}

		$attachmentText = $this->_generateAttachmentText();

		$mail = new Semlib_Email_Message();
		$mail->addHeader('Content-Class', 'urn:content-classes:calendarmessage');

		$mail->setFrom($this->_organiser['email'], $this->_organiser['name']);

		foreach ($this->_recipients as $email => $name) {
			$mail->addTo($email, $name);
		}
		foreach ($this->_recipientsBCC as $email) {
            $mail->addBcc($email);
        }

        if(!is_null($this->_subject)) {
        	$mail->setSubject($this->_subject);
        }
		else {
			$mail->setSubject($this->_summary);
		}
		$mail->setBodyText($this->_description);

		$mail->createAttachment($attachmentText, 'text/calendar', Zend_Mime::DISPOSITION_INLINE, Zend_Mime::ENCODING_BASE64, 'etips_event.ics');

		$config = Zend_Registry::get('config');
		$mailTransport = null;
		if($config->application->mailing->smtp_host) {
		    $mailTransport = new Zend_Mail_Transport_Smtp($config->application->mailing->smtp_host);
		}

		try {
			$result = $mail->send($mailTransport);
			if (is_null($result)) {
				throw new Exception('There has been an error sending the email.');
			}
		} catch (Exception $e) {
			if(true===$throwExceptions) {
				throw new Semlib_Exception($e->getMessage());
			}
			$msg =  'Message could not be sent. <p>';
			$msg .= 'Mailer Error: ' . $e->getMessage();
			return $msg;
		}

		return 'Message has been sent';
	}

	protected function _generateAttachmentText() {
	    throw new Semlib_Exception("This method must be overriden");
	}

    protected function _validate() {

        // Check the organiser details
        if (empty($this->_organiser['email'])) {
            $this->_errors[] = 'Missing Organiser Email.';
        }

        // Check the recipients details
        if (count($this->_recipients) == 0) {
            $this->_errors[] = 'No Recipients have been added.';
        } else {
            foreach ($this->_recipients as $email => $name) {
                if (empty($email)) {
                    $this->_errors[] = 'Empty recipient email supplied.';
                }
            }
        }

        // Check the Start Date Time
        if (empty($this->_startTimestamp) || !is_int($this->_startTimestamp)) {
            $this->_errors[] = 'Missing or invalid Start Timestamp.';
        }

        // Check the End Date Time
        if (empty($this->_endTimestamp) || !is_int($this->_endTimestamp)) {
            $this->_errors[] = 'Missing or invalid End Timestamp.';
        }

        // Check the Description
        if (empty($this->_description)) {
            $this->_errors[] = 'Missing Description.';
        }

        // Check the Summary
        if (empty($this->_summary)) {
            $this->_errors[] = 'Missing Summary.';
        }
    }
}