<?php
require_once('Semlib/Email/Mailbox.php');

class Semlib_Email_Mailbox_iCalendarEvents {
	
	public static function getCalendarEvents($deleteAfterReading = false) {
		
		$mailbox = new Semlib_Email_Mailbox('dep-s-mail', '', '');
		
		$emailCount = $mailbox->getMailCount();
	
		if ($emailCount == 0) return;
		$events = array();
		for ($mailId = 1; $mailId <= $emailCount; $mailId++) {
			$parts = $mailbox->getMail($mailId);
		
			for ($x = 2; $x <= count($parts); $x++) {
				$part = $parts[$x];
				
				if (strstr($part['data'], 'BEGIN:VCALENDAR')) {
					$event = array();
					$error = array();
					
					$data = preg_replace('/(\r?\n) +/', '', $part['data']);
					
					preg_match('/UID:([^\r\n]*)/i', $data, $matches);
					
					if (!isset($matches[1])) {
						$error[] = 'Cannot find UID';
					} else {
					
						$uid = $matches[1];
						list(,,$event['crmEventId']) = explode('-', $uid, 3);
					
						//preg_match('/ATTENDEE;CN="(.*?)".*?PARTSTAT=(.*?):mailto:([^\r\n]*)/', $data, $matches);
						preg_match('/ATTENDEE;(.*?)\r?\n/i', $data, $matches);
						if (count($matches) != 2) {
							$error[] = 'Cannot find Attendee';
						} else {
						
							$attendee = $matches[1];
									
							preg_match('/CN="?(.*?)"?(;|:)/i', $attendee, $matches);
									
							if (count($matches) != 3) {
								$error[] = 'Cannot find Name';
							} else {
								$event['name'] = $matches[1];
								
								preg_match('/PARTSTAT=(.*?)(;|:)/i', $attendee, $matches);
								
								if (count($matches) != 3) {
									$error[] = 'Cannot find Response';
								} else {
									$event['response'] = $matches[1];
									
									preg_match('/mailto:(.*?)\n/i', $attendee."\n", $matches);
									if (count($matches) != 2) {
										$error[] = 'Cannot find Email';
										var_dump($attendee);
									} else {
										$event['email'] = $matches[1];
								
										$events[] = $event;
									
										if ($deleteAfterReading) {
											$mailbox->deleteMail($mailId);
										}
										
										break;
									}
								}
							}
						}
					}
					
					if (count($error) > 0) {
						echo '<pre>';
						var_dump($error);
						var_dump($data);
						echo '</pre>';
						break;
					}
				}
			}
		}
		
		if ($deleteAfterReading) {
			$mailbox->expungeDeletedEmails();
		}
		
		return $events;
	}
	
}
