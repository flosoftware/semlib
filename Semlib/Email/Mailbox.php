<?php
class Semlib_Email_Mailbox {
	
	protected $_username;
	protected $_password;
	protected $_host;
	protected $_port;
	protected $_type;
	protected $_mailbox;
	protected $_extraParams;
	
	protected $_connection;
	
	public function __construct($host, $user, $pass, $port = 110, $type = 'pop3', $mailbox = 'INBOX', $extraParams = null) {
		$this->_username = $user;
		$this->_password = $pass;
		$this->_host = $host;
		$this->_port = $port;
		$this->_type = $type;
		$this->_mailbox = $mailbox;
		$this->_extraParams = $extraParams;
		
		set_error_handler(array($this, 'handleNoticeErrors'), E_NOTICE);

		$this->_validate();
		$this->_connect();
	}
	
	private function _validate() {
		if (!is_string($this->_host) || empty($this->_host)) {
			throw new Exception('Host is a required field and must be a string.');
		}
		
		if (!is_string($this->_username) || empty($this->_username)) {
			throw new Exception('Username is a required field and must be a string.');
		}
		
		if (!is_string($this->_password) || empty($this->_password)) {
			throw new Exception('Password is a required field and must be a string.');
		}
		
		if (!is_int($this->_port) || $this->_port <= 0) {
			throw new Exception('Port is an optional field, but must be a number greater than zero.');
		}
		
		if (!is_string($this->_mailbox) || empty($this->_mailbox)) {
			throw new Exception('Mailbox is an optional field but must be a string.');
		}
		
		if (!is_null($this->_extraParams) && (!is_string($this->_extraParams) || empty($this->_extraParams))) {
			throw new Exception('Extra Params is an optional field but must be a string.');
		}
	}
	
	private function _connect() {
		$connectionString = '{'.$this->_host.':'.$this->_port.'/'.$this->_type;
		
		
		if (!is_null($this->_extraParams)) {
			$connectionString .= $this->_extraParams;
		}
		$connectionString .= '}'.$this->_mailbox;
		
		try {
			$this->_connection = imap_open($connectionString, $this->_username, $this->_password);
		} catch (Exception $e) {
			throw new Exception('Exception: '.$e->getMessage());
		}
		if (!$this->_connection) {
			throw new Exception('Error Connecting to host: '.join('<br/>\n', imap_errors()));
		}
	}
	
	
	public function getMailCount() {
		$info = imap_check($this->_connection);
		return $info->Nmsgs;
	}
	
	public function getMailboxStatus() {
		return imap_mailboxmsginfo($this->connection);
	}
	
	public function getMailList() {
		$mailCount = $this->getMailCount();
		
		$response = imap_fetch_overview($this->_connection, $mailCount);
		
    	foreach ($response as $msg) {
    		$result[$msg->msgno] = $msg;
    	}
    	
        return $result;
	}
	
	public function getMails() {
		$mailCount = $this->getMailCount();
		
		for ($x = 1; $x <= $mailCount; $x++) {
			$mails[$x] = $this->getMail($x);
		}
	}
	
	public function getMailHeader($mailId) {
		return imap_fetchheader($this->_connection, $mailId, FT_PREFETCHTEXT);
	}
	
	public function getMail($mailId) {
		$mail = imap_fetchstructure($this->_connection, $mailId);
	    $mail = $this->_getParts($mailId, $mail, 0);
    	$mail[0]["parsed"] = $this->_parseHeaders($mail[0]["data"]);
	    
	    return $mail;
	}
	
	public function deleteMail($mailId) {
		return imap_delete($this->_connection, $mailId);
	}
	
	public function expungeDeletedEmails() {
		imap_expunge($this->_connection);
	}
	
	private function _parseHeaders($headers) {
	    $headers = preg_replace('/\r\n\s+/m', '', $headers);
	    preg_match_all('/([^: ]+): (.+?(?:\r\n\s(?:.+?))*)?\r\n/m', $headers, $matches);
	    
	    foreach ($matches[1] as $key =>$value) {
	    	$result[$value] = $matches[2][$key];
	    }
	    
	    return($result);
	}
	
	private function _getParts($mailId, $part, $prefix) {   
	    $attachments=array();
	    
	    $attachments[$prefix]=$this->_decodePart($mailId, $part, $prefix);
	    
	     // multipart
	    if (isset($part->parts)) {
	        $prefix = ($prefix == "0") ? "" : "$prefix.";
	        foreach ($part->parts as $number => $subpart) {
	            $attachments = array_merge($attachments, $this->_getParts($mailId, $subpart, $prefix . ($number + 1)));
	        }
	    }
	    return $attachments;
	}
	
	private function _decodePart($mailId, $part, $prefix) {
	    $attachment = array();
	
	    if($part->ifdparameters) {
	        foreach($part->dparameters as $object) {
	            $attachment[strtolower($object->attribute)] = $object->value;
	            
	            if(strtolower($object->attribute) == 'filename') {
	                $attachment['is_attachment'] = true;
	                $attachment['filename'] = $object->value;
	            }
	        }
	    }
	
	    if($part->ifparameters) {
	        foreach($part->parameters as $object) {
	            $attachment[strtolower($object->attribute)] = $object->value;
	            
	            if(strtolower($object->attribute) == 'name') {
	                $attachment['is_attachment'] = true;
	                $attachment['name'] = $object->value;
	            }
	        }
	    }
	
	    $attachment['data'] = imap_fetchbody($this->_connection, $mailId, $prefix);
	    
	    if($part->encoding == 3) { // 3 = BASE64
	        $attachment['data'] = base64_decode($attachment['data']);
	        
	    } else if($part->encoding == 4) { // 4 = QUOTED-PRINTABLE
	        $attachment['data'] = quoted_printable_decode($attachment['data']);
	    }
	    
	    return($attachment);
	}
	
	public function __destruct() {
		if (!is_null($this->_connection)) {
			imap_close($this->_connection);
		}
	}
	
	public function handleNoticeErrors($errno, $errstr, $errfile = null, $errline = null, $errcontext = null) {
		//echo "NOTICE: ".$errstr."<br/>";
	}
		
	
	
	
	public function getCalendarEvents($deleteAfterReading = false) {
		$emailCount = $this->getMailCount();
	
		if ($emailCount == 0) return;
		$events = array();
		for ($mailId = 1; $mailId <= $emailCount; $mailId++) {
			$parts = $this->getMail($mailId); 
		
			for ($x = 2; $x <= count($parts); $x++) {
				$part = $parts[$x];
				
				if (strstr($part['data'], 'BEGIN:VCALENDAR')) {
					$event = array();
					$error = array();
					
					$data = preg_replace('/(\r?\n) +/', '', $part['data']);
					
					preg_match('/UID:([^\r\n]*)/i', $data, $matches);
					
					if (!isset($matches[1])) {
						$error[] = 'Cannot find UID';
					} else {
					
						$uid = $matches[1];
						$email['uid'] = $uid;
					
						//preg_match('/ATTENDEE;CN="(.*?)".*?PARTSTAT=(.*?):mailto:([^\r\n]*)/', $data, $matches);
						preg_match('/ATTENDEE;(.*?)\r?\n/i', $data, $matches);
						if (count($matches) != 2) {
							$error[] = 'Cannot find Attendee';
						} else {
						
							$attendee = $matches[1];
									
							preg_match('/CN="?(.*?)"?(;|:)/i', $attendee, $matches);
									
							if (count($matches) != 3) {
								$error[] = 'Cannot find Name';
							} else {
								$event['name'] = $matches[1];
								
								preg_match('/PARTSTAT=(.*?)(;|:)/i', $attendee, $matches);
								
								if (count($matches) != 3) {
									$error[] = 'Cannot find Response';
								} else {
									$event['response'] = $matches[1];
									
									preg_match('/mailto:(.*?)\n/i', $attendee."\n", $matches);
									if (count($matches) != 2) {
										$error[] = 'Cannot find Email';
										var_dump($attendee);
									} else {
										$event['email'] = $matches[1];
								
										$events[] = $event;
									
										if ($deleteAfterReading) {
											$this->deleteMail($mailId);
										}
										
										break;
									}
								}
							}
						}
					}
					
					if (count($error) > 0) {
						echo '<pre>';
						var_dump($error);
						var_dump($data);
						echo '</pre>';
						break;
					}
				}
			}
		}
		
		if ($deleteAfterReading) {
			$this->_expungeDeletedEmails();
		}
		
		return $events;
	}
	
}
