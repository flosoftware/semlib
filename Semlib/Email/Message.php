<?php

class Semlib_Email_Message extends Zend_Mail {
	
	protected $_defaultFromName = 'eTips';
	protected $_defaultFromEmail = 'systems@depoel.co.uk';
	
	public function __construct($charset='utf-8') {
		parent::__construct($charset);
	}

	public function send($transport = null) {
		$from = $this->getFrom();
		
		if (is_null($from) || empty($from)) {
			$this->setFrom($this->_defaultFromEmail, $this->_defaultFromName);
		}
		
		return parent::send($transport);
	}
}