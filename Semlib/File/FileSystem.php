<?php
class Semlib_File_FileSystem extends Semlib_File{
	public function read(){
		return file_get_contents($this->_filename);
	}

	public function write($data){
		$path = dirname($this->_filename);
		if(!is_dir($path)){
			mkdir($path, 0755, true);
		}
		return file_put_contents($this->_filename, $data);
	}

	public function delete(){
		return unlink($this->_filename);
	}
}