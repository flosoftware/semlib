<?php
class Semlib_File_Memached extends Semlib_File{
	protected $_storage;
	protected $_memcache = false;

	public function __construct($filename, Array $servers, Semlib_File $storage = null){
		parent::__construct($filename);
		$this->_storage = $storage;
		if(class_exists('Memcached', false)){
			$m = new Memcached('files');
			$m->setOption(Memcached::OPT_LIBKETAMA_COMPATIBLE, true);
			$m->setOption(Memcached::OPT_COMPRESSION, true);
			$m->setOption(Memcached::OPT_NO_BLOCK, true);
// Waiting on memcached upgrade
//			$m->setOption(Memcached::OPT_BINARY_PROTOCOL, true);
			if (!count($m->getServerList())) {
				$m->addServers($servers);
			}
			$this->_memcache = $m;
		}
	}

	public function read(){
		if($this->_memcache){
			$id = $this->_hashFilename();
			$callback = null;
			if($this->_storage){
				$callback = array($this->_storage, 'readCallback');
			}
			$data = $this->_memcache->getByKey($id, $id, $callback);
			if($this->_memcache->getResultCode() != Memcached::RES_SUCCESS){
				return false;
			}else{
				return $data;
			}
		}elseif($this->_storage){
			return $this->_storage->read();
		}
		return false;
	}

	public function write($data){
		$written = false;
		if($this->_memcache){
			$id = $this->_hashFilename();
			$this->_memcache->setByKey($id, $id, $data);
			$written = true;
		}
		if($this->_storage){
			$this->_storage->write($data);
			$written = true;
		}
		if($written){
			return count($data);
		}
		return false;
	}

	public function delete(){
		$deleted = false;
		if($this->_memcache){
			$id = $this->_hashFilename();
			$deleted = $this->_memcache->deleteByKey($id, $id);
		}
		if($this->_storage){
			$this->_storage->delete();
			$deleted = $deleted && true;
		}
		return $deleted;
	}
}