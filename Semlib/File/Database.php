<?php
class Semlib_File_Database extends Semlib_File{
	protected $_db;
	protected $_table;
	protected $_idField;
	protected $_pathField;
	protected $_filenameField;
	protected $_dataField;
	protected $_path;
	protected $_basename;
	protected $_data;

	public function __construct($filename, DB_Mysql $db, $table, $id_field = 'id', $path_field = 'path', $filename_field = 'filename', $data_field = 'contents'){
		parent::__construct($filename);
		$this->_db = $db;
		$this->_table = $db->filterFieldName($table);
		$this->_idField = $db->filterFieldName($id_field);
		$this->_pathField = $db->filterFieldName($path_field);
		$this->_filenameField = $db->filterFieldName($filename_field);
		$this->_dataField = $db->filterFieldName($data_field);
		$this->_path = dirname($this->_filename);
		$this->_basename = basename($this->_filename);
	}

	protected function _hashFilename(){
		return $this->_db->real_escape_string(parent::_hashFilename());
	}

	public function read(){
		if(!is_null($this->_data)){
			return $this->_data;
		}
		$sql = 'SELECT '.$this->_dataField.'
				FROM '.$this->_table.'
				WHERE '.$this->_idField.' = "'.$this->_hashFilename().'"';
		$result = $this->_db->execute($sql);
		if($result->fetch_num_rows() == 0){
			return false;
		}
		list($data) = $result->fetch_row();
		$data = @gzuncompress($data);
		if($data === false){
			return false;
		}
		$this->_data = $data;
		return $data;
	}

	public function write($data){
		$db = $this->_db;
		$this->_data = $data;
		$data = gzcompress($data, 3);
		$sql = 'REPLACE INTO '. $this->_table.'
				SET '.$this->_idField.' = "'.$this->_hashFilename().'",
				'.$this->_pathField.' = "'.$db->real_escape_string($this->_path).'",
				'.$this->_filenameField.' = "'.$db->real_escape_string($this->_basename).'",
				'.$this->_dataField.' = "'.$db->real_escape_string($data).'"';
		if($this->_db->execute($sql)){
			return count($this->_data);
		}
		return false;
	}

	public function delete(){
		$sql = 'DELETE
				FROM '.$this->_table.'
				WHERE '.$this->_idField.' = "'.$this->_hashFilename().'"';
		$this->_db->execute($sql);
		return ($this->_db->get_mysql_affected_rows() > 0);
	}
}