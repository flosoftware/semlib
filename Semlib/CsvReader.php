<?php
class Semlib_CsvReader{

	protected $_fileAsArray;
	protected $_headersForComparison;

	//Constants to define the way to select columns
	const NAME = 1;
	const OFFSET = 2;

	public function __construct($filepath, array $headersForComparison = null){
		if(!$filepath) {
			throw new Semlib_Exception_InvalidArgumentException('No file specified');
		}
		elseif(!file_exists($filepath)){
			throw new Semlib_Exception('Cannot find file at: '.$filepath);
		}
		ini_set('auto_detect_line_endings', true);
		$this->_headersForComparison = $headersForComparison;
		$this->_fileAsArray = file($filepath, FILE_IGNORE_NEW_LINES);
	}

	public function buildArray($columns = null, $columnType = self::NAME){
		//If columnType is NAME the first row will be the keys, otherwise the first row is the first row of data
		if($columnType == self::NAME){
			$keys = explode(',', $this->_fileAsArray[0]);
			unset($this->_fileAsArray[0]);

			if(!empty($this->_headersForComparison) && implode(',',$keys)!='"'.implode('","',$this->_headersForComparison).'"') {
				throw new Semlib_Exception('CSV column headers do not match expected headers');
			}

			$columnOffsets = array();
			foreach($keys as $position => $key){
				if(is_null($columns) || in_array($key, $columns)){
					$columnOffsets[$key] = $position;
				}
			}
		}else{
			$columnOffsets = array();
			foreach($columns as $offset){
				$columnOffsets[$offset] = $offset;
			}
		}
		$returnData = array();
		foreach($this->_fileAsArray as $rowNumber => $row){
			if(trim_all($row, array(',')) != ''){
				foreach($columnOffsets as $key => $offset){
						$rowArray = explode(',', $row);
						$returnData[$rowNumber][trim($key, '"')] = trim($rowArray[$offset], '"');
				}
			}
		}
		return $returnData;
	}
}