<?php
abstract class Semlib_SessionHandler {
	protected $_lifetime;
	protected $_log;
	public function __construct() {
		$this->_lifetime = ini_get('session.gc_maxlifetime');
		if ('user' == ini_get('session.save_handler')){
			session_set_save_handler(array(&$this, 'open'),
									 array(&$this, 'close'),
									 array(&$this, 'read'),
									 array(&$this, 'write'),
									 array(&$this, 'destroy'),
									 array(&$this, 'gc'));
		}
	}

	public function start() {
		Zend_Session::start();
	}

	public function setLog(Zend_Log $log) {
		$this->_log = $log;
	}

    protected function _log($msg, $level = 'err') {
    	if(!is_null($this->_log)){
	   		/* @var $log Zend_Log */
	   		$log = $this->_log;
	   		try{
	   			$log->$level(get_class($this).': '.$msg);
	   		}catch(Exception $e){

	   		}
    	}
	}

	abstract public function open($save_path, $session_name);
	abstract public function close();
	abstract public function read($id);
	abstract public function write($id, $data);
	abstract public function destroy($id);
	abstract public function gc($maxlifetime);
}