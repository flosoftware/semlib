<?php
class Semlib_Format{
	public static function money($value, $currency = 'GBP'){
        switch ($currency) {
            case 'EUR':
                return '&euro;'.number_format($value, 2);
                break;
            default:
            case 'GBP':
                return '&pound;'.number_format($value, 2);
                break;
        }

	}

	public static function date($timestamp){
		$timestamp = (int) $timestamp;
		if ($timestamp < 946684800){  //Reject dates before 2000-01-01
				return 'Invalid date';
		}
		return date('d M Y',$timestamp);
	}

	public static function time($timestamp){
		$timestamp = (int) $timestamp;
		if ($timestamp < 946684800){  //Reject dates before 2000-01-01
				return 'Invalid time';
		}
		return date('H:i:s',$timestamp);
	}

	public static function datetime($timestamp){
		return Semlib_Format::date($timestamp).' '.Semlib_Format::time($timestamp);
	}
}