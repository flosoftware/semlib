<?php
abstract class Semlib_Profiler_Abstract{
	abstract public function initialise();
	abstract public function start();
	abstract public function stop();
	abstract public function difference();
	abstract public function getAverage();
	abstract public function getTotal();
}