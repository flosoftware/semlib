<?php
class Semlib_Profiler_Memory extends Semlib_Profiler_Abstract{
	protected $_initial = 0;
	protected $_final = 0;
	protected $_difference = 0;
	protected $_usages = array();

	protected function _isStarted(){
		return (0 != $this->_initial);
	}

	protected function _isStopped(){
		return (0 != $this->_final);
	}

	protected function _memory(){
		return memory_get_usage();
	}

	public function initialise(){
		$this->_inital = 0;
		$this->_final = 0;
		$this->_difference = 0;
	}

	public function start(){
		$this->initialise();
		$this->_initial = $this->_memory();
		return $this->_initial;
	}

	public function stop(){
		if(!$this->_isStarted()){
			throw new Exception('Memory usage monitor not started');
		}
		if(!$this->_isStopped()){
			$this->_final = $this->_memory();
			$this->_usages[] = $this->_difference = $this->_final - $this->_initial;
		}
		return $this->_final;
	}

	public function difference(){
		$this->stop();
		return $this->_difference;
	}

	public function getAverage(){
		$numRuns = count($this->_usages);
		if ($numRuns > 0){
			return array_sum($this->_usages) / $numRuns;
		}
		return 0;
	}

	public function getTotal(){
		$numRuns = count($this->_usages);
		if ($numRuns > 0){
			return array_sum($this->_usages);
		}
		return 0;
	}
}
?>