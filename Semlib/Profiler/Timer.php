<?php
class Semlib_Profiler_Timer extends Semlib_Profiler_Abstract{
	protected $_start = 0;
	protected $_stop = 0;
	protected $_elapsed = 0;
	protected $_times = array();

	protected function _isStarted(){
		return (0 != $this->_start);
	}

	protected function _isStopped(){
		return (0 != $this->_stop);
	}

	protected function _time(){
		return microtime(true);
	}

	public function initialise(){
		$this->_start = 0;
		$this->_stop = 0;
		$this->_elapsed = 0;
	}

	public function start(){
		$this->initialise();
		$this->_start = $this->_time();
		return $this->_start;
	}

	public function stop(){
		if(!$this->_isStarted()){
			throw new Exception('Timer not started');
		}
		if(!$this->_isStopped()){
			$this->_stop = $this->_time();
			$this->_times[] = $this->_elapsed = $this->_stop - $this->_start;
		}
		return $this->_stop;
	}

	public function difference(){
		$this->stop();
		return $this->_elapsed;
	}

	public function getElapsed(){
		if($this->_isStopped()){
			return $this->_elapsed;
		}
		return $this->_time() - $this->_start;
	}

	public function getAverage(){
		$numTimes = count($this->_times);
		if ($numTimes > 0){
			return array_sum($this->_times) / $numTimes;
		}
		return 0;
	}

	public function getTotal(){
		$numTimes = count($this->_times);
		if ($numTimes > 0){
			return array_sum($this->_times);
		}
		return 0;
	}
}
?>