<?php
abstract class Semlib_File{
	protected $_filename;
	protected $_id;
	public function __construct($filename){
		$this->_filename = static::normaliseFilename($filename);
	}
	abstract public function read();
	abstract public function write($data);
	abstract public function delete();

	public function readLines(){
		return explode("\n",$this->read());
	}

	public static function normaliseFilename($filename){
	    $original = null;
	    while($original != $filename){
	        $original = $filename;
	        $find = array(  '/([^\/.]+\/\.\.\/)+/i',
	                        '/\/\/+/i');
	        $replace = array(   '',
	                            '/');
	        $filename = preg_replace($find, $replace, $filename);
	    }
	    return $filename;
	}

	protected function _hashFilename(){
		if(!is_null($this->_id)){
			return $this->_id;
		}
		$this->_id = md5($this->_filename);
		return $this->_id;
	}

	public function readCallback($memc, $key, &$value){
		$value = $this->read();
		return ($value !== false);
	}
}