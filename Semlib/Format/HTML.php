<?php
class Semlib_Format_HTML extends Semlib_Format{
    public static function money($value, $currency = 'GBP'){
        if($value == 0){
            return '&nbsp;-&nbsp;';
        }
        return parent::money($value, $currency);
    }
	public static function difference($value){
		if($value == 0){
			return '&nbsp;-&nbsp;';
		}
		if($value > 0){
			return '<div style="color:blue;">+'.number_format($value,2).'</div>';
		}else{
			return '<div style="color:red;">'.number_format($value,2).'</div>';
		}

	}

	/**
	 * Method will display a float value in red for negative values
	 *
	 * @param float $value
	 * @return string;
	 */
	public static function showAsNegative($value = 0) {
		if (is_float($value) && $value < 0) {
			return '<span style="color:red;">' . self::money($value) . '</span>';
		} else {
			return self::money($value);
		}
	}
}