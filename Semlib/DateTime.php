<?php
class Semlib_DateTime extends DateTime
{
	const SUNDAY    = 0;
	const MONDAY    = 1;
	const TUESDAY   = 2;
	const WEDNESDAY = 3;
	const THURSDAY  = 4;
	const FRIDAY    = 5;
	const SATURDAY  = 6;
	static protected $_days = array(Semlib_DateTime::SUNDAY => 'Sunday',
									Semlib_DateTime::MONDAY => 'Monday',
									Semlib_DateTime::TUESDAY => 'Tuesday',
									Semlib_DateTime::WEDNESDAY => 'Wednesday',
									Semlib_DateTime::THURSDAY => 'Thursday',
									Semlib_DateTime::FRIDAY => 'Friday',
									Semlib_DateTime::SATURDAY => 'Saturday');

	public static function isDay($day){
		if(!isset(self::$_days[$day])){
			throw new Exception('Provided day is not valid');
		}else{
			return true;
		}
	}

	public static function dayName($day){
		self::isDay($day);
		return self::$_days[$day];
	}

	/**
	 * Returns the date of the next occurence of a day.  By default if today is that day it will return next week's date
	 * @param $day
	 * @param $returnToday Sets the function to return todays date if today is the day we're looking for
	 * @return unknown_type
	 */
	public static function getDateOfNext($day, $returnToday = false, $startDate = null){
		self::isDay($day);
		if(is_null($startDate)) {
			$date = self::today();
		}else {
			$date = $startDate;
		}
		$today = date('w', $date);
		$days = $day - $today;
		if(!$returnToday && $days <= 0){
			$days += 7;
		}
		return strtotime('+'.$days.' day', $date);
	}

	public static function getDateOfLast($day){
		$next = self::getDateOfNext($day, true);
		return strtotime('- 1 week', $next);
	}

	public static function realToWorkingDays($days,$start_date = null){
		if (is_null($start_date)){
			$start_date = time();
		}
		$realDays = (floor($days/5)*7);
		$remainder = $days % 5;
		$realDays += $remainder;
		$startDay = date('w',$start_date);
		if ($startDay == (6 - $remainder)){
			$realDays += 2;
		}elseif($startDay == ((7 - $remainder) % 7)){
			$realDays++;
		}
		$end_date = strtotime('+ '.$realDays.' day', $start_date);
		$bank_holidays = bank_holidays(date('Y',$start_date),date('Y',$end_date));
		foreach ($bank_holidays as $holiday){
			$holiday = strtotime($holiday);
			if (($holiday >= $start_date) && ($holiday <= $end_date)){
				$realDays++;
			}
		}
		return $realDays;
	}

	public static function workingDaysBetween($start,$end){
		$working_days = 0;
		while ( $start <= $end ) {
			if (date('D',$start) == 'Sat' ||
				date('D',$start) == 'Sun' ||
				array_key_exists(date('Ymd',$start),bank_holidays(date('Y',$start),date('Y',$end))) ) {
			} else {
				$working_days++;
			}
			$start= strtotime('+ 1 day', $start);
		}
		return $working_days;
	}

	public static function bankHolidays($start_year, $end_year = null){
		$bankHols = array();
		if (is_null($end_year)){
			$end_year = $start_year;
		}
		for($year = $start_year; $year <= $end_year; $year++){
			$bankHols[] = date('Ymd',self::newYearBankHoliday($year));
			$bankHols[] = date('Ymd',self::goodFriday($year));
			$bankHols[] = date('Ymd',self::easterMonday($year));
			$bankHols[] = date('Ymd',self::mayDayBankHoliday($year));
			$bankHols[] = date('Ymd',self::springBankHoliday($year));
			$bankHols[] = date('Ymd',self::summerBankHoliday($year));
			$bankHols[] = date('Ymd',self::christmasBankHoliday($year));
			$bankHols[] = date('Ymd',self::boxingDayBankHoliday($year));
			if(intval($year) == 2011){
				$bankHols[] = '20110429';
			}elseif(intval($year) == 2012){
				$bankHols[] = '20120605';
			}
		}
		return $bankHols;
	}

	public static function newYearBankHoliday($year){
		$dayTimestamp = strtotime($year.'-01-01');
		$dayNum = date('w',$dayTimestamp);
		if ($dayNum == 0){
			$dayTimestamp += DAY_IN_SECONDS;
		}elseif ($dayNum == 6){
			$dayTimestamp = strtotime('+ 2 day', $dayTimestamp);
		}
		return $dayTimestamp;
	}

	public static function easterSunday($year){
		return strtotime($year.'-03-21 + '.easter_days($year).' days');
	}

	public static function goodFriday($year){
		return strtotime('- 2 day', easter_sunday($year));
	}

	public static function easterMonday($year){
		return strtotime('+ 1 day',easter_sunday($year));
	}

	public static function mayDayBankHoliday($year){
		$firstMay = strtotime($year.'-05-01');
		$dayNum = date('w',$firstMay);
		if ($dayNum == 0){
			$mayDay = strtotime('+ 1 day',$firstMay);
		}elseif($dayNum == 1){
			$mayDay = $firstMay;
		}else{
			$mayDay = strtotime(' + '.(8 - $dayNum).' day', $firstMay);
		}
		return $mayDay;
	}

	protected static function _mondayAdjustment($dayNum){
		$days = 0;
		if ($dayNum == 0){
			$days = 6;
		}elseif($dayNum == 1){
			$days = 7;
		}else{
			$days = ($dayNum - 1);
		}
		return $days;
	}

	public static function springBankHoliday($year){
		if(intval($year) == 2012){
			return strtotime('2012-06-04');
		}
		$firstJune = strtotime($year.'-06-01');
		$days = self::_mondayAdjustment(date('w',$firstJune));
		return strtotime('- '.$days.' day', $firstJune);
	}

	public static function summerBankHoliday($year){
		$firstSept = strtotime($year.'-09-01');
		$days = self::_mondayAdjustment(date('w',$firstSept));
		return strtotime('- '.$days.' day', $firstSept);
	}

	public static function christmasBankHoliday($year){
		$christmas_bh = strtotime($year.'-12-25');
		$dayNum = date('w',$christmas_bh);
		$days = 0;
		if ($dayNum == 0){
			$days = 1;
		}elseif ($dayNum == 6){
			$days = 2;
		}
		return strtotime('+ '.$days.' day', $christmas_bh);
	}

	public static function boxingDayBankHoliday($year){
		$christmas_bh = christmas_bh($year);
		$days = 1;
		$dayNum = date('w',$christmas_bh);
		if ($dayNum == 5){
			$days = 3;
		}
		return strtotime('+ '.$days.' day', $christmas_bh);
	}

	public static function isBankHoliday($date){
		$bankHolidays = self::bankHolidays(date('Y', $date), date('Y', $date));
		return in_array(date('Ymd', $date), $bankHolidays);
	}

	public static function today(){
		return strtotime(date('Y-m-d'));
	}

	public static function yesterday(){
		return strtotime('-1 days', self::today());
	}

	public static function secondsToString($seconds, $depth = 2, $periods = null){
		$time_string = array();
		if(is_null($periods)){
			$periods = array( 'day'    => DAY_IN_SECONDS,
						      'hour'   => 3600,
			      			  'minute' => 60,
							  'second' => 1);
		}
		foreach($periods as $name => $period){
			if($depth == 0){
				break;
			}
			$period_count = 0;
			if($seconds >= $period){
		   		$period_count = floor($seconds/$period);
		   		$seconds -= ($period_count * $period);
		   		$depth--;
		  	}
		   	$time_string[] = $period_count.' '.$name.(($period_count > 1 || $period_count == 0)?'s':'');
		}
		return implode(' ',$time_string);
	}

	/**
	 * Accepts a timestamp and returns the
	 * week number using Zend_Date
	 *
	 * @param string
	 */
	public static function getWeekNumberFromDate($timestamp = null) {
		$date = (!is_null($timestamp)) ? $timestamp : time();
		$zendDate = new Zend_Date($date);
		return $zendDate->toString('w');
	}

	/**
	 * Accepts a timestamp and returns the
	 * tax week number
	 *
	 * @param int $taxWeek
	 */
	public static function getTaxWeekFromDate($timestamp) {

    	// get dates
    	$now = new DateTime( (is_integer($timestamp)?'@':'').$timestamp , new DateTimeZone('UTC'));
        $thisTaxYearStarts = new DateTime("6th April this year", new DateTimeZone('UTC'));
        $lastTaxYearStarts = new DateTime("6th April last year", new DateTimeZone('UTC'));

        // calculate week
        if($now < $thisTaxYearStarts) {
            $taxWeek = (int) ceil(($lastTaxYearStarts->diff($now)->format('%a') + 1) / 7);
        }
        else {
            $taxWeek = (int) ceil(($thisTaxYearStarts->diff($now)->format('%a') + 1) / 7);
        }

	    return $taxWeek;
    }

    public static function getNextTaxWeekEndDate() {

        // get dates
        $now = new DateTime('now', new DateTimeZone('UTC'));
        $thisTaxYearEnds = new DateTime("5th April this year", new DateTimeZone('UTC'));
        $nextTaxYearEnds = new DateTime("5th April next year", new DateTimeZone('UTC'));

        // calculate week
        if($now > $thisTaxYearEnds) {
            return $nextTaxYearEnds->format('Y-m-d');
        }
        return $thisTaxYearEnds->format('Y-m-d');
    }

    /**
     * Accepts a datetime string and validates if its a valid datetime
     *
     * @param $date
     */
    public static function checkIsRealDate($date)
    {
    	if (preg_match("/^(\d{4})-(\d{2})-(\d{2})$/", $date, $datePart))
    	{
    		if (checkdate($datePart[2], $datePart[3], $datePart[1]))
    		{
    			return true;
    		}
    	}
    	return false;
    }

    public static function checkDateIsInPast($date){
    	if(strtotime($date) < strtotime(date("Y-m-d"))){
    		return true;
    	}
    		return false;
    }
}