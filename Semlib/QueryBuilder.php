<?php
class Semlib_QueryBuilder{
	protected $_select = array();
	protected $_insert = array();
	protected $_where = array();
	protected $_from = '';
	protected $_join = array();
	protected $_leftJoin = array();
	protected $_group = array();
	protected $_withRollup = false;
	protected $_having = array();
	protected $_orderBy = array();
	protected $_multipleInsertValues = array();
	protected $_multipleInsertMode = false;

	protected $_limit_start = '';
	protected $_limit_duration = '';

	protected $_insert_table = '';
	protected $_set_vars = array();
	protected $_set_var_increments = array();

	protected $_insert_ignore = false;

	protected $_update_table = '';
	protected $_allow_update_without_where = false;

	protected $_delete_table = '';
	protected $_allow_delete_without_where = false;

	protected $_replace_table = '';

	protected $_db;



	const INSERT = 'insert';
	const DELETE = 'delete';
	const UPDATE = 'update';

	public function __construct(DB_Mysql $dbConn){
		$this->_db = $dbConn;
	}

	/**
	 * @return DB_MysqlStatement
	 */
	public function execute() {
		return $this->_db->execute($this);
	}

    public function getLastInsertId() {
        return $this->_db->get_last_insert_id();
    }

    public function getAffectedRows() {
    	return $this->_db->get_mysql_affected_rows();
    }

    public function escape($string) {
        return $this->_db->real_escape_string($string);
    }

	public function reset(){
		$this->_select = array();
		$this->_insert = array();
		$this->_where = array();
		$this->_from = '';
		$this->_join = array();
		$this->_leftJoin = array();
		$this->_group = array();
		$this->_withRollup = false;
		$this->_having = array();
		$this->_orderBy = array();
		$this->_limit_start = '';
		$this->_limit_duration = '';

		$this->_insert_table = '';
		$this->_set_vars = array();
		$this->_set_var_increments = array();
		$this->_multipleInsertValues = array();
		$this->_multipleInsertMode = false;

		$this->_insert_ignore = false;

		$this->_update_table = '';
		$this->_allow_update_without_where = false;

		$this->_delete_table = '';
		$this->_allow_delete_without_where = false;

		$this->_replace_table = '';
	}

	/**
	 * @param string $column
	 * @return Semlib_QueryBuilder
	 */
	public function addSelect($column){
		$this->_select[] = $column;
		return $this;
	}

	/**
	 * @param string $column
	 * @return Semlib_QueryBuilder
	 */
	public function addInsertCol($column){
		$this->_insert[] = $column;
		return $this;
	}

	public function clearSelect(){
		$this->_select = array();
		return $this;
	}

	/**
	 * @param string $from
	 * @return Semlib_QueryBuilder
	 */
	public function setFrom($from){
		$this->_from = $from;
		return $this;
	}

	/**
	 * @param string $where
	 * @return Semlib_QueryBuilder
	 */
	public function addWhere($where){
		$this->_where[] = $where;
		return $this;
	}

	/**
	 * @param string $join
	 * @return Semlib_QueryBuilder
	 */
	public function addJoin($join){
		$this->_join[] = $join;
		return $this;
	}

	/**
	 * @param string $join
	 * @return Semlib_QueryBuilder
	 */
	public function addLeftJoin($join){
		$this->_leftJoin[] = $join;
		return $this;
	}

	/**
	 * @param string $group
	 * @return Semlib_QueryBuilder
	 */
	public function addGroup($group){
		$this->_group[] = $group;
		return $this;
	}

	/**
	 * @param bool $rollup
	 * @return Semlib_QueryBuilder
	 */
	public function setRollup($rollup = false){
		$this->_withRollup = $rollup;
		return $this;
	}

	/**
	 * @param string $group
	 * @return Semlib_QueryBuilder
	 */
	public function addHaving($having){
		$this->_having[] = $having;
		return $this;
	}

	public function addOrderBy($orderBy){
		$this->_orderBy[] = $orderBy;
		return $this;
	}

	public function setLimit($limit_start, $limit_duration=null) {
		$this->_limit_start = $limit_start;
		if (!is_null($limit_duration)) {
			$this->_limit_duration = $limit_duration;
		}
		return $this;
	}

	public function setInsert($tableName, $ignore = false) {
		$this->_insert_table = $tableName;
		$this->_replace_table = '';
		$this->_update_table = '';
		$this->_delete_table = '';
		$this->_insert_ignore = (boolean)$ignore;
		return $this;
	}

	public function addSetVars($vars) {
		$this->_set_vars = $vars + $this->_set_vars;
		return $this;
	}

	public function addSetVar($col, $val) {
		$this->_set_vars[$col] = $val;
        return $this;
	}

	public function addSetVarIncrement($col, $val) {
		$this->_set_var_increments[$col] = $val;
	}

	/**
	 * Handle multiple inserts
	 *
	 * @param array $cols Array of column names to set
	 * @param array $values 2d array of values
	 */
	public function addSetVarMultiple(array $cols, array $values) {
		$this->_multipleInsertValues = array('cols'=>$cols, 'values'=>$values);
		$this->_multipleInsertMode = true;
	}

	public function setUpdate($tableName) {
		$this->_update_table = $tableName;
		$this->_replace_table = '';
		$this->_insert_table = '';
		$this->_delete_table = '';
		return $this;
	}

	public function setAllowUpdateWithoutWhere() {
		$this->_allow_update_without_where = true;
	}

	public function setDelete($tableName) {
		$this->_delete_table = $tableName;
		$this->_insert_table = '';
		$this->_update_table = '';
		$this->_replace_table = '';
        return $this;
	}

	public function setAllowDeleteWithoutWhere() {
		$this->_allow_delete_without_where = true;
	}

	public function setReplace($tableName) {
		$this->_replace_table = $tableName;
		$this->_insert_table = '';
		$this->_update_table = '';
		$this->_delete_table = '';
        return $this;
	}

	public function __tostring(){

		if (count($this->_where) == 0 && ( (!empty($this->_update_table) && !$this->_allow_update_without_where) || (!empty($this->_delete_table) && !$this->_allow_delete_without_where) ) ) {
			trigger_error('Missing Where Clause in SQL statemtent (or call setAllowUpdateWithoutWhere() or setAllowDeleteWithoutWhere() to apply update to all rows)',E_USER_WARNING);
			return '';
		}

		$query = '';

		if (!empty($this->_update_table)) {
			$query .= 'UPDATE '.$this->_update_table.' ';
            if(count($this->_join) > 0){
                $query .= "\nINNER JOIN ".implode("\nINNER JOIN ",$this->_join)." ";
            }
            if(count($this->_leftJoin) > 0){
                $query .= "\nLEFT JOIN ".implode("\nLEFT JOIN ",$this->_leftJoin)." ";
            }
		} else if (!empty($this->_insert_table)) {
			$query .= 'INSERT '.(true===$this->_insert_ignore?'IGNORE ':'').'INTO `'.$this->_insert_table.'` ';
			if (!empty($this->_insert)) {
				$query .= '(' . implode(",\n", $this->_insert) . ') ';
			}elseif($this->_multipleInsertMode && !empty($this->_multipleInsertValues) && !empty($this->_multipleInsertValues['cols'])) {
				$query .= '(' . implode(",\n", $this->_multipleInsertValues['cols']) . ') ';
			}
		} else if (!empty($this->_delete_table)) {
			$query .= 'DELETE FROM `'.$this->_delete_table.'` ';
		} else if (!empty($this->_replace_table)) {
			$query .= 'REPLACE `'.$this->_replace_table.'` ';
		}

		if (count($this->_set_vars) > 0 || count($this->_set_var_increments) > 0 || (count($this->_multipleInsertValues) > 0 && $this->_multipleInsertMode) ) {
			if($this->_multipleInsertMode) {
				$query .= 'VALUES ';
				if (count($this->_multipleInsertValues) > 0) {
					$valuesStr = '';
					foreach($this->_multipleInsertValues['values'] as $values) {
						$valuesStr .= '(';
						foreach($values as $val) {
							if (is_null($val)) {
								$valuesStr .= 'NULL,';
							} else if (is_int($val) || is_float($val)) {
								$valuesStr .= $val.',';
							} else if ($val == 'NOW()' || $val == 'CURDATE()') {
								$valuesStr .= $val.',';
							} else {
								$valuesStr .= '\''.$this->_db->real_escape_string($val).'\',';
							}
						}
						$valuesStr = substr($valuesStr, 0, -1);
						$valuesStr .= '),';
					}
				}
				$valuesStr = substr($valuesStr, 0, -1);
				$query .= $valuesStr;
			}else {
				$query .= 'SET ';
				$vars_string = '';

				if (count($this->_set_vars) > 0) {
					foreach ($this->_set_vars as $col => $val) {
						if (!empty($vars_string)) $vars_string .= ', ';
						if (is_null($val)) {
							$vars_string .= $col.' = NULL ';
						} else if (is_int($val) || is_float($val)) {
							$vars_string .= $col.' = '.$val;
						} else if ($val == 'NOW()' || $val == 'CURDATE()') {
							$vars_string .= $col.' = '.$val;
						} else {
							$vars_string .= $col.' = \''.$this->_db->real_escape_string($val).'\'';
						}
					}
				}

				if (count($this->_set_var_increments) > 0) {
					foreach ($this->_set_var_increments as $col => $val) {
						if (!empty($vars_string)) $vars_string .= ', ';

						if (is_null($val)) {
							$vars_string .= $col.' = NULL ';
						} else {
							$vars_string .= $col.' = '.$col.' + '.(int)$val;
						}
					}

				}

				$query .= $vars_string.' ';
			}

			if(count($this->_where) > 0){
				$query .= "\nWHERE ".implode("\nAND ",$this->_where);
			}

		} else if (!empty($this->_select)) {
			$query .= 'SELECT '.implode(",\n", $this->_select);
			if(!empty($this->_from)){
				$query .= "\nFROM ".$this->_from;
			}
			if(count($this->_join) > 0){
				$query .= "\nINNER JOIN ".implode("\nINNER JOIN ",$this->_join);
			}
			if(count($this->_leftJoin) > 0){
				$query .= "\nLEFT JOIN ".implode("\nLEFT JOIN ",$this->_leftJoin);
			}
			if(count($this->_where) > 0){
				$query .= "\nWHERE ".implode("\nAND ",$this->_where);
			}
			if(count($this->_group) > 0){
				$query .= "\nGROUP BY ".implode(", ",$this->_group);
				if($this->_withRollup){
					$query .= ' WITH ROLLUP';
				}
			}
			if(count($this->_having) > 0){
				$query .= "\nHAVING ".implode("\nAND ",$this->_having);
			}
			if(count($this->_orderBy) > 0){
				$query .= "\nORDER BY ".implode(", ",$this->_orderBy);
			}
			if($this->_limit_start!=0 || $this->_limit_duration!=0){
				if(isset($this->_limit_start)){
					$query .= "\nLIMIT ".$this->_limit_start;
					if(!empty($this->_limit_duration)){
						$query .= ", ".$this->_limit_duration;
					}
				}
			}
		} else if (isset($this->_update_table) || isset($this->_delete_table)) {
			if(count($this->_where) > 0){
				$query .= "\nWHERE ".implode("\nAND ",$this->_where);
			}
		}
		return $query;
	}
}