<?php
class Semlib_ProcessContainer implements Semlib_Process_Interface
{
	protected $_processManager;
	protected $_processes = array();
	protected $_runningProcessCount = 0;
	protected $_onCloseProcesses = array();

	protected $_parentContainer;

	public function addProcess(Semlib_Process_Interface $process){
		$process->attachContainer($this);
		$this->_processes[] = $process;
	}

	public function notifyClosed(){
		$this->_runningProcessCount--;
		if($this->_runningProcessCount == 0){
			foreach($this->_onCloseProcesses as $process){
				$process->addToProcessManager($this->_processManager);
			}
			if(!is_null($this->_parentContainer)){
				$this->_parentContainer->notifyClosed();
			}
		}
	}

	public function addOnCloseProcess(Semlib_Process_Interface $process){
		$this->_onCloseProcesses[] = $process;
	}

	public function attachContainer(Semlib_ProcessContainer $container){
		$this->_parentContainer = $container;
	}

	public function addToProcessManager(Semlib_ProcessManager $processManager){
		$this->_runningProcessCount = count($this->_processes);
		$this->_processManager = $processManager;
		foreach($this->_processes as $process){
			$process->addToProcessManager($processManager);
		}
	}

}