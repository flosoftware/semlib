<?php
/**
 * Semlib_Memcached Class to utilise the Memcached caching system for objects
 *
 * Uses the Singleton pattern, so use Semlib_Memcached::getInstance()
 *
 * @author samb
 *
 */
class Semlib_Memcached {

	private static $_instance;
	private $_useMemcached;
	private $_memcached;

	public static function getInstance() {
		if (!isset(self::$_instance)) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	public function fetchAllKeys() {
		if ($this->_useMemcached == false) {
			return array();
		}

		// Get the servers from the Zend Registry which in turn comes from the config
		$servers = Zend_Registry::get('MemcachedServers');

		// Add the servers to the Memcached object to allow getting and setting of data
		$serverCount = 0;
		$serverString = '';
		foreach($servers as $server) {
			if (!empty($server[0])) {
				if (!empty($serverString)) $serverString .= ',';
				$serverString .= $server[0];
			}
		}
		return explode("\n", trim(`memdump --servers=$serverString | /bin/grep -vE '^memc.sess' | /bin/grep -vE '^etips.'`));
	}

	protected function __construct() {
		// Check that Memcached class exists
		if(!class_exists('Memcached', false) || !Zend_Registry::isRegistered('MemcachedServers')) {
			$this->_useMemcached = false;
			return;
		}

		// Load memcached with the desired options
		$this->_memcached = new Memcached();
		$this->_memcached->setOption(Memcached::OPT_LIBKETAMA_COMPATIBLE, true);
		$this->_memcached->setOption(Memcached::OPT_COMPRESSION, true);
		$this->_memcached->setOption(Memcached::OPT_NO_BLOCK, true);
		// Waiting on memcached upgrade
		//$this->_memcached->setOption(Memcached::OPT_BINARY_PROTOCOL, true);

		// Get the servers from the Zend Registry which in turn comes from the config
		$servers = Zend_Registry::get('MemcachedServers');

		// Add the servers to the Memcached object to allow getting and setting of data
		$serverCount = 0;
		foreach($servers as $server) {
			if (!empty($server[0]) && !empty($server[1])) {
				$this->_memcached->addServer($server[0], $server[1]);
				$serverCount++;
			}
		}

		// If no servers are added, don't use memcached
		if ($serverCount==0) {
			$this->_useMemcached = false;
			return;
		}

		$this->_useMemcached = true;

	}

	/**
	 * Adds OR replaces a value in the cache
	 * @param string $key
	 * @param mixed $value
	 * @param int $expiration
	 * @return bool
	 */
	public function set($key, $value, $expiration = 0) {
		if ($this->_useMemcached && isset($this->_memcached)) {
			// Set the value in the cache with the supplied unique key
			return $this->_memcached->set($key, $value, $expiration);
		} else {
			return false;
		}
	}

	/**
	 * Gets a value from the cache based upon the supplied unique key
	 *
	 * If the key does not exist, a new object is created from the key supplied
	 *
	 * @param string $key
	 */
	public function get($key) {
		if ($this->_useMemcached && isset($this->_memcached)) {
			// Get the value from memcached using the supplied unique key
			return $this->_memcached->get($key);
		} else {
			return false;
		}
	}

	/**
	 * Deletes a key from the cache to free up memory
	 *
	 * @param string $key
	 */
	public function delete($key) {
		if ($this->_useMemcached && isset($this->_memcached)) {
			// Delete the key in the cache
			return $this->_memcached->delete($key);
		} else {
			return false;
		}
	}

	public function getLastResultMessage(){
		if ($this->_useMemcached && isset($this->_memcached)) {
			return $this->_memcached->getResultMessage();
		} else {
			return false;
		}
	}

}