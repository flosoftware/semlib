<?php
class Semlib_Filter {

    protected $_fields = array();
	protected $_filter;

	protected $_filterRules = array();
	protected $_validatorRules = array();

	protected $_data = array();

	/**
	 * Separate store for objects because
	 * Zend_Filter_Input cannot deal with objects
	 *
	 * @var $_objData array
	 *
	 */
	protected $_objData = array();

	protected $_options = array(Zend_Filter_Input::ESCAPE_FILTER => 'StripTags');

	protected $_runValidation = true;
    protected $_returnFilteredValues = false;

	protected function _setFilter() {
		$this->_filter = new Zend_Filter_Input($this->_filterRules, $this->_validatorRules, $this->_data, $this->_options);
	}

	protected function _addFilter($id, $filter) {
		$this->_filterRules[$id] = $filter;
	}

	protected function _addValidator($id, $validator) {
		if(!isset($this->_validatorRules[$id])) {
			$this->_validatorRules[$id] = $validator;
		}
	}

	protected function _addData($id, $data) {
		$this->_data[$id] = $data;
	}

    protected function _addDataFromArray($data)
    {
        if(count($this->_fields)) {
            foreach($this->_fields as $key => $filedName) {
                $this->_addData($filedName, $data[$key]);
            }
        } else {
            throw new Exception('Semlib Filter Error. Value object has not  defined fields list');
        }
    }

    protected function _replaceDataWithFilteredValues()
    {
        if(count($this->_fields)) {
            $filterData = $this->getFilteredData();
            foreach($this->_fields as $key => $filedName) {
                $this->_addData($filedName, $filterData->getEscaped($filedName));
            }
        } else {
            throw new Exception('Semlib Filter Error. Value object has not  defined fields list');
        }
    }

	protected function _addObject($id, $object) {
		$this->_objData[$id] = $object;
	}

	protected function _addOption($id, $option) {
		$this->_options[$id] = $option;
	}

	public function getData() {
		return $this->_data;
	}

	public function getObjectData() {
		return $this->_objData;
	}

    public function getFilteredData()
    {
        return $this->_filter;
    }

	public function hasErrors() {
		if(!$this->_runValidation) {
			return false;
		}
		elseif($this->_filter->hasInvalid() || $this->_filter->hasMissing()) {
			return true;
		}
        if($this->_returnFilteredValues) {
            $this->_replaceDataWithFilteredValues();
        }
		return false;
	}

	public function getMessages() {
		return $this->_filter->getMessages();
	}

	public function getMessagesAsString() {
		$messagesString = '';
		foreach($this->_filter->getMessages() as $message){
			$messagesString .= implode(', ', $message)."\n";
		}
		return $messagesString;
	}



	public function getValidationState() {
		return $this->_runValidation;
	}

	public function __get($fieldName) {
		// if object allow to pass thru
		if (array_key_exists($fieldName, $this->_objData) && is_object($this->_objData[$fieldName])) {
			return $this->_objData[$fieldName];
		}

		if($this->_runValidation) {
			/* @var $filter Zend_Filter_Input */
			$filter = $this->_filter;
			return $filter->$fieldName;
		}
		else {
			if (array_key_exists($fieldName, $this->_data)) {
            	return $this->_data[$fieldName];
        	}
        	else {
        		throw new Exception('Get cannot find the field name you requested: ' . $fieldName);
        	}
		}
	}

	public function __isset($id) {
        return isset($this->_data[$id]) || isset($this->_objData[$id]);
    }

	public function __set($name,$value) {
		throw new Exception('Can\'t set field on filter');
	}
}