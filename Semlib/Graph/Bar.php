<?php
class Semlib_Graph_Bar extends Semlib_Graph{
	/**
	 * Enter description here...
	 *
	 * @param string $name
	 * @param array $data
	 * @param string $colour
	 * @param bool $addToGraph
	 * @return BarPlot
	 */
	public function addPlot($name, Array $data, $colour, $addToGraph = false){
		$plot = new BarPlot($data);
		$plot->SetLegend($name);
		$plot->SetFillColor($colour);
		if($addToGraph){
			$this->_graph->Add($plot);
		}
		return $plot;
	}

	public function addAccPlot(Array $plots, $addToGraph = false){
		$plot = new AccBarPlot($plots);
		if($addToGraph){
			$this->_graph->Add($plot);
		}
		return $plot;
	}

	public function addGroupPlot(Array $plots){
		$plot = new GroupBarPlot($plots);
		$this->_graph->Add($plot);
		return $plot;
	}
}
require_once 'jpgraph/jpgraph_bar.php';