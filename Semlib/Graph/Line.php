<?php
class Semlib_Graph_Line extends Semlib_Graph{
	public function addPlot($name, $ydata, $xdata, $weight = 1,$y2 = false){
		$plot = new LinePlot($ydata, $xdata);
		$plot->SetLegend($name);
		$plot->SetWeight($weight);
		if(!$y2){
			$this->_graph->Add($plot);
		}else{
			$this->_graph->AddY2($plot);
		}
		return $plot;
	}
}
require_once 'jpgraph/jpgraph_line.php';