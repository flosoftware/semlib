<?php
/*
Requires the following table:
CREATE TABLE `sessions` (
  `id` varchar(100) NOT NULL default '',
  `data` text NOT NULL,
  `started` int(11) NOT NULL default '0',
  `expires` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `expires` (`expires`)
) TYPE=InnoDB;
*/
class Semlib_SessionHandler_Memcached_DbBacked extends Semlib_SessionHandler_Memcached {
	private $_storage;

	protected function _prefixId($id){
		/* @var $db DB_Mysql */
		$db = $this->_storage;
		$id = parent::_prefixId($id);
		return $db->real_escape_string($id);
	}

	public function __construct($prefix, DB_Mysql $db) {
		$this->_storage = $db;
		parent::__construct($prefix);
	}

	public function open($save_path, $session_name) {
		return parent::open($save_path, $session_name);
	}

	public function close() {
		return $this->gc(0);
	}

	protected function _writeToCache($id,$data) {
		$cacheSecs = floor($this->_lifetime * 0.9);
		parent::write($id,$data,$cacheSecs);
	}

	public function read($id) {
		$data = parent::read($id);
		$prefixId = $this->_prefixId($id);
		if($data === false){
			/* @var $db DB_Mysql */
			$db = $this->_storage;
			$result = $db->execute('SELECT data FROM sessions WHERE id ="'.$prefixId.'"');
			list($data) = $result->fetch_row();
			$this->_writeToCache($id, $data);
		}
		return $data;
	}

	public function write($id, $data) {
		$this->_writeToCache($id, $data);
		/* @var $db DB_Mysql */
		$db = $this->_storage;
		$id = $this->_prefixId($id);
		$data = $db->real_escape_string($data);
		$time = time();
		if(empty($data)){
			$this->_log($data.print_r(debug_backtrace(), true));
		}
		$db->execute('INSERT INTO sessions VALUES ("'.$id.'", "'.$data.'", '.$time.', '.$time.')
						ON DUPLICATE KEY UPDATE expires = '.$time.', data = "'.$data.'"');
	}

	public function destroy($id) {
		parent::destroy($id);
		/* @var $db DB_Mysql */
		$db = $this->_storage;
		$id = $this->_prefixId($id);
		$db->execute('DELETE FROM sessions WHERE id = "'.$id.'"');
	}

	public function gc($maxlifetime) {
		parent::gc($maxlifetime);
		/* @var $db DB_Mysql */
		$db = $this->_storage;
		$time = time() - $this->_lifetime;
		$db->execute('DELETE FROM sessions WHERE expires < '.$time);
	}
}