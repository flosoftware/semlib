<?php
class Semlib_SessionHandler_Memcached extends Semlib_SessionHandler{
	private $_storage = false;
	protected $_prefix;

	protected function _prefixId($id){
		return $this->_prefix.$id;
	}

	public function __construct($prefix) {
		if(empty($prefix)){
			$prefix = 'sess.';
		}
		$this->_prefix = $prefix;
		if(class_exists('Memcached', false)){
			$this->_storage = new Memcached('sessions');
			$this->_storage->setOption(Memcached::OPT_LIBKETAMA_COMPATIBLE, true);
			$this->_storage->setOption(Memcached::OPT_COMPRESSION, true);
			$this->_storage->setOption(Memcached::OPT_NO_BLOCK, true);
// Waiting on memcached upgrade
//			$this->_storage->setOption(Memcached::OPT_BINARY_PROTOCOL, true);
		}
		parent::__construct();
	}

	public function open($save_path, $session_name) {
		if($this->_storage){
			$servers = explode(',',$save_path);
			foreach($servers as &$server){
				$server = explode(':',trim($server));
			}
			if (!count($this->_storage->getServerList())) {
				$this->_storage->addServers($servers);
			}
		}else{
			$this->_log('Memcached unavailable');
		}
		return true;
	}

	public function close() {
		return true;
	}

	public function read($id) {
		if(!$this->_storage){
			return false;
		}
		$id = $this->_prefixId($id);
		$data = $this->_storage->getByKey($id, $id);
		return $data;
	}

	public function write($id, $data, $lifetime = false) {
		if(!$this->_storage){
			return true;
		}
		if($lifetime === false){
			$lifetime = $this->_lifetime;
		}
		$id = $this->_prefixId($id);
		return $this->_storage->setByKey($id, $id, $data, $lifetime);
	}

	public function destroy($id) {
		if(!$this->_storage){
			return true;
		}
		$id = $this->_prefixId($id);
		return $this->_storage->deleteByKey($id, $id);
	}

	public function gc($maxlifetime) {
		return true;
	}
}