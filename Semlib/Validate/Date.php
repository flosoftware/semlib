<?php
class Semlib_Validate_Date extends Zend_Validate_Date {

	public function __construct($dateFormat = 'YYYY-MM-dd', $locale = 'en_GB') {
		parent::__construct($dateFormat, $locale);
    }
    
    public function isValid($value) {
    	if (parent::isValid($value) && date('Y-m-d', strtotime($value)) == $value) {
    		return true;
    	} else {
    		$this->_error(self::INVALID);
    		return false;
    	}
    }
}