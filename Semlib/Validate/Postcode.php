<?php
 
class Semlib_Validate_Postcode extends Zend_Validate_Abstract
{
 
    const NOT_MATCH = 'postcodeNotMatch';
 
    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_MATCH => "'%value%' is not a valid UK post code"
    );
 
    /**
     * Defined by Zend_Validate_Interface
     *
     * Returns true if and only if $value matches against the postcode patterns
     *
     * @param  string $value
     * @return boolean
     */
    public function isValid($value)
    {
        //Set the value for error messages
        $this->_setValue($value);
 
        // Permitted letters depend upon their position in the postcode.
        $alpha1 = "[abcdefghijklmnoprstuwyz]";                          // Character 1
        $alpha2 = "[abcdefghklmnopqrstuvwxy]";                          // Character 2
        $alpha3 = "[abcdefghjkstuw]";                                   // Character 3
        $alpha4 = "[abehmnprvwxy]";                                     // Character 4
        $alpha5 = "[abdefghjlnpqrstuwxyz]";                             // Character 5
 
        // Expression for postcodes: AN NAA, ANN NAA, AAN NAA, and AANN NAA
        $pcexp[0] = '/^('.$alpha1.'{1}'.$alpha2.'{0,1}[0-9]{1,2})([0-9]{1}'.$alpha5.'{2})$/';
 
        // Expression for postcodes: ANA NAA
        $pcexp[1] =  '/^('.$alpha1.'{1}[0-9]{1}'.$alpha3.'{1})([0-9]{1}'.$alpha5.'{2})$/';
 
        // Expression for postcodes: AANA NAA
        $pcexp[2] =  '/^('.$alpha1.'{1}'.$alpha2.'[0-9]{1}'.$alpha4.')([0-9]{1}'.$alpha5.'{2})$/';
 
        // Exception for the special postcode GIR 0AA
        $pcexp[3] =  '/^(gir)(0aa)$/';
 
        // Exception for the special postcode SAN TA1
        $pcexp[4] =  '/^(san)(ta1)$/';
 
        // Standard BFPO numbers
        $pcexp[5] = '/^(bfpo)([0-9]{1,4})$/';
 
        // c/o BFPO numbers
        $pcexp[6] = '/^(bfpo)(c\/o[0-9]{1,3})$/';
 
        // Load up the string to check, converting into lowercase and removing spaces
        $postcode = strtolower($value);
        $postcode = str_replace(' ', '', $postcode);
 
        // Assume we are not going to find a valid postcode
        $valid = false;
 
        // Check the string against the six types of postcodes
        foreach ($pcexp as $regexp) {
 
            if (preg_match($regexp,$postcode, $matches)) {
 
                // Load new postcode back into the form element
                $value = strtoupper($matches[1] . ' ' . $matches [2]);
 
                // Take account of the special BFPO c/o format
                $value = preg_replace('/C\/O/', '/c/o /', $value);
 
                // Remember that we have found that the code is valid and break from loop
                $valid = true;
                break;
            }
        }
 
        // Return false and set error message
        if(!$valid) {
            $this->_error();
            return false;
        }
 
        return true;
    }
 
}