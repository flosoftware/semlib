<?php
/**
 * User: mpietrzak
 * Date: 25/09/2013
 * Time: 15:17
 * Validation of BSN (Dutch equivalent of National Insurance Number)
 */

/**
 * BSN Validator Class
 * Class Semlib_Validate_Bsn
 */
class Semlib_Validate_Bsn extends Zend_Validate_Abstract
{

    /**
     * Error message key for the BSN which does not contain exactly 9 digits
     */
    const WRONG_FORMAT = 'invalidLetters';

    /**
     * Error message key for the BSN which fails 11-test validation
     */
    const WRONG_TEST11 = 'test11Failed';

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::WRONG_FORMAT => 'BSN contains invalid letters.',
        self::WRONG_TEST11 => 'Invalid BSN'
    );

    /**
     * Returns true if and only if $value is a valid BSN
     *
     * BSN is valid when it contains only 9 digits and passes 11-test algorithm
     * Algorithm:
     * if given string is 456336540, then:
     *   (9 x 4) + (8 x 5) + (7 x 6) + (6 x 3) + (5 × 3) + (4 x 6) + (3 x 5) + (2 x 4) + (- 1 x 0)
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  string $value BSN
     * @return boolean
     * @throws Zend_Valid_Exception If validation of $value is impossible
     */
    public function isValid($value)
    {
        $valid = false;
        if (preg_match('/^[0-9]{9}$/', $value)){
            $factor = 9;
            $sum = 0;
            for ($i = 0; $i < 8; $i++){
                $sum += $value[$i] * $factor--;
            }
            // the last number in BSN is multiplied by -1
            $sum += -1 * $value[8];

            // summary must be eleven's multiplier
            $valid = ($sum % 11 == 0);

            if (!$valid){
                $this->_error(self::WRONG_TEST11);
            }
        } else {
            $this->_error(self::WRONG_FORMAT);
        }
        return $valid;
    }
}