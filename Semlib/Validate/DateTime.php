<?php
class Semlib_Validate_DateTime extends Zend_Validate_Date {

	public function __construct($dateFormat = 'YYYY-MM-dd HH:mm:ss', $locale = 'en_GB') {
		parent::__construct($dateFormat, $locale);
    }
    
    public function isValid($value) {

    	if (parent::isValid($value) && date('Y-m-d H:i:s', strtotime($value)) == $value) {
    		return true;
    	} else {
    		$this->_error(self::INVALID);
    		return false;
    	}
    }
    
}