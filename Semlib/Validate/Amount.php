<?php
/*
 * Validator for amount value. Amount should have only 2 decimal digits.
 */

class Semlib_Validate_Amount extends Zend_Validate_Regex {

    const NOT_MATCH = 'regexNotMatch';

    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_MATCH => "'%value%' does not valid amount value"
    );

    /**
     * @var array
     */
    protected $_messageVariables = array();

    /**
     * Regular expression pattern
     *
     * @var string
     */
    protected $_pattern = '/^[0-9]+(\.[0-9]{1,2})?$/';
    
    public function __construct($pattern=null)
    {
    }
    
    public function setPattern($pattern)
    {
        return $this;
    }
}
?>
