<?php
/**
 *
 * @author igristock
 *
 * Validator class used to validate if a given object is of the correct instance type.
 * The type to compare against is passed into the construct as a string.
 */
class Semlib_Validate_ObjectInstance extends Zend_Validate_Abstract{

	const INCORRECT_INSTANCE = 'incorrectInstance';

	private $_comparator;

	protected $_messageTemplates = array(
			self::INCORRECT_INSTANCE => " '%value%' does not match the expected object instance"
			);

	public function __construct($comparator = null){
		$this->_comparator = $comparator;
	}
	public function isValid($object){

		//ensure that $object is a valid object
		if(!is_object($object)){
			$this->_error(self::INCORRECT_INSTANCE);
			return false;
		}
		// get the class name from the given object
		$objectInstance = (string)get_class($object);
		$this->_setValue($objectInstance);
		// compare the two
		if(!$objectInstance instanceof $this->_comparator){
			$this->_error(self::INCORRECT_INSTANCE);
			return false;
		}

		return true;
	}
}
?>