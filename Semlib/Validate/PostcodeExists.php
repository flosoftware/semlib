<?php

class Semlib_Validate_PostcodeExists extends Zend_Validate_Abstract{
	
	const NOT_MATCH = 'postcodeNotMatch';
	
	 /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_MATCH => "'%value%' is not a valid UK post code"
    );
    
    public function isValid($value){
    	
    	$this->_setValue($value);
    	$postcode = Postcode::getPostcodeByValue($value);
    	if($postcode instanceof Postcode && !($postcode instanceof Postcode_Base) && !($postcode instanceof Postcode_Null)){
    		return true;
    	}
    	
    	return false;
    }
}