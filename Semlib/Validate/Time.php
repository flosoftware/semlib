<?php
class Semlib_Validate_Time extends Zend_Validate_Date {

	public function __construct($dateFormat = 'HH:mm:ss', $locale = 'en_GB') {
		parent::__construct($dateFormat, $locale);
    }
    
    public function isValid($value) {
    	if (parent::isValid($value) && date('H:i:s', strtotime($value)) == $value) {
    		return true;
    	} else {
    		$this->_error(self::INVALID);
    		return false;
    	}
    }
}