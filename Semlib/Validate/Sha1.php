<?php
class Semlib_Validate_Sha1 extends Zend_Validate_Regex {

	const NOT_MATCH = 'regexNotMatch';

    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_MATCH => "'%value%' does not valid sha1 string"
    );

    /**
     * @var array
     */
    protected $_messageVariables = array();

    /**
     * Regular expression pattern
     *
     * @var string
     */
    protected $_pattern = "([a-fA-F\d]{40})";

	/**
     *
     * @return void
     */
    public function __construct()
    {
    	//do nothing (dont change pattern)
    }

	public function setPattern($pattern)
    {
		//do nothing (just in case if soeone try to change pattern)
        return $this;
    }
}


