<?php
class Semlib_Filter_Word_UnderscoreToCamelCase extends Zend_Filter_Word_UnderscoreToCamelCase {
	
	/**
	 * At time of writing, the Zend_Filter_Word_UnderscoreToCamelCase
	 * is returning MixedCase instead of camelCased so this class
	 * fixes this problem
	 *
	 * @param string $value
	 */
	public function filter($value) {
		$value = (string)$value;
		$mixedCase = parent::filter($value);
		if (strlen($value) <= 1) {
			return strtolower($mixedCase);
		} else {
			return strtolower(substr($mixedCase,0,1)).substr($mixedCase, 1);
		}
	}
}