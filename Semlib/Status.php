<?php
abstract class Semlib_Status{
	protected $_user;
	protected $_statusTypeId;
	protected $_statusType;
	protected $_date;
	protected $_note;
	protected static $_statusTable = '';

	abstract public function __construct($id);

	protected function _init($userId, $statusTypeId, $date, $note = ''){
		$this->_user = $userId;
		$this->_statusTypeId = $statusTypeId;
		$this->_date = $date;
		$this->_note = $note;
	}

	public static function getStatusTypes(){
		$db = Zend_Registry::get('dbconn');
		$result = $db->execute('SELECT name FROM '.$db->real_escape_string(static::$_statusTable));
		$types = $result->fetchall_assoc();
		return array_pluck('name', $types);
	}

	public static function getTypeIdFromName($statusTypeName){
		$db = Zend_Registry::get('dbconn');
		$result = $db->execute('SELECT id FROM '.$db->real_escape_string(static::$_statusTable).' WHERE name = "'.$db->real_escape_string($statusTypeName).'"');
		if($row = $result->fetch_assoc()){
			return intval($row['id']);
		}else{
			throw new Exception('Cannot find status type with name: '.$statusTypeName);
		}
	}

	public function getStatus() {
		if(is_null($this->_statusType)){
			$db = Zend_Registry::get('dbconn');
			$result = $db->execute('SELECT name FROM '.$db->real_escape_string(static::$_statusTable).' WHERE id = '.(int)$this->_statusTypeId);
			if($row = $result->fetch_assoc()){
				$this->_statusType = $row['name'];
			}else{
				throw new Exception('Cannot find the status type with id: '.$this->_statusTypeId);
			}
		}
		return $this->_statusType;
	}


	public function getUser() {
		return lazy_load($this->_user, 'User', false);
	}

	public function getNote() {
		return $this->_note;
	}

	public function getDate() {
		return $this->_date;
	}

	public function getName(){
		return $this->getStatus();
	}

	public function getTypeId(){
		return $this->_statusTypeId;
	}

	public function __toString(){
		return $this->getName();
	}
}