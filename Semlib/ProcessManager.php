<?php
class Semlib_ProcessManager{
	protected $_maxProcesses;
	protected $_displayOutput;
	protected $_displayErrors;
	protected $_showProcessStatus;
	protected $_status = array();
	protected $_processes = array();
	protected $_runningProcesses = array();
	protected $_finishedProcesses = array();
	protected $_processCounter = 0;

	public function __construct($displayOutput = true, $displayErrors = true, $maxProcesses = null, $showProcessStatus = 60){
		if($showProcessStatus > 0){
			$this->_displayOutput = false;
			$this->_displayErrors = false;
		}else{
			$this->_displayOutput = $displayOutput;
			$this->_displayErrors = $displayErrors;
		}
		$this->_maxProcesses = $maxProcesses;
		$this->_showProcessStatus = $showProcessStatus;
	}

	public function addProcess(Semlib_Process_Interface $process){
		$this->_processes[] = $process;
		$process->setProcessManager($this);
	}

	protected function _initProcesses(){
		$count = $this->_processCounter;
		while(	isset($this->_processes[$count]) &&
				(is_null($this->_maxProcesses) || count($this->_runningProcesses) < $this->_maxProcesses)){
			$this->_processes[$count]->run();
			$this->_runningProcesses[$count] = $this->_processes[$count];
			$count++;
		}
		$this->_processCounter = $count;
	}

	protected function _dumpRunningStatuses(){
		echo "\n\nReceived interrupt (Ctr+C). Dumping process statuses and exiting:\n";
		foreach($this->_runningProcesses as $proc){
			var_dump($proc->getStatus(false));
		}
		die();
	}

	public function run(){
		declare(ticks = 1);
		pcntl_signal(SIGINT, array($this,'_dumpRunningStatuses'));
		$this->_initProcesses();
		while(count($this->_runningProcesses) > 0){
			usleep(250000);

			/* @var $process Semlib_Process */
			foreach ($this->_runningProcesses as $processId => $process){
				if($process->isRunning()){
						$this->_addStatusLine($process,"\033[1mRunning\033[0m\t\t");
						$this->_processOutput($process);
				}else{
					$this->_closeProcess($processId);
				}
			}
			foreach ($this->_finishedProcesses as $processId => $process){
				$this->_addStatusLine($process,"Finished\t");
			}
			$this->_displayStatus();
			$this->_initProcesses();
		}
	}

	protected function _displayStatus(){
		if($this->_showProcessStatus == 0){
			return;
		}elseif($this->_showProcessStatus < 0){
			$status = $this->_status;
		}else{
			$numRunningProcesses = count($this->_status);
			$end = min($numRunningProcesses, $this->_showProcessStatus);
			$status = array_reverse(array_slice($this->_status, 0, $end), true);
		}
		passthru('clear');

		echo $this->_getStatusHeader();
		echo implode("\n", $status);
		$this->_status = array();
	}

	protected function _getStatusHeader(){
		if($this->_showProcessStatus == 0){
			return;
		}
		return "\033[1mStatus\t\tTime\t\tProcess\033[0m\n";
	}

	protected function _addStatusLine(Semlib_Process $process, $status){
		if($this->_showProcessStatus == 0){
			return;
		}
		$this->_status[] = $status.Semlib_DateTime::secondsToString($process->getTime(), 3, array('min' => 60, 'sec' => 1))."\t".$process->getName();
	}

	protected function _processOutput(Semlib_Process $process){
		$output = '';
		if($this->_displayOutput){
			$output .= $process->getOutput();
		}else{
			$process->getOutput();
		}
		if($this->_displayErrors){
			$output .= $process->getErrors();
		}else{
			$process->getErrors();
		}
		if(!empty($output)){
			echo $output;
		}
	}

	protected function _closeProcess($processId){
		$this->_runningProcesses[$processId]->close();
		array_unshift($this->_finishedProcesses, $this->_runningProcesses[$processId]);
		unset($this->_runningProcesses[$processId]);
	}
}