<?php

class Semlib_Exception extends Exception {

	// so we can redefine the exception message
    public function __construct($message = '', $code = 0, Exception $previous = null) {

    	$env = Zend_Registry::get('environment');
    	$disableForApi = Zend_Registry::isRegistered('disable_backtrace');
    	if(!$disableForApi){
	    	if(!empty($env) && $env != 'live') {
		    	$message .= '<br />-------------------------------<br />';
		    	$message .= 'EXCEPTION BACKTRACE:<br />';
		    	$message .= '-------------------------------<br />';
		    	$message .= $this->getTraceAsString();
		    	$message .= '<br />-------------------------------<br />';
	    	}
    	}

        parent::__construct($message, $code, $previous);
    }
}