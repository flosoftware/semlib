<?php

/**
 * Classes that implement Persistable can interact with a
 * database as a persistable Database Access Object.
 * 
 * @author jtaylor
 * @version 0.2
 *
 */
interface Semlib_Persistable {
	
	/**
	 * Create a new database record and return the persistable object.
	 * 
	 * @param User $user the transacting user
	 * @param Semlib_Filter $valueObject a value object of data to insert
	 * @return Persistable the persistable object
	 */
	public static function create(User $user, Semlib_Filter $valueObject);
	
	/**
	 * Update the database record by persisting.
	 * 
	 * @param User $user the transacting user
	 */
	public function update(User $user);
}