<?php
function r_implode($glue, $pieces){
	$retVal = array();
	foreach($pieces as $r_pieces){
		if(is_array($r_pieces)){
			$retVal[] = r_implode($glue, $r_pieces);
		}else{
			$retVal[] = $r_pieces;
		}
	}
	return implode($glue, $retVal);
}

function trim_all($str, $extraCharsToRemove = array()){
	$charsToRemove = array_merge(array("\n", "\r", "\t", " ", "\o", "\xOB", "'",  '"', '&', '+', '(', ')', '\\', '/', '.', ','), $extraCharsToRemove);
	$str = str_replace($charsToRemove, '', $str);
	return $str;
}

function pluralise($singular = "", $plural = "", $count) {
	return (!$count || $count > 1) ? $plural : $singular;
}

function truncate($string, $max = 20) {
    if (strlen($string) <= $max) {
        return $string;
    }
    $leave = $max;
    $text = substr($string, 0, $leave);
    // dont break words.
    if($text{strlen($text)-1} != ' ') {
    	$strLen = strlen($text);
    	$newStr = '';
    	$spacePos = 0;

    	for($i = $strLen-1; $i > 0; $i--) {
    		if($text{$i} == ' ') {
    			$spacePos = $i;
    			$i = 0; // exit loop
    		}
    	}

    	if($spacePos > 0) {
    		$leave = $leave-($max-$spacePos);
    		$text = substr($string, 0, $leave);
    		$text = $text . '...';
    	}else {
    		// non spaced string
    		$text = substr($string, 0, $leave);
    	}
    	return $text;
    } else {
    	$text = substr($string, 0, $leave-1); // -1 = remove space
    	return $text . '...';
    }
}