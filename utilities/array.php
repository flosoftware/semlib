<?php
/**
 * Take input arrays and sum values for each key of the input array:
 *
 * $arr1 = array('a' => 2, 'b' => 3, 'c' => 4);
 * $arr2 = array('b' => 2, 'c' => 3, 'd' => 4);
 *
 * $result = array_add($arr1, $arr2);
 *
 * Result:
 *
 * array('a' => 2, 'b' => 5, 'c' => 7, 'd' => 4);
 *
 * @return Array
 */
function array_add(Array $a1, Array $a2){ // ...
	$aRes = $a1;
	foreach (array_slice(func_get_args(), 1) as $aRay){
		foreach (array_intersect_key($aRay, $aRes) as $key => $val){
			$aRes [$key] += $val;
		}
		$aRes += $aRay;
	}
	return $aRes;
}

function ifkey($array, $key){
	if(is_array($array) && array_key_exists($key, $array)){
		return $array[$key];
	} else if ($array instanceOf ArrayObject && $array->offsetExists($key)) {
		return $array[$key];
	}
	return null;
}

function array_sum_recursive(Array $array){
	$total = (float)0;
	foreach($array as $val){
		if(is_numeric($val)){
			$total += (float)$val;
		}elseif (is_array($val)){
			$total += array_sum_recursive($val);
		}
	}
	return $total;
}

function multiple_array_keys_exist($array, $keys){
	$keysExist = true;
	foreach ($keys as $key){
		if (!array_key_exists($key,$array)){
			trigger_error('<'.$key.'> not found in array', E_USER_WARNING);
			$keysExist = false;
		}
	}
	return $keysExist;
}

function array_pluck($key, $array){
	$toObj = false;
	if ($array instanceof ArrayObject) {
		$toObj = true;
		$array = (array)$array;
	}
    if (is_array($key) || !is_array($array)) return array();
    $funct = create_function('$e', 'return is_array($e) && array_key_exists("'.$key.'",$e) ? $e["'. $key .'"] : null;');
    $array =  array_map($funct, $array);
    if ($toObj){
    	$array = new ArrayObject($array);
    }
    return $array;
}

function array_values_are_unique($array){
	if(is_array($array)){
		return (bool)($array == array_unique($array));
	}else{
		exit;
	}	
}

//peek (to be used if treating an array as a stack)
function array_peek($stack, $where=null){
  //First see if the stack is empty
  $cnt=count($stack);
  if($cnt==0)return NULL;
  //Then set $where to top of stack if it is omitted
  if(func_num_args() <= 1)$where = $cnt-1;
  //Next make sure $where is pointing to somewhere in the stack -
  //otherwise make it point to the top of the stack
  if($where >= $cnt || $where<0)$where = $cnt-1;
  return $stack[$where];
}