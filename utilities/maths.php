<?php
function to_int($var){return (int)$var;}
//Mathematical operators as functions, useful for callbacks
function add($a, $b){ // ...
	return array_sum(func_get_args());
}
function multiply($a, $b){ // ...
	return array_reduce(func_get_args(),'rmul');
}
function subtract($a, $b){return $a - $b;}
function divide($a, $b){return $a / $b;}

function rmul($a, $b){
	$a *= $b;
	return $a;
}

/*
 * Takes a number and returns it rounded to the desired number of places, but without showing trailing zeros
 */
function significant_decimals($value, $places){
	$value = round($value, $places);
	return ((string)$value);
}