<?php
function real_to_working_days($days,$start_date = null){
	return Semlib_DateTime::realToWorkingDays($days, $start_date);
}

function working_days_between($start,$end){
	return Semlib_DateTime::workingDaysBetween($start, $end);
}

function bank_holidays($start_year, $end_year = null){
	return Semlib_DateTime::bankHolidays($start_year, $end_year);
}

function new_year($year){
	return Semlib_DateTime::newYearBankHoliday($year);
}

function easter_sunday($year){
	return Semlib_DateTime::easterSunday($year);
}

function good_friday($year){
	return Semlib_DateTime::goodFriday($year);
}

function easter_monday($year){
	return Semlib_DateTime::easterMonday($year);
}

function may_day($year){
	return Semlib_DateTime::mayDayBankHoliday($year);
}

function spring_bh($year){
	return Semlib_DateTime::springBankHoliday($year);
}

function summer_bh($year){
	return Semlib_DateTime::summerBankHoliday($year);
}

function christmas_bh($year){
	return Semlib_DateTime::christmasBankHoliday($year);
}

function boxingday_bh($year){
	return Semlib_DateTime::boxingDayBankHoliday($year);
}

function today(){
	return Semlib_DateTime::today();
}

function yesterday(){
	return Semlib_DateTime::yesterday();
}

function secondsToString($seconds){
	return Semlib_DateTime::secondsToString($seconds);
}

