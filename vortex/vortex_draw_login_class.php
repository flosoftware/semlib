<?php
class Draw_login {
	private $_trackingInclude;

	public function __construct($trackingInclude = '') {
		$this->_trackingInclude = $trackingInclude;
	}

	public function draw_head() {
		?>
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
			<title>De Poel Login</title>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<style type="text/css">
			<!--
			.loginfield {
				font-family: Arial, Helvetica, sans-serif;
				font-size: 12px;
				color: #666666;
			}
			.logindisplay {
				font-family: Verdana, Arial, Helvetica, sans-serif;
				font-size: 14px;
				color: #333333;
			}
			.message {
				font-family: Arial, Helvetica, sans-serif;
				font-size: 12px;
				color: #FF9933;
			}
			.outertable {
				border: 1px solid #000066;
			}
			.logobackground {
				background-color: #FFFFFF;
				border-bottom-width: 1px;
				border-bottom-style: solid;
				border-bottom-color: #000066;
			}
			.messagebackground {
				background-color: #FFFFFF;
				height: 50px;
			}

			.innertable {
				font-family: Arial, Helvetica, sans-serif;
				font-size: 12px;
				color: #333333;
			}

			.formfields {
				background-color: #FFFFFF;
			}
			-->
			</style>
			</head>

			<body>
			<div align="center">
		<?php

		$this->draw_start_background();

	}

	public function draw_tail() {

		$this->draw_end_background();

		?>
			</div>
		<?php
			if(!empty($this->_trackingInclude)){
				include $this->_trackingInclude;
			}
		?>
			</body>
			</html>
		<?php
	}

	public function draw_two_field_form(	$field_1,
								$field_2,
								$field_label_1,
								$field_label_2,
								$field_1_value,
								$field_2_value,
								$field_1_type,
								$field_2_type,
								$submit_value,
								$message
							) {
	?>
		<table>
		<tr>
			<td colspan="2" class="message"><?php echo $message; ?><br>&nbsp;</td>
		</tr>
		<tr><td>
			<form name="Login" method="post" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>">
			  <table width="250" border="0" cellspacing="0" cellpadding="2">
				<?php
				if($submit_value != "SEND ONE TIME PASSWORD"){
					?>
					<tr>
				  	<td width="70" class="loginfield"><?php echo $field_label_1; ?></td>
				  	<td align="right"><input name="<?php echo $field_1; ?>" type="<?php echo $field_1_type; ?>" id="<?php echo $field_1; ?>" size="20" maxlength="65" value="<?php echo $field_1_value; ?>"></td>
					</tr>
					<?php
				}
				?>
				<tr>
				  <td width="70" class="loginfield"><?php echo $field_label_2; ?></td>
				  <td align="right"><input name="<?php echo $field_2; ?>" type="<?php echo $field_2_type; ?>" id="<?php echo $field_2; ?>" size="20" maxlength="65" value="<?php echo $field_2_value; ?>"></td>
				</tr>
				<tr>
				  <td width="70">&nbsp;</td>
				  <td><div align="right">
					  	<input type="hidden" name="Submit" value="<?php echo $submit_value; ?>">
					  	<?php
					  	if ( $submit_value == "LOGIN" ) {
					  		?>
							<input type="image" src="Media/vortex/Login_button.gif" width="100" height="40" border="0">
							<?php
						} elseif ( $submit_value == "SEND ONE TIME PASSWORD" ) {
							?>
							<input type="image" src="Media/vortex/OneTimePassword_button.gif" width="171" height="40" border="0">
							<?php
						} elseif ($submit_value == "SET PASSWORD" ) {
							?>
							<input type="image" src="Media/vortex/SetPersonalPassword_button.gif" width="177" height="40" border="0">
							<?php
						}
					  ?>
					</div></td>
				</tr>
			  </table>
			</form>
		</td></tr>
		</table>
	<?php
	}

	public function draw_lost_password() {
		?>
			<form name="help" method="post" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>">
			  <table width="250" border="0" cellspacing="0" cellpadding="0">
				<tr>
				  <td width="70">&nbsp;</td>
				  <td><div align="right">

					  	<input type="hidden" name="Submit" value="LOST PASSWORD">
						<input type="image" src="Media/vortex/forgotten_password.jpg" width="300" height="26" border="0">
					  <?php
					  /**
					  * old
					  	<input type="submit" name="Submit" value="LOST PASSWORD">
					  */
					  ?>
					</div></td>
				</tr>
			  </table>
		</form>

		<?php

	}



	/**
	*
	*
	*/
	public function draw_three_field_form(	$field_1,
										$field_2,
										$field_3,
										$field_label_1,
										$field_label_2,
										$field_label_3,
										$field_1_value,
										$field_2_value,
										$field_3_value,
										$field_1_type,
										$field_2_type,
										$field_3_type,
										$submit_value,
										$message
									) {

	//draw a three field form with submit button and message (for instruction, error etc)

	?>
		<table>
			<tr><td>
				<form name="Login" method="post" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>">
					<table width="250" border="0" cellspacing="0" cellpadding="2">
						<tr>
						  <td colspan="2" class="message"><?php echo $message; ?></td>
						</tr>
						<tr>
						  <td width="70" class="loginfield"><?php echo $field_label_1; ?></td>
						  <td align="right"><input name="<?php echo $field_1; ?>" type="<?php echo $field_1_type; ?>" id="<?php echo $field_1; ?>" size="20" maxlength="60" value="<?php echo $field_1_value; ?>"></td>
						</tr>
						<tr>
						  <td width="70" class="loginfield"><?php echo $field_label_2; ?></td>
						  <td align="right"><input name="<?php echo $field_2; ?>" type="<?php echo $field_2_type; ?>" id="<?php echo $field_2; ?>" size="20" maxlength="60" value="<?php echo $field_2_value; ?>"></td>
						</tr>
						<tr>
						  <td width="70" class="loginfield"><?php echo $field_label_3; ?></td>
						  <td align="right"><input name="<?php echo $field_3; ?>" type="<?php echo $field_3_type; ?>" id="<?php echo $field_3; ?>" size="20" maxlength="60" value="<?php echo $field_3_value; ?>"></td>
						</tr>
						<tr>
						  <td width="70">&nbsp;</td>
						  <td><div align="right">
							  <input type="hidden" name="Submit" value="<?php echo $submit_value; ?>">
							  <input type="image" src="Media/vortex/set_new_user_name_button.gif" width="177" height="40" border="0">
							</div></td>
						</tr>
					</table>
				</form>
			</td></tr>
		</table>
	<?php
	}





	private function draw_start_background() {
		?>
		<table border="2" width="800" align="center" cellpadding="0" cellspacing="0" bordercolor="#666666" bgcolor="#FFFFFF">
			<tr>
			<td>
				<table width="100%"  border="0" align="center" cellpadding="5" cellspacing="5">
					<tr>
						<td>&nbsp;</td>
						<td width="400" align="left" valign="bottom"><img src="Media/vortex/LogInHere.gif" width="178" height="32" /></td>
					</tr>
					<tr>
						<td rowspan="4" align="center" valign="top"><img src="<?php echo Zend_Registry::get('branding')->login_image; ?>" /></td>
						<td align="right" valign="bottom">

							<table width="400" border="0" cellspacing="0" cellpadding="0" >
								<tr>
								<td class="formfields">

		<?php
	}

		private function draw_end_background() {
			?>

								</td>
								</tr>
								<tr>
								<td align="left" valign="middle">&nbsp;</td>
								</tr>
							</table>

						</td>
					</tr>

					<tr>
						<td align="right" valign="bottom"><img src="<?php echo Zend_Registry::get('branding')->login_logo; ?>" /></td>
					</tr>
				</table>
			</td>
			</tr>
		</table>
		<?php
	}


} //end class

?>