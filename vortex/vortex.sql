
GRANT SELECT, INSERT, UPDATE, EXECUTE ON vortex.* TO vortex_user@localhost IDENTIFIED BY '66vortex99';

GRANT SELECT, INSERT, UPDATE, EXECUTE ON vortex.* TO remote_vortex_user@'%' IDENTIFIED BY '66remotevortex99';

GRANT EXECUTE ON vortex.* TO vortex_user;





DROP PROCEDURE IF EXISTS `addUserToApplication`;
DELIMITER $$
CREATE DEFINER=`vortex_user`@`localhost` PROCEDURE `addUserToApplication`(IN in_vortex_user_id INT, IN in_AppID INT)
    MODIFIES SQL DATA
BEGIN

DECLARE hash VARCHAR(255);
DECLARE new_vortex_userID INT;
DECLARE errcheck INT DEFAULT 0;
DECLARE errmsg VARCHAR(255);

/* validate arguments */
SET errmsg = 'ERROR: ';

IF(CHAR_LENGTH(in_vortex_user_id) = 0 OR in_vortex_user_id = 0) THEN 
		SET errcheck = 1;
		SET errmsg = CONCAT(errmsg, ' - no vortex id argument');
END IF;

IF(CHAR_LENGTH(in_AppID) = 0 OR in_AppID = 0) THEN 
		SET errcheck = 1;
		SET errmsg = CONCAT(errmsg, ' - no applicationID argument');
END IF;

IF(errcheck=1) THEN 
		CALL my_signal(errmsg);
ELSE
	BEGIN

			/* process arguments based on check */

			/* insert user in userToApp table */
			INSERT INTO `staging_vortex`.`UserToApp` (`vortex_userID`, `appID`, `application_status_flag`) 
				VALUES (in_vortex_user_id, in_AppID, '1');



	END;
END IF;

END
$$




DROP PROCEDURE IF EXISTS `createVortexUser`;
DELIMITER $$
CREATE DEFINER=`vortex_user`@`localhost` PROCEDURE `createVortexUser`(IN in_vortex_email VARCHAR(255), IN in_vortex_password VARCHAR(64), IN in_AppID INT, OUT out_new_vortex_userID INT)
    MODIFIES SQL DATA
BEGIN

DECLARE hash VARCHAR(255);
DECLARE new_vortex_userID INT;
DECLARE errcheck INT DEFAULT 0;
DECLARE errmsg VARCHAR(255);

/* validate arguments */
SET errmsg = 'ERROR: ';


IF(CHAR_LENGTH(in_vortex_email) = 0) THEN 
		SET errcheck = 1;
		SET errmsg = CONCAT(errmsg, ' - no email argument');
END IF;

IF(CHAR_LENGTH(in_vortex_password) = 0) THEN 
		SET errcheck = 1;
		SET errmsg = CONCAT(errmsg, ' - no password argument');
END IF;

IF(CHAR_LENGTH(in_AppID) = 0 OR in_AppID = 0) THEN 
		SET errcheck = 1;
		SET errmsg = CONCAT(errmsg, ' - no applicationID argument');
END IF;

IF(errcheck=1) THEN 
		CALL my_signal(errmsg);
ELSE
	BEGIN

			/* process arguments based on check */
	
			/* create hash */
			SET hash = MD5(CONCAT(in_vortex_email,in_vortex_password));
	
			/* insert user - set username field with value of in_vortex_email */
			INSERT INTO `staging_vortex`.`User` (`vortex_email`, `vortex_password`, `vortex_username`, `memorable_phrase_hint_ID`, `memorable_phrase`, `vortex_login_attempts`, `vortex_status`) 
				VALUES (in_vortex_email, hash, in_vortex_email, 0, '', '25', '2');
		

			/* get new vortex user ID */
			SELECT `User`.`vortex_userID` INTO new_vortex_userID FROM `staging_vortex`.`User` WHERE `User`.`vortex_email` = in_vortex_email;
			
			/* log user creation */
			INSERT INTO `staging_vortex`.`User Creation Log` (`vortex_id`,`user_email`,`one_time_password`,`username`,`entry_time`)
				VALUES (new_vortex_userID,in_vortex_email,in_vortex_password, in_vortex_email, NOW());


			/* insert user in userToApp table */
			INSERT INTO `staging_vortex`.`UserToApp` (`vortex_userID`, `appID`, `application_status_flag`) 
				VALUES (new_vortex_userID, in_AppID, '1');

			/* return vortex user ID */

			SET out_new_vortex_userID = new_vortex_userID;


	END;
END IF;

END
$$



DROP PROCEDURE IF EXISTS `get_vortex_id`;
DELIMITER $$
CREATE DEFINER=`vortex_user`@`localhost` PROCEDURE `get_vortex_id`(IN in_vortex_email VARCHAR(255), OUT out_vortex_user_id INT)
BEGIN
DECLARE found_vortex_userID INT;

	SELECT `User`.`vortex_userID` INTO found_vortex_userID FROM `staging_vortex`.`User` WHERE `User`.`vortex_email` = in_vortex_email;

SET out_vortex_user_id = found_vortex_userID;


END
$$



DROP PROCEDURE IF EXISTS `insert_user`;
DELIMITER $$
CREATE DEFINER=`vortex_user`@`localhost` PROCEDURE `insert_user`(IN in_vortex_email VARCHAR(255), IN in_vortex_password VARCHAR(64), IN in_AppID INT, OUT out_new_vortex_userID INT)
    MODIFIES SQL DATA
BEGIN

DECLARE hash VARCHAR(255);
DECLARE new_vortex_userID INT;
DECLARE errcheck INT DEFAULT 0;
DECLARE errmsg VARCHAR(255);

/* validate arguments */
SET errmsg = 'ERROR: ';


IF(CHAR_LENGTH(in_vortex_email) = 0) THEN 
		SET errcheck = 1;
		SET errmsg = CONCAT(errmsg, ' - no email argument');
END IF;

IF(CHAR_LENGTH(in_vortex_password) = 0) THEN 
		SET errcheck = 1;
		SET errmsg = CONCAT(errmsg, ' - no password argument');
END IF;

IF(CHAR_LENGTH(in_AppID) = 0 OR in_AppID = 0) THEN 
		SET errcheck = 1;
		SET errmsg = CONCAT(errmsg, ' - no applicationID argument');
END IF;

IF(errcheck=1) THEN 
		CALL my_signal(errmsg);
ELSE
	BEGIN

			/* process arguments based on check */
	
			/* create hash */
			SET hash = MD5(CONCAT(in_vortex_email,in_vortex_password));
	
			/* insert user - set username field with value of in_vortex_email */
			INSERT INTO `staging_vortex`.`User` (`vortex_email`, `vortex_password`, `vortex_username`, `memorable_phrase_hint_ID`, `memorable_phrase`, `vortex_login_attempts`, `vortex_status`) 
				VALUES (in_vortex_email, hash, in_vortex_email, 0, '', '25', '2');
		

			/* get new vortex user ID */
			SELECT `User`.`vortex_userID` INTO new_vortex_userID FROM `staging_vortex`.`User` WHERE `User`.`vortex_email` = in_vortex_email;

			/* insert user in userToApp table */
			INSERT INTO `staging_vortex`.`UserToApp` (`vortex_userID`, `appID`, `application_status_flag`) 
				VALUES (new_vortex_userID, in_AppID, '1');

			/* return vortex user ID */

			SET out_new_vortex_userID = new_vortex_userID;


	END;
END IF;

END
$$




DROP PROCEDURE IF EXISTS `my_signal`;
DELIMITER $$
CREATE DEFINER=`vortex_user`@`localhost` PROCEDURE `my_signal`(IN in_errortext VARCHAR(255))
BEGIN
	SET @sql=CONCAT('UPDATE `', in_errortext, '` SET x=1');
	PREPARE my_signal_stmt FROM @sql;
	EXECUTE my_signal_stmt;
	DEALLOCATE PREPARE my_signal_stmt;
END
$$



DROP PROCEDURE IF EXISTS `register_user_with_app`;
DELIMITER $$
CREATE DEFINER=`vortex_user`@`localhost` PROCEDURE `register_user_with_app`(IN in_vortex_id INT, IN in_AppID INT)
    MODIFIES SQL DATA
BEGIN

DECLARE errcheck INT DEFAULT 0;
DECLARE errmsg VARCHAR(255);

/* validate arguments */
SET errmsg = 'ERROR: ';


IF(CHAR_LENGTH(in_vortex_id) = 0) THEN 
		SET errcheck = 1;
		SET errmsg = CONCAT(errmsg, ' - no vortex id argument');
END IF;

IF(CHAR_LENGTH(in_AppID) = 0 OR in_AppID = 0) THEN 
		SET errcheck = 1;
		SET errmsg = CONCAT(errmsg, ' - no applicationID argument');
END IF;

IF(errcheck=1) THEN 
		CALL my_signal(errmsg);
ELSE
	BEGIN

			/* process arguments based on check */
			
			/* revoke any records that already refer to this vortex id and this application */
			UPDATE  `staging_vortex`.`UserToApp` SET `UserToApp`.`application_status_flag` = '0'  
			WHERE `UserToApp`.`vortex_userID` = in_vortex_id AND `UserToApp`.`appID` = in_AppID;
			
			/* insert user in userToApp table */
			INSERT INTO `staging_vortex`.`UserToApp` (`vortex_userID`, `appID`, `application_status_flag`) 
				VALUES (in_vortex_id, in_AppID, '1');

	END;
END IF;

END
$$




DROP PROCEDURE IF EXISTS `revoke_user`;
DELIMITER $$
CREATE DEFINER=`vortex_user`@`localhost` PROCEDURE `revoke_user`(in_vortex_userID INT, in_appID INT)
    MODIFIES SQL DATA
BEGIN

DECLARE errcheck INT DEFAULT 0;
DECLARE errmsg VARCHAR(255);

SET errmsg = 'ERROR: ';

IF(CHAR_LENGTH(in_vortex_userID) = 0 ) THEN 
		SET errcheck = 1;
		SET errmsg = CONCAT(errmsg, ' - no vortex user ID argument');
END IF;

IF(in_vortex_userID <= 0) THEN 
		SET errcheck = 1;
		SET errmsg = CONCAT(errmsg, ' - vortex user ID is not a +ve integer');
END IF;

IF(CHAR_LENGTH(in_appID) = 0) THEN 
		SET errcheck = 1;
		SET errmsg = CONCAT(errmsg, ' - no application ID argument');
END IF;


IF(errcheck=1) THEN 
		CALL my_signal(errmsg);
ELSE
	BEGIN

		UPDATE  `staging_vortex`.`UserToApp` SET `UserToApp`.`application_status_flag` = '0'  
		WHERE `UserToApp`.`vortex_userID` = in_vortex_userID AND `UserToApp`.`appID` = in_appID;
	END;
END IF;

END
$$




DROP PROCEDURE IF EXISTS `update_email`;
DELIMITER $$
CREATE DEFINER=`vortex_user`@`localhost` PROCEDURE `update_email`(in_email VARCHAR(255), in_vortex_userID INT, in_application VARCHAR(30))
    MODIFIES SQL DATA
BEGIN

DECLARE temp_vortex_email VARCHAR(255);
DECLARE errcheck INT DEFAULT 0;
DECLARE errmsg VARCHAR(255);
DECLARE count_email INT DEFAULT 0;

SET errmsg = 'ERROR: ';

IF(CHAR_LENGTH(in_email) = 0) THEN 
		SET errcheck = 1;
		SET errmsg = CONCAT(errmsg, ' - no email argument');
END IF;

IF(in_vortex_userID <= 0) THEN 
		SET errcheck = 1;
		SET errmsg = CONCAT(errmsg, ' - vortex user ID is not a +ve integer');
END IF;

IF(CHAR_LENGTH(in_application) = 0) THEN 
		SET errcheck = 1;
		SET errmsg = CONCAT(errmsg, ' - no application argument');
END IF;

/* first get any records with same email - shouldn't be any*/
SELECT COUNT(`User`.`vortex_email`) INTO count_email FROM `vortex`.`User` WHERE `User`.`vortex_email` = in_email AND `User`.`vortex_userID` != in_vortex_userID AND vortex_status != 0;

IF (count_email > 0 ) THEN
	SET errcheck = 1;
	SET errmsg = CONCAT(errmsg, ' - email already exists');
END IF;

IF(errcheck=1) THEN 
		CALL my_signal(errmsg);
ELSE
	BEGIN

		
		/* close any open email_change_logs */
		UPDATE  `vortex`.`email_change_log` SET `email_change_notification_status` = '0'  
		WHERE `email_change_log`.`vortex_userID` = in_vortex_userID AND `email_change_notification_status` = '1';

		/* get old email */
		SELECT `User`.`vortex_email` INTO temp_vortex_email FROM `vortex`.`User` WHERE `user`.`vortex_userID` = in_vortex_userID;


		/* update email within vortex */
		UPDATE `vortex`.`User` SET `User`.`vortex_email` = in_email WHERE `user`.`vortex_userID` = in_vortex_userID;

		/* update email in other applications - add as reuired */

		/* local etips 2 test database */
		/* UPDATE `local_etips2_votex-ed`.`user` SET `user`.`user_email` = in_email WHERE `user`.`vortex_userID` = in_vortex_userID; */

		/* local sub-e test database */
		/* UPDATE `local_sub-e_vortex-ed`.`User` SET `User`.`eMail` = in_email WHERE `User`.`vortex_userID` = in_vortex_userID; */

		/* insert open email change into log */
		INSERT INTO `vortex`.`email_change_log` (`vortex_userID`,`old_email`,`new_email`,`initiating_application`, `date`, `email_change_notification_status`) 
		VALUES(in_vortex_userID, temp_vortex_email, in_email, in_application, NOW(), '1');
	END;
END IF;

END
$$


