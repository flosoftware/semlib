<?php
/**
 * Login Class - version 3
 * This comment lies
 * Limited to two fields - username and password
 * User can request one time password
 * User can change their username
 * User cannot change their email
 * Hashed password based on the user's username as seed
 * Usernames must be unique within the user database
 *
 */

class Vortex_Login {
	public $db_login;
	public $gateway_page_url;
	public $passwords_hashed_flag;
	public $logout_note;
	public $passThruData;
	public $userData;
	private $user_action;
	private $PASSWORD_FIELD_LENGTH = 16;
	private $MIN_PASSWORD_LENGTH = 8;
	private $URL;
	private $ADMIN_EMAIL;
	private $dbobj;
	private $_trackingInclude;
	protected $drawobj;

	/**
	 * __construct()
	 * @access public
	 * @param $db_login {db_login object}
	 * @param $drawobj {draw object}
	 *
	 */
	public function __construct($dbobj, $trackingInclude = '', $drawLogin = false) {
		$this->_trackingInclude = $trackingInclude;
		//set properties
		$this->db_login = new DB_login($dbobj); //login's database object
		$this->dbobj = $dbobj;
		$this->URL = ''; //absolute URL to login
		$this->ADMIN_EMAIL = 'systems@depoel.co.uk'; //admin's email address
		$this->handleGetData(); //get any pass thru data and put in session
		$this->handlePostData(); //store data sent by user in POST variable
		$this->drawobj = new Draw_login($trackingInclude);
		if($drawLogin){
			$this->drawobj->draw_head();
		}
		$this->action($drawLogin);
		if($drawLogin){
			$this->drawobj->draw_tail();
		}
	}


	protected function _init() {
		if ( !isset($_SESSION['vortex_userID']) ) {
			header("location: login.php?logout&logoutnote=Session%20not%20started%20or%20no%20applications%20established.");
		} else {
			if ( isset($_SESSION['passThruData']) ) {
				$passThruData = $_SESSION['passThruData'];
			}
			$s = "";
			if ( isset($passThruData) && is_array($passThruData) ) {
				$s = "?";
				foreach ($passThruData as $k=>$v) {
					$s.= $k."=".$v."&";
				}
				$s = substr($s, 0, strlen($s)-1); //lop off last &
			}
			header('Location: /index.php'.$s);
			die();
		}
	}

	private function action($drawLogin = true) {
		switch ($this->user_action){
			case "LOGIN" :
				$user = $this->db_login->validate_user_login($this->userData['Username'], $this->hash_password($this->userData['Username'], $this->userData['Password']));
				if ($user !== false){
					$this->processValidUser($user, $drawLogin);
				}else{
					$this->processInvalidUser($drawLogin);
				}
				break;
			case "LOST PASSWORD" :
				$field_1 = "MemorablePhrase";
				$field_2 = "Email";
				$field_label_1 = "Memorable Phrase";
				$field_label_2 = "Email";
				$field_1_value = "";
				$field_2_value = "";
				$field_1_type = "text";
				$field_2_type = "text";
				$submit_value = "SEND ONE TIME PASSWORD";
				$message = "Please enter your email address to recieve a one-time password";
				if($drawLogin){
					$this->drawobj->draw_two_field_form($field_1, $field_2, $field_label_1, $field_label_2, $field_1_value, $field_2_value, $field_1_type, $field_2_type, $submit_value, $message);
				}
				break;
			case "SEND ONE TIME PASSWORD" :
				$user = $this->db_login->validate_email($this->userData['Email']);
				if ($user !== false){
					$this->db_login->changeStatus($user[$this->db_login->get_user_ID_field()], '2');
					$oneTimePassword = $this->randomPassword(8);
					$hash = $this->hash_password($user[$this->db_login->get_username_field()], $oneTimePassword);
					$this->db_login->update_password_field($user[$this->db_login->get_user_ID_field()], $hash);
					$email_sent = $this->emailPassword($user[$this->db_login->get_username_field()], $user[$this->db_login->get_email_field()], $oneTimePassword);
					if ($email_sent){
						$message = "When you receive your one time password by email please login";
					}else{
						$message = "A one time password has been created but the process of emailing it to you failed."
									."<br />Please try generating a new one time password.<br />If the problem persists please contact support at " . $this->ADMIN_EMAIL;
					}
					$field_1 = "Username";
					$field_2 = "Password";
					$field_label_1 = "User name";
					$field_label_2 = "Password";
					$field_1_value = "";
					$field_2_value = "";
					$field_1_type = "text";
					$field_2_type = "password";
					$submit_value = "LOGIN";
					if($drawLogin){
						$this->drawobj->draw_two_field_form($field_1, $field_2, $field_label_1, $field_label_2, $field_1_value, $field_2_value, $field_1_type, $field_2_type, $submit_value, $message);
					}
				}else{
					$field_1 = "MemorablePhrase";
					$field_2 = "Email";
					$field_label_1 = "Memorable Phrase";
					$field_label_2 = "Email";
					$field_1_value = "";
					$field_2_value = "";
					$field_1_type = "text";
					$field_2_type = "text";
					$submit_value = "SEND ONE TIME PASSWORD";
					$message = "NO username / email match found; please try again";
					if($drawLogin){
						$this->drawobj->draw_two_field_form($field_1, $field_2, $field_label_1, $field_label_2, $field_1_value, $field_2_value, $field_1_type, $field_2_type, $submit_value, $message);

					}
				}
				break;
			case "SET PASSWORD" :
				$userID = $_SESSION[$this->db_login->get_user_ID_field()];
				if ($this->matchPasswords($this->userData['Password'], $this->userData['Password2'])){
					if ($this->validatePassword($this->userData['Password'], $this->PASSWORD_FIELD_LENGTH, $this->MIN_PASSWORD_LENGTH)){
						$username = $this->db_login->get_username($userID);
						$hash = $this->hash_password($username[$this->db_login->get_username_field()], $this->userData['Password']);
						$this->db_login->update_password_field($userID, $hash);
						$this->db_login->changeStatus($userID, 1);
						$_SESSION['user_status'] = 1;
						$this->_init();
					}else{
						$field_1 = "Password";
						$field_2 = "Password2";
						$field_label_1 = "Password";
						$field_label_2 = "Re-Type Password";
						$field_1_value = "";
						$field_2_value = "";
						$field_1_type = "password";
						$field_2_type = "password";
						$submit_value = "SET PASSWORD";
						$_SESSION['set_new_password'] = true;
						$message = "Please ensure both passwords are typed the same, are at least 8 characters long,"
									." no more than 16 and are a random combination of alphabetical characters and numerals.";
						if($drawLogin){
							$this->drawobj->draw_two_field_form($field_1, $field_2, $field_label_1, $field_label_2, $field_1_value, $field_2_value, $field_1_type, $field_2_type, $submit_value, $message);
						}
					}

				}else{
					$field_1 = "Password";
					$field_2 = "Password2";
					$field_label_1 = "Password";
					$field_label_2 = "Re-Type Password";
					$field_1_value = "";
					$field_2_value = "";
					$field_1_type = "password";
					$field_2_type = "password";
					$submit_value = "SET PASSWORD";
					$_SESSION['set_new_password'] = true;
					$message = "Please ensure both passwords are typed the same.";
					if($drawLogin){
						$this->drawobj->draw_two_field_form($field_1, $field_2, $field_label_1, $field_label_2, $field_1_value, $field_2_value, $field_1_type, $field_2_type, $submit_value, $message);
					}
				}
				break;
			case "CHANGE USER NAME" :
				$field_1 = "oldUserName";
				$field_2 = "newUserName";
				$field_3 = "password";
				$field_label_1 = "Old user name";
				$field_label_2 = "New user name";
				$field_label_3 = "Password";
				$field_1_value = "";
				$field_2_value = "";
				$field_3_value = "";
				$field_1_type = "text";
				$field_2_type = "text";
				$field_3_type = "password";
				$submit_value = "SET NEW USER NAME";
				$message = "Please enter your current user name, your choice for new user name and confirm by entering your password.";
				if($drawLogin){
					$this->drawobj->draw_three_field_form($field_1, $field_2, $field_3, $field_label_1, $field_label_2, $field_label_3, $field_1_value, $field_2_value, $field_3_value, $field_1_type, $field_2_type, $field_3_type, $submit_value, $message);
				}
				break;
			default :
				$field_1 = "Username";
				$field_2 = "Password";
				$field_label_1 = "User name";
				$field_label_2 = "Password";
				$field_1_value = "";
				$field_2_value = "";
				$field_1_type = "text";
				$field_2_type = "password";
				$submit_value = "LOGIN";
				$message = "";
				if (isset($this->logout_note)){
					$message .= $this->logout_note . "<br />";
				}
				$message .= "";
				if($drawLogin){
					$this->drawobj->draw_two_field_form($field_1, $field_2, $field_label_1, $field_label_2, $field_1_value, $field_2_value, $field_1_type, $field_2_type, $submit_value, $message);
					$this->drawobj->draw_lost_password();
				}
				break;
		}
		if(isset($message)){
			$_SESSION['message'] = $message;
		}
	}

	/**
	 * logout()
	 * @access public
	 *
	 */
	public function logout() {
		$_SESSION = array();
		if (isset($_COOKIE[session_name()])){
			setcookie(session_name(), "", 0, '/');
		}
		session_destroy();
		header("Location: " . $_SERVER['SCRIPT_NAME'] . "?logoutnote=" . $this->logout_note);
		exit();
	}

	/**
	 * private handleGetData() {accesses the global GET array and checks if a logout command is issued else copies array in object
	 * @access private
	 *
	 */
	private function handleGetData() {
		if (!empty($_GET)){
			if (isset($_GET['logoutnote'])){
				$this->logout_note = $_GET['logoutnote'];
			}
			if (isset($_GET['Logout']) || isset($_GET['logout']) || isset($_GET['LOGOUT'])){
				$this->logout();
			}
			foreach ($_GET as $k => $v){
				if (($k != 'logoutnote') && ($k != 'logout') && ($k != 'Logout') && ($k != 'LOGOUT')){
					$this->passThruData[$k] = $v;
				}
			}
			$_SESSION['passThruData'] = $this->passThruData;
		}
	}

	/**
	 * private writePassThruData()
	 * @access private
	 * @deprecated {no longer required as pass thru data is handled in session
	 *
	 */
	private function writePassThruData() {
		$s = "";
		if (is_array($this->passThruData) && isset($this->passThruData)){
			foreach ($this->passThruData as $k => $v){
				$s .= $k . "=" . $v . "&";
			}
			$s = substr($s, 0, strlen($s) - 1);
		}
		return $s;
	}

	/**
	 * handlePostData() { checks for user action on submit button and copies POST array to object
	 * @access private
	 *
	 *
	 */
	private function handlePostData() {
		//reads $_POST and stores in an array $this->userData
		if (!empty($_POST)){
			//store user's action carried in value of login submit buttons
			$this->user_action = "";
			if (isset($_POST["Submit"])){
				$this->user_action = strtoupper($_POST["Submit"]);
			}
			foreach ($_POST as $k => $v){
				if ($k != "Submit" && $k != "token" && $k != "y" && $k != "x"){
					$this->userData[$k] = trim($v);
				}
			}
		}

	}

	/**
	 * randomPassword() { generates a random password of specified length }
	 * @access private
	 * @param $length {length of password }
	 *
	 */
	private function randomPassword($length) {
		return generate_password($length);
	}

	/**
	 * hash_password() {generate md5 hash password from username and plain password
	 * @access private
	 * @param $username {user name string}
	 * @param $plain_password {plain password string}
	 * @return {string}
	 */
	private function hash_password($username, $plain_password) {
		return md5($username . $plain_password);
	}

	/**
	 * processValidUser()
	 * @access private()
	 * @param $valid_user { valid user array }
	 *
	 */
	private function processValidUser($valid_user, $drawLogin = true) {
		$update = $this->db_login->update_attempts_field($valid_user[$this->db_login->get_user_ID_field()], 25);
		$_SESSION['vortex_userID'] = $valid_user[$this->db_login->get_user_ID_field()];
		$_SESSION['vortex_email'] = $valid_user[$this->db_login->get_email_field()];
		$_SESSION['user_status'] = $valid_user[$this->db_login->get_status_field()];
		$ipAddress = $_SERVER['REMOTE_ADDR'];
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		$log = "Logged in";
		$this->db_login->log_entry($_SESSION['vortex_userID'], $log, $ipAddress, $user_agent);
		if ($valid_user[$this->db_login->get_status_field()] == 1){
			$this->_init();
		}elseif ($valid_user[$this->db_login->get_status_field()] == 2){
			$field_1 = "Password";
			$field_2 = "Password2";
			$field_label_1 = "Password";
			$field_label_2 = "Re-Type Password";
			$field_1_value = "";
			$field_2_value = "";
			$field_1_type = "password";
			$field_2_type = "password";
			$submit_value = "SET PASSWORD";
			$message = "Welcome back.<br />Please set a new password to replace the one time password.";
			$_SESSION['set_new_password'] = true;
			if($drawLogin){
				$this->drawobj->draw_two_field_form($field_1, $field_2, $field_label_1, $field_label_2, $field_1_value, $field_2_value, $field_1_type, $field_2_type, $submit_value, $message);
			}
		}
		$_SESSION['message'] = $message;
	}

	/**
	 * processInvalidUser()
	 * @access private
	 *
	 */

	private function processInvalidUser($drawLogin = true) {
		try{
			$update = $this->db_login->decrement_attempts_field($this->userData['Username']);
			$field_1 = "Username";
			$field_2 = "Password";
			$field_label_1 = "User name";
			$field_label_2 = "Password";
			$field_1_value = "";
			$field_2_value = "";
			$field_1_type = "text";
			$field_2_type = "password";
			$submit_value = "LOGIN";
			$user = $this->db_login->validate_email($this->userData['Username']);
			if ($user !== false){
				$message = '<form name="Login" method="post" action="/login.php" style="display:inline; margin:0px;">
					<input name="Email" type="hidden" id="Email" value="' . $this->userData['Username'] . '">
					<input type="hidden" name="Submit" value="SEND ONE TIME PASSWORD">
					<div style="float:left;"><br>
						Details provided were incorrect &nbsp; &nbsp; </div><input type="image" src="Media/vortex/OneTimePassword_button.gif" width="171" height="40" border="0">
					</div>
				</form>';
			}else{
				$message = "<br>Details provided were incorrect - please try again<br>&nbsp;";
			}
			$_SESSION['message'] = $message;
			if($drawLogin){
				$this->drawobj->draw_two_field_form($field_1, $field_2, $field_label_1, $field_label_2, $field_1_value, $field_2_value, $field_1_type, $field_2_type, $submit_value, $message);
				$this->drawobj->draw_lost_password();
			}
		}catch (loginException $e){
			print $e;
		}
	}

	/**
	 * emailPassword()
	 * @access private
	 * @param $username {string}
	 * @param $email {string}
	 * @param $oneTimePassword {string}
	 * @return {boolean}
	 */
	private function emailPassword($username, $email, $oneTimePassword) {
		$BCC = "";
		$emailbody = "NEW ONE TIME PASSWORD INFO";
		$emailbody .= "\n\nURL: ".($_SERVER['HTTPS']?'https://':'http://').$_SERVER['HTTP_HOST'];
		$emailbody .= "\n\nUsername: " . $username;
		$emailbody .= "\n\nOne-time password: " . $oneTimePassword;
		$emailbody .= "\n\nOn logging-in you will be required to enter a new password and confirm it.";
		$emailbody .= "\n\nPlease choose a password made of a mixture of numbers and characters.";
		$emailbody .= "\n\nAPPLICATION SUPPORT";
		$emailbody .= "\n\nIf you have any queries or concerns please call ".Zend_Registry::get('branding')->company_support_number." for the support team, who will be able to help.";
		$mailheaders = "From: ".Zend_Registry::get('branding')->company_email."\r\nBcc:".$BCC;
		return mail($email, "New one time password for ".Zend_Registry::get('branding')->application_name, $emailbody, $mailheaders);
	}

	/**
	 * matchPasswords() {test two passwords are identical}
	 * @param $p1 {first instance of password used}
	 * @param $p2 {second instance of password used}
	 * @return boolean
	 */
	private function matchPasswords($p1, $p2) {
		return ($p1 === $p2);
	}

	/**
	 * validatePassword()
	 * @param str $field {contents of password field}
	 * @param str $PASSWORD_FIELD_LENGTH {character length of password field - maximum legnth of password}
	 * @param str $MIN_PASSWORD_LENGTH {minimum allowed length of password}
	 *
	 */
	function validatePassword($field, $PASSWORD_FIELD_LENGTH, $MIN_PASSWORD_LENGTH) {
		//allow between 8 and 16 chars made up from 0-9, A-Z, a-z, hyphen, single quote, double quote, underscore, fullpoint
		$reg_expr = '/^[0-9A-Za-z\'-._"]{' . $MIN_PASSWORD_LENGTH . ',' . $PASSWORD_FIELD_LENGTH . '}$/';
		$bool0 = preg_match($reg_expr, $field);
		// must not be all digits
		$bool1 = !preg_match('/^[0-9]+$/', $field);
		//must not be alpha only
		$bool2 = !preg_match('/^[A-Za-z]+$/', $field);
		return ($bool0 && $bool1 && $bool2);
	}
}
?>