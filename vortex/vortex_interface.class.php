<?php
class Vortex_Interface {
	private $_mysqli;
	private $ADMIN_EMAIL = "systems@depoelconsulting.com"; //admin's email

	/**
	 * Vortex_Interface
	 *
	 */
	public function __construct(DB_Mysql $dbobj) {
		$this->_mysqli = $dbobj->get_dbh();
	}


	/**
	 * create_user_in_vortex()
	 * @param str $email 255 char max email
	 * @param str $pass plain password
	 * @param int $application application id as integer
	 * @return int vortex user id or negative error number -1 means connection error; -2 means execution error
	 */
	public function create_user_in_vortex($email, $pass="", $application) {
		$mysqli = $this->_mysqli;
		if ($pass == "") {
			$pass = $this->randomPassword(8);
		}
		//test if user exists
		$query = "CALL get_vortex_id(?, @vortex_user_id)";
		$stmt = $mysqli->stmt_init();
		$stmt->prepare($query);
		$stmt->bind_param('s', $email);
		//execute the stored procedure
		$stmt->execute();

		if($mysqli->errno) {
			return -2;
		} else {
			$results = $mysqli->query("SELECT @vortex_user_id AS vortex_user_id");
			$row = $results->fetch_object();
		}

		if ($row->vortex_user_id == 0 || $row->vortex_user_id == NULL) {
			$query2 = "CALL createVortexUser(?,?,?, @out_new_votex_userID )";
			$stmt2 = $mysqli->stmt_init();
			$stmt2->prepare($query2);
			$stmt2->bind_param('ssd', $email, $pass, $application);

			//execute the stored procedure
			$stmt2->execute();
			if($mysqli->errno) {
				return -2;
			} else {
				$results2 = $mysqli->query("SELECT @out_new_votex_userID AS out_new_votex_userID");
				$row2 = $results2->fetch_object();
				$this->emailPassword($email, $email, $pass);
				return $row2->out_new_votex_userID;
			}
		} elseif($row->vortex_user_id > 0) {
			$this->emailWelcome($email, Zend_Registry::get('branding')->application_name);
			return $row->vortex_user_id;
		}
	}

	/**
	 * randomPassword()
	 * @param int $length number of characters in password
	 * @return str $pass random plain text password
	 */
	private function randomPassword($length) {
		return generate_password($length);
	}

	/**
	* emailPassword()
	* @access private
	* @param $username {string}
	* @param $email {string}
	* @param $oneTimePassword {string}
	* @return {boolean}
	*/
	private function emailPassword($username, $email, $oneTimePassword) {
		$BCC="supportservices@depoelconsulting.com";
		$emailbody  = "LOG IN INFORMATION";
		$emailbody .= "\n\nURL: ".($_SERVER['HTTPS']?'https://':'http://').$_SERVER['HTTP_HOST'];
		$emailbody .= "\n\nUsername: ".$username;
		$emailbody .= "\n\nOne-time password: ".$oneTimePassword;
		$emailbody .= "\n\nOn logging-in you will be required to enter a new password and confirm it.";
		$emailbody .= "\n\nPlease choose a password made of a mixture of numbers and characters.";
		$emailbody .= "\n\nAPPLICATION SUPPORT";
		$emailbody .= "\n\nIf you have any queries or concerns please call ".Zend_Registry::get('branding')->company_support_number." for the support team, who will be able to help.";
		//$emailbody .= "\n\nTo login please click link below (or paste it into your browser).";
		//$emailbody .= "\n\n".$this->URL;
		$emailbody .= "\n\n";
        
        $mail = new Zend_Mail();
        $mail->setFrom(Zend_Registry::get('branding')->company_email, 'Support Services');
        $mail->setSubject('Welcome to ' . Zend_Registry::get('branding')->application_name);
        $mail->setBodyText($emailbody);
        $mail->addTo($email, $email);
        $mail->addBcc($BCC);
        return $mail->send();
	}


	/**
	 * emailWelcome()
	 * @access private
	 * @param str $mail user's email address
	 * @param int $application
	 * @return boolean
	 */
	private function emailWelcome($email, $application) {
		
		$BCC="supportservices@depoelconsulting.com";
		$emailbody  = "FAO: ".$email;
		$emailbody .= "\n\nYou have been registered with ".$application.".\n\n";
		$emailbody .= "\n\nIf you have any questions please contact your administrator.";
		//$emailbody .= "\n\nTo login please click link below (or paste it into your browser).";
		//$emailbody .= "\n\n".$this->URL;
		$emailbody .= "\n\n";
		
		$mail = new Zend_Mail();
        $mail->setFrom(Zend_Registry::get('branding')->company_email, 'Support Services');
        $mail->setSubject('Welcome to ' . Zend_Registry::get('branding')->application_name);
        $mail->setBodyText($emailbody);
        $mail->addTo($email, $email);
        $mail->addBcc($BCC);
        return $mail->send();
	}

	/**
	* reissue_password_in_vortex
	* @param int $vortex_id
	* @param string $email
	* @param string $user_name
	*
	*/
	public function reissue_password_in_vortex($vortex_id, $email, $user_name) {
		$mysqli = $this->_mysqli;
		$plain_oneTimePassword = $this->randomPassword(8);

		//test if user exists
		$query = "CALL reissue_password(?,?,?,@out_vortex_username)";
		$stmt = $mysqli->stmt_init();
		$stmt->prepare($query);
		$stmt->bind_param('dss', $vortex_id, $email, $plain_oneTimePassword);

		//execute the stored procedure
		$stmt->execute();
		if($mysqli->errno) {
			$error = -2;
			return $error;
		} else {
			$result = $mysqli->query("SELECT @out_vortex_username AS out_vortex_username");
			$row = $result->fetch_object();
			$username = $row->out_vortex_username;
			return $this->emailPassword($username, $email, $plain_oneTimePassword);
		}
	}
}