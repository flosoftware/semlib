<?php
// database classes
class DB_login {
	/* @var DB_Mysql $dbobj */
	private $dbobj;

	private $user_table = "User";
	private $user_ID_field = "vortex_userID";
	private $email_field = "vortex_email";
	private $username_field = "vortex_username";
	private $password_field = "vortex_password";
	private $status_field = "vortex_status";
	private $login_attempts_field = "vortex_login_attempts";
	private $memorable_phrase_field = "memorable_phrase";
	private $memorable_phrase_hint_ID_field = "memorable_phrase_hint_ID";

	public function __construct(DB_Mysql $dbobj) {
		$this->dbobj = $dbobj;
	}

	public function get_user_ID_field() {
		return $this->user_ID_field;
	}

	public function get_status_field() {
		return $this->status_field;
	}

	public function get_username_field() {
		return $this->username_field;
	}

	public function get_email_field() {
		return $this->email_field;
	}

	public function get_memorable_phrase_field() {
		return $this->memorable_phrase_field;
	}

	public function get_memorable_phrase_hint_ID_field() {
		return $this->memorable_phrase_hint_ID_field;
	}

	public function validate_user_login($Username, $Password) {
		if(!basic_email_validation($Username)){
			return false;
		}
		//if user is valid return their userID else false
		$query = "SELECT " . $this->email_field . ",
				" . $this->user_ID_field . ",
				" . $this->status_field . ",
				" . $this->login_attempts_field . ",
				" . $this->memorable_phrase_field . ",
				" . $this->memorable_phrase_hint_ID_field . "
				FROM User
				WHERE " . $this->username_field . " ='" . $this->dbobj->real_escape_string($Username) . "'
				AND " . $this->password_field . " ='" . $this->dbobj->real_escape_string($Password) . "'
				AND " . $this->login_attempts_field . " > '0'
				AND " . $this->status_field . " != '0'";
		$result = $this->dbobj->execute($query);
		if ($result->fetch_num_rows() == 1) {
			$valid_user = $result->fetch_assoc();
			return $valid_user;
		} else {
			//we have no valid user or there are several users with same username and password.
			return false;
		}
	}

	public function validate_new_user_name($newUserName) {
		if(!basic_email_validation($newUserName)){
			return false;
		}
		//check if new user name exists in database against live users
		$query = "SELECT " . $this->username_field . "
				FROM User
				WHERE " . $this->username_field . " ='" . $this->dbobj->real_escape_string($newUserName) . "'
				AND " . $this->status_field . " != '0'";
		$result = $this->dbobj->execute($query);
		return ($result->fetch_num_rows() > 0);
	}

	public function validate_username_email($Username, $Email) {
		if(!basic_email_validation($Username)
			|| !basic_email_validation($Email)){
			return false;
		}
		//if user is valid return their userID else false
		$query = "SELECT " . $this->user_ID_field . ", " . $this->email_field . ", " . $this->username_field . "
				FROM User
				WHERE " . $this->username_field . " ='" . $this->dbobj->real_escape_string($Username) . "'
				AND " . $this->email_field . " ='" . $this->dbobj->real_escape_string($Email) . "'
				AND " . $this->login_attempts_field . " > '0'
				AND " . $this->status_field . " != '0'";
		$result = $this->dbobj->execute($query);
		if ($result->fetch_num_rows() == 1) {
			$valid_user = $result->fetch_assoc();
			return $valid_user;
		} else {
			//we have no valid user or there are several users with same username and password.
			return false;
		}
	}

	public function validate_email($Email) {
		if(!basic_email_validation($Email)){
			return false;
		}
		//if user is valid return their userID else false
		$query = "SELECT " . $this->user_ID_field . ",
						" . $this->email_field . ",
						" . $this->username_field . "
				FROM User
				WHERE " . $this->email_field . " ='" . $this->dbobj->real_escape_string($Email) . "'
				AND " . $this->login_attempts_field . " > '0'
				AND " . $this->status_field . " != '0'";
		$result = $this->dbobj->execute($query);
		if ($result->fetch_num_rows() == 1) {
			$valid_user = $result->fetch_assoc();
			return $valid_user;
		} else {
			//we have no valid user or there are several users with same username and password.
			return false;
		}
	}

	public function validate_memorable_phrase_email($MemorablePhrase, $Email) {
		if(!basic_email_validation($Email)){
			return false;
		}
		//if user is valid return their userID else false
		$query = "SELECT " . $this->user_ID_field . ",
						" . $this->email_field . ",
						" . $this->username_field . ",
						" . $this->memorable_phrase_field . "
				FROM User
				WHERE " . $this->memorable_phrase_field . " ='" . $this->dbobj->real_escape_string($MemorablePhrase) . "'
				AND " . $this->email_field . " ='" . $this->dbobj->real_escape_string($Email) . "'
				AND " . $this->login_attempts_field . " > '0'
				AND " . $this->status_field . " != '0'";
		$result = $this->dbobj->execute($query);
		if ($result->fetch_num_rows() == 1) {
			$valid_user = $result->fetch_assoc();
			return $valid_user;
		} else {
			//we have no valid user or there are several users with same username and password.
			return false;
		}
	}

	public function get_username($userID) {
		$query = sprintf("SELECT " . $this->username_field . " FROM User
							WHERE " . $this->user_ID_field . " = '%d' ", $this->dbobj->real_escape_string($userID));
		$result = $this->dbobj->execute($query);
		if ($result->fetch_num_rows() == 1) {
			$username = $result->fetch_assoc();
			return $username;
		} else {
			//we have no valid user or there are several users with same username and password.
			return false;
		}
	}

	public function update_attempts_field($userID, $value) {
		$query = sprintf("UPDATE User SET
							" . $this->login_attempts_field . " ='%s'
							WHERE " . $this->user_ID_field . " = '%d' ", $this->dbobj->real_escape_string($value), $this->dbobj->real_escape_string($userID));
		$result = $this->dbobj->execute($query);
		return (bool)$result;
	}

	public function update_password_field($userID, $value) {
		$query = sprintf("UPDATE User SET
							" . $this->password_field . " ='%s'
							WHERE " . $this->user_ID_field . " = '%d' ", $this->dbobj->real_escape_string($value), $this->dbobj->real_escape_string($userID));
		$result = $this->dbobj->execute($query);
		return (bool)$result;
	}

	public function decrement_attempts_field($username) {
		$query = sprintf("UPDATE User SET
							" . $this->login_attempts_field . " = " . $this->login_attempts_field . " -1
							WHERE " . $this->username_field . " = '%s' ", $this->dbobj->real_escape_string($username));

		$result = $this->dbobj->execute($query);
		return (bool)$result;
	}

	public function changeStatus($userID, $status) {
		$query = sprintf("UPDATE User SET
							" . $this->status_field . " = '%s'
							WHERE " . $this->user_ID_field . " = '%s' ", $this->dbobj->real_escape_string($status), $this->dbobj->real_escape_string($userID));
		$result = $this->dbobj->execute($query);
		return (bool)$result;
	}

	public function update_memorable_phrase_fields($userID, $memorable_phrase, $memorable_phrase_hint_ID) {
		$query = sprintf("UPDATE User SET
							" . $this->memorable_phrase_hint_ID_field . " ='%d',
							" . $this->memorable_phrase_field . " ='%s'
							WHERE " . $this->user_ID_field . " = '%d' ", $this->dbobj->real_escape_string($memorable_phrase_hint_ID), $this->dbobj->real_escape_string($memorable_phrase), $this->dbobj->real_escape_string($userID));
		$result = $this->dbobj->execute($query);
		return (bool)$result;
	}

	public function replace_username($userID, $username, $hashed_password) {
		$query = sprintf("UPDATE User SET
							" . $this->username_field . " ='%s' ,
							" . $this->password_field . " ='%s'
							WHERE " . $this->user_ID_field . " = '%d' ", $this->dbobj->real_escape_string($username), $this->dbobj->real_escape_string($hashed_password), $this->dbobj->real_escape_string($userID));
		$result = $this->dbobj->execute($query);
		return (bool)$result;
	}

	public function log_entry($vortex_userID, $log, $ipAddress, $user_agent) {
		$query = sprintf("INSERT INTO Logs (vortex_userID, log, ipAddress, user_agent, time_of_log)
							VALUES ('%d', '%s', '%s', '%s', NOW() )", $this->dbobj->real_escape_string($vortex_userID), $this->dbobj->real_escape_string($log), $this->dbobj->real_escape_string($ipAddress), $this->dbobj->real_escape_string($user_agent));
		$result = $this->dbobj->execute($query);
		return (bool)$result;
	}
}