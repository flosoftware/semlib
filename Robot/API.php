<?php

class Robot_API
{

    const CREATE_SUPPLIER_SCHEDULED_CREATION_TASK = 'create.supplier.scheduled.creation.task';

    const GET_SUPPLIER = 'get.robot.supplier.id';
    const UPDATE_SUPPLIER_DETAILS = 'update.supplier.details';
    const GET_SUPPLIER_INVOICING_UNIT_ID = 'get.supplier.invoicingunit.id';
    const GET_CANDIDATE_INVOICING_UNIT_ID = 'get.candidate.invoicingunit.id';
    const GET_CUSTOMER_INVOICING_UNIT_ID = 'get.customer.invoicingunit.id';
    const UPDATE_SUPPLIER_INVOICING_UNIT = 'update.supplier.invoicingunit';
    const INSERT_SUPPLIER_INVOICE = 'insert.supplier.invoice';
    const RAISE_SUPPLIER_INVOICE = 'raise.supplier.invoice';
    const CHECK_SUPPLIER_INVOICE_EXISTS = 'check.supplier.invoice.exists';
    const RAISE_SUPPLIER_CREDIT_NOTE = 'raise.supplier.credit.note';
    const RAISE_CREDIT_OFF_PRODUCT_INVOICE = 'raise.credit.off.invoice';

    const INSERT_CUSTOMER_INVOICE = 'insert.customer.invoice';
    const INSERT_REQUEST_FOR_PAYMENT = 'insert.request.for.payment';
    const INSERT_MARGIN_INVOICE = 'insert.margin.invoice';
    const INSERT_PAYROLL_INVOICE = 'insert.payroll.invoice';
    const INSERT_PAYROLL_ADVANCE = 'insert.payroll.advance';

    const INSERT_PAYSLIP = 'insert.payslip';
    const INSERT_PAYSLIP_ADVANCE = 'insert.payslip.advance';
    const INSERT_PAYSLIP_BREAKDOWN = 'insert.payslip.breakdown';

    const GET_CUSTOMER = 'get.robot.customer.id';
    const UPDATE_CUSTOMER_DETAILS = 'update.customer.details';
    const RAISE_CUSTOMER_CREDIT_NOTE = 'raise.customer.credit.note';

    const GET_CANDIDATE = 'get.robot.candidate.id';
    const UPDATE_CANDIDATE = 'update.candidate.details';
    const UPDATE_CANDIDATE_ADDRESS = 'update.candidate.address';
    const UPDATE_CANDIDATE_BANK_DETAILS = 'update.candidate.bank.details';
    const UPDATE_CANDIDATE_CONTACT_DETAILS = 'update.candidate.contact.details';
    const UPDATE_CANDIDATE_DEFAULT_PAYMENT_METHOD = 'update.candidate.default.payment.method';

    const CREDIT_OFF_INVOICES = 'credit.off.invoices';
    const LINK_FILE_TO_INVOICE = 'link.file.to.customer.invoice';
    const LINK_CONSOLIDATE_FILE_TO_CLIENT = 'link.consolidated.file.to.customer';

    const INSERT_COMPANY_GROUP_TYPE = 'insert.group';
    const GET_COMPANY_GROUP = 'get.company.group.id';

    const CREATE_SCHEDULED_INVOICE_ENTITY = 'create.supplier.scheduled.creation.task';

    const APPLICATION_ID_ETIPS = 1;

    const GET_PAYROLL_INVOICE = 'get.payroll.invoice';
    const CANCEL_PAYROLL_INVOICE = 'cancel';
    const INSERT_TEMPO_INVOICE = 'insert.tempo.invoice';

    const GET_OPEN_AMOUNT_FOR_CANDIDATE = 'get.open.amount.for.candidate';
    const GET_OPEN_AMOUNT_FOR_COMPANY = 'get.open.amount.for.company';
    const GET_OPEN_AMOUNT_FOR_BILLING_UNIT = 'get.open.amount.for.billing.unit';

    const INSERT_PRODUCT_INVOICE = 'insert.product.invoice';

    const GET_SUPPLIER_CREDIT_STATUS = 'get.supplier.credit.status';

    const RUN_SUPPLIER_EARLY_SETTLEMENT_REPORT = 'run.supplier.early.settlement.report';

    const TEST_API = 'test';

    const RUN_PAYROLL_REPORTS = 'run.payroll.reports';

    protected $_APIUrl;
    protected $_APIKey;
    protected $_APISecret;
    protected $_useDigitalSignature;
    protected $_applicationId;
    protected $_postData;

    protected $_isSetup = false;

    protected $_supplierIDCache = array();
    protected $_supplierInvoicingUnitIDCache = array();
    protected $_customerIDCache = array();

    protected $_log;

    protected function _log($msg, $level = 'err')
    {
        if (is_null($this->_log)) {
            echo $msg;
        } else {
            /* @var $log Zend_Log */
            $log = $this->_log;
            $log->$level('Robot_API: ' . $msg);
        }
    }

    protected function _setup(
        $APIUrl,
        $applicationId,
        $postData = true,
        Zend_Log $log = null,
        $APIKey = null,
        $useDigitalSignature = false,
        $APISecret = null
    ) {
        if ($APIKey == null)
            throw new Exception('APIKey has not been set');

        if ($useDigitalSignature && $APISecret == null)
            throw new Exception('APISecret has not been set');

        $this->_APIUrl = $APIUrl;
        $this->_APIKey = $APIKey;
        if ($useDigitalSignature) {
            $this->_useDigitalSignature = true;
            $this->_APISecret = $APISecret;
        }
        $this->_applicationId = $applicationId;
        $this->_postData = $postData;
        $this->_isSetup = true;
        $this->_log = $log;
    }

    protected function _checkSetup()
    {
        if (!$this->_isSetup) {
            throw new Exception('$this->_setup() must be called before calling API methods');
        }
    }

    /**
     * Gets the signature based string used to create the digital signature.
     * We percent encode using RFC 3986
     * @param string $httpMethod
     * @param string $baseUrl
     * @param array $arguments
     * @return string
     */
    protected function _getSignatureBasedString($httpMethod, $baseUrl, $arguments)
    {
        $percentEncodedParametersString = '';

        $percentEncodedArguments = array();
        foreach ($arguments as $key => $value) {
            if (!is_array($value)) {
                $percentEncodedArguments[rawurlencode($key)] = rawurlencode($value);
            }
        }

        ksort($percentEncodedArguments);

        foreach ($percentEncodedArguments as $key => $value) {
            $percentEncodedParametersString .= $key . '=' . $value . '&';
        }

        $percentEncodedParametersString = substr(
            $percentEncodedParametersString,
            0,
            strlen($percentEncodedParametersString) - 1
        );

        $this->_log('Percent encoded parameters String: ' . $percentEncodedParametersString, 'debug');

        $signatureBaseString = '';
        $signatureBaseString .= rawurlencode($percentEncodedParametersString);

        return $signatureBaseString;
    }

    /**
     * Calculates the message authentication code (digital signature) using HMAC-SHA1 and the shared secret and then base64 encoded
     * @param string $httpMethod
     * @param string $baseUrl
     * @param array $arguments
     * @param string $apiSecret
     * @return string
     */
    protected function _getMessageAuthenticationCode($httpMethod, $baseUrl, $arguments, $apiSecret)
    {
        $signatureBasedString = $this->_getSignatureBasedString($httpMethod, $baseUrl, $arguments);
        return base64_encode(hash_hmac("sha1", $signatureBasedString, $apiSecret, TRUE));
    }

    protected function _callRobotAPIMethod($method, Array $arguments)
    {
        $this->_checkSetup();
        $url = $this->_APIUrl . $method;

        $apiSecret = $this->_APISecret;
        $arguments['api_key'] = $this->_APIKey;
        if ($this->_postData) {
            $httpMethod = 'POST';
        } else {
            $httpMethod = 'GET';
        }

        foreach ($arguments as $key => $value) {
            if (gettype($value) == 'boolean') {
                $arguments[$key] = (int)$value;
            }
        }

        if ($this->_useDigitalSignature) {
            //Need to do this since curl doesn't send empty parameters
            $this->_unsetEmptyParameters($arguments);

            $messageAuthenticationCode = $this->_getMessageAuthenticationCode($httpMethod, $url, $arguments, $apiSecret);
            $arguments['mac'] = $messageAuthenticationCode;
        }

        //open connection
        $ch = curl_init();
        //set the url, number of POST vars, POST data
        $arguments = http_build_query($arguments);
        $this->_log($url);
        if ($this->_postData) {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);
            curl_setopt($ch, CURLOPT_POST, count($arguments));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $arguments);
        } else {
            $url = $url . '?' . $arguments;
            curl_setopt($ch, CURLOPT_URL, $url);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //execute post
        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            $this->_log('Curl error ' . curl_errno($ch));
            throw new Exception('Failed to connect to Robot API');
        }

        //close connection
        curl_close($ch);

        $curlResult = json_decode($result);
        if ($curlResult instanceof stdClass) {
            if (property_exists($curlResult, 'status')) {
                if ($curlResult->status == 'Error') {
                    $errorCode = "";
                    if (property_exists($curlResult, 'code')) {
                        $errorCode .= " : Error Code - {$curlResult->code}";
                    }
                    throw new Semlib_Exception("Robot API {$errorCode} | {$curlResult->result}");
                }
            }
        }

        return json_decode($result);
    }

    /**
     * @param $parameters
     */
    protected function _unsetEmptyParameters(&$parameters)
    {
        foreach ($parameters as $key => &$value) {
            if (is_array($value)) {
                $this->_unsetEmptyParameters($value);
            } elseif (trim($value) == '') {
                unset($parameters[$key]);
            }
        }
    }

    protected function _checkCompanyCache($cache, $companyId, $entityNumber, $invoiceEntity)
    {
        if (array_key_exists($this->_applicationId, $cache)
            && array_key_exists($companyId, $cache[$this->_applicationId])
            && array_key_exists($entityNumber, $cache[$this->_applicationId][$companyId])
            && array_key_exists($invoiceEntity, $cache[$this->_applicationId][$companyId][$entityNumber])
        ) {
            return $cache[$this->_applicationId][$companyId][$entityNumber][$invoiceEntity];
        }
        return false;
    }

    protected function _addToCompanyCache(&$cache, $companyId, $id, $entityNumber = 1, $invoiceEntity = 0)
    {
        if (!array_key_exists($this->_applicationId, $cache)) {
            $cache[$this->_applicationId] = array();
        }
        if (!array_key_exists($companyId, $cache[$this->_applicationId])) {
            $cache[$this->_applicationId][$companyId] = array();
        }
        if (!array_key_exists($entityNumber, $cache[$this->_applicationId][$companyId])) {
            $cache[$this->_applicationId][$companyId][$entityNumber] = array();
        }
        $cache[$this->_applicationId][$companyId][$entityNumber][$invoiceEntity] = $id;
    }

    protected function _getRobotSupplierId($companyId, $entityNumber = 1, $invoiceEntity = 0)
    {
        $this->_checkSetup();
        $id = $this->_checkCompanyCache($this->_supplierIDCache, $companyId, $entityNumber, $invoiceEntity);
        if ($id) {
            return $id;
        }
        $arguments = array(
            'application_id' => $this->_applicationId,
            'company_id' => $companyId,
            'entity' => $entityNumber,
            'invoice_entity' => $invoiceEntity
        );
        $id = $this->_callRobotAPIMethod(self::GET_SUPPLIER, $arguments);
        $this->_addToCompanyCache($this->_supplierIDCache, $companyId, $id, $entityNumber, $invoiceEntity);
        return $id;
    }

    protected function _getRobotCustomerId(
        $companyId,
        $entityNumber = 1
    ) {
        $this->_checkSetup();
        $id = $this->_checkCompanyCache($this->_supplierIDCache, $companyId, $entityNumber, 0);
        if ($id) {
            return $id;
        }
        $arguments = array(
            'application_id' => $this->_applicationId,
            'company_id' => $companyId,
            'entity' => $entityNumber
        );
        $id = $this->_callRobotAPIMethod(self::GET_CUSTOMER, $arguments);
        $this->_addToCompanyCache($this->_supplierIDCache, $companyId, $id);
        return $id;
    }

    protected function _getRobotCandidateId($candidateId, $entityNumber = 1)
    {
        $this->_checkSetup();
        $arguments = array(
            'application_id'	=> $this->_applicationId,
            'company_id'		=> $candidateId,
            'entity'			=> $entityNumber
        );
        $id = $this->_callRobotAPIMethod(self::GET_CANDIDATE, $arguments);
        return $id;
    }

    protected function _createScheduledInvoiceEntity(
        $companyId,
        $name,
        $company_reg,
        $utr,
        $vat_number,
        $reason,
        $entityNumber = 1,
        $invoiceEntity = 0,
        $effectiveDate,
        $typeRef,
        $creationTypeRef,
        $branchId = null,
        $address = null,
        $city = null,
        $county = null,
        $post_code = null,
        $country = null,
        $existingRobotSupplierId
    ) {

        $this->_checkSetup();
        $arguments = array(
            'applicationId' => $this->_applicationId,
            'applicationCompanyId' => $companyId,
            'entityNumber' => $entityNumber,
            'invoiceEntity' => $invoiceEntity,
            'name' => $name,
            'registrationNumber' => $company_reg,
            'uniqueTaxRecord' => $utr,
            'vatNumber' => $vat_number,
            'reasonChanged' => $reason,
            'address' => $address,
            'city' => $city,
            'county' => $county,
            'country' => $country,
            'postcode' => $post_code,
            'creationTypeRef' => $creationTypeRef,
            'effectiveDate' => $effectiveDate,
            'typeRef' => $typeRef,
            'branchId' => $branchId,
            'existingSupplierRobotId' => $existingRobotSupplierId
        );
        return $this->_callRobotAPIMethod(self::CREATE_SCHEDULED_INVOICE_ENTITY, $arguments);
    }

    protected function _updateSupplierDetails(
        $companyId,
        $name,
        $company_reg,
        $utr,
        $vat_number,
        $reason,
        $address = null,
        $city = null,
        $county = null,
        $post_code = null,
        $bank_name = null,
        $account_name = null,
        $account_number = null,
        $sort_code = null,
        $firstName = null,
        $surname = null,
        $email = null,
        $phoneNumber = null,
        $entityNumber = 1,
        $invoiceEntity = 0,
        $rollNumber = null,
        $country = null
    ) {

        return $this->_updateSupplierOrCandidateDetails(
            self::UPDATE_SUPPLIER_DETAILS,
            $companyId,
            $name,
            $company_reg,
            $utr,
            $vat_number,
            $reason,
            $address,
            $city,
            $county,
            $post_code,
            $bank_name,
            $account_name,
            $account_number,
            $sort_code,
            $firstName,
            $surname,
            $email,
            $phoneNumber,
            false,
            $entityNumber,
            $invoiceEntity,
            $rollNumber,
            $country
        );
    }

    protected function _updateCandidateDetails(
        $candidateId,
        $name,
        $company_reg,
        $utr,
        $vat_number,
        $reason,
        $address = null,
        $city = null,
        $county = null,
        $post_code = null,
        $bank_name = null,
        $account_name = null,
        $account_number = null,
        $sort_code = null,
        $firstName = null,
        $surname = null,
        $email = null,
        $phoneNumber = null,
        $payByChaps = null,
        $rollNumber = null,
        $country = null,
        $serviceGroupId = 1
    ) {
        return $this->_updateSupplierOrCandidateDetails(
            self::UPDATE_CANDIDATE,
            $candidateId,
            $name,
            $company_reg,
            $utr,
            $vat_number,
            $reason,
            $address,
            $city,
            $county,
            $post_code,
            $bank_name,
            $account_name,
            $account_number,
            $sort_code,
            $firstName,
            $surname,
            $email,
            $phoneNumber,
            $payByChaps,
            $serviceGroupId,
            0,
            $rollNumber,
            $country
        );
    }

    protected function _updateSupplierOrCandidateDetails(
        $function,
        $companyId,
        $name,
        $company_reg,
        $utr,
        $vat_number,
        $reason,
        $address = null,
        $city = null,
        $county = null,
        $post_code = null,
        $bank_name = null,
        $account_name = null,
        $account_number = null,
        $sort_code = null,
        $firstName = null,
        $surname = null,
        $email = null,
        $phoneNumber = null,
        $payByChaps = null,
        $entityNumber = 1,
        $invoiceEntity = 0,
        $rollNumber = null,
        $country = null
    ) {
        $this->_checkSetup();
        $arguments = array(
            'application_id' => $this->_applicationId,
            'company_id' => $companyId,
            'entity' => $entityNumber,
            'invoice_entity' => $invoiceEntity,
            'name' => $name,
            'company_reg' => $company_reg,
            'utr' => $utr,
            'vat_number' => $vat_number,
            'reason' => $reason,
            'bank_name' => $bank_name,
            'account_name' => $account_name,
            'account_number' => $account_number,
            'sort_code' => $sort_code,
            'address' => $address,
            'city' => $city,
            'county' => $county,
            'post_code' => $post_code,
            'country' => $country,
            'first_name' => $firstName,
            'surname' => $surname,
            'email' => $email,
            'phone' => $phoneNumber,
            'payByChaps' => $payByChaps,
            'roll_number' => $rollNumber
        );
        return $this->_callRobotAPIMethod($function, $arguments);
    }

    protected function _updateCustomerDetails(
        $companyId,
        $name,
        $company_reg,
        $utr,
        $vat_number,
        $reason,
        $address = null,
        $city = null,
        $county = null,
        $post_code = null,
        $firstName = null,
        $surname = null,
        $email = null,
        $phoneNumber = null,
        $pdfDescription = null,
        $invoiceDay = null,
        $paymentTermsDays = null,
        $entityNumber = 1,
        $country = null
    ) {
        $this->_checkSetup();
        $arguments = array(
            'application_id' => $this->_applicationId,
            'company_id' => $companyId,
            'entity' => $entityNumber,
            'name' => $name,
            'company_reg' => $company_reg,
            'utr' => $utr,
            'vat_number' => $vat_number,
            'reason' => $reason,
            'address' => $address,
            'city' => $city,
            'county' => $county,
            'post_code' => $post_code,
            'country' => $country,
            'first_name' => $firstName,
            'surname' => $surname,
            'email' => $email,
            'phone' => $phoneNumber,
            'pdf_description' => str_replace('.', '', $pdfDescription),
            'invoice_day' => $invoiceDay,
            'payment_terms_days' => $paymentTermsDays
        );
        return $this->_callRobotAPIMethod(self::UPDATE_CUSTOMER_DETAILS, $arguments);
    }

    protected function _getRobotSupplierInvoicingUnitId($robotCompanyId, $branchId)
    {
        return $this->_getRobotInvoicingUnitId($robotCompanyId, $branchId, self::GET_SUPPLIER_INVOICING_UNIT_ID);
    }

    protected function _getRobotCustomerInvoicingUnitId($robotCompanyId, $branchId)
    {
        return $this->_getRobotInvoicingUnitId($robotCompanyId, $branchId, self::GET_CUSTOMER_INVOICING_UNIT_ID);
    }

    protected function _getRobotCandidateInvoicingUnitId($robotCompanyId, $serviceGroupId = 3)
    {
        return $this->_getRobotInvoicingUnitId(
            $robotCompanyId,
            1,
            self::GET_CANDIDATE_INVOICING_UNIT_ID,
            $serviceGroupId
        );
    }

    protected function _getRobotInvoicingUnitId($robotCompanyId, $branchId, $method, $serviceGroupId = 1)
    {
        $this->_checkSetup();
        if (array_key_exists($this->_applicationId, $this->_supplierInvoicingUnitIDCache)
            && is_array($this->_supplierInvoicingUnitIDCache[$this->_applicationId])
            && array_key_exists($robotCompanyId, $this->_supplierInvoicingUnitIDCache[$this->_applicationId])
            && is_array($this->_supplierInvoicingUnitIDCache[$this->_applicationId][$robotCompanyId])
            && array_key_exists($branchId, $this->_supplierInvoicingUnitIDCache[$this->_applicationId][$robotCompanyId])
        ) {
            return $this->_supplierInvoicingUnitIDCache[$this->_applicationId][$robotCompanyId][$branchId];
        }
        $arguments = array(
            'application_id' => $this->_applicationId,
            'company_id' => $robotCompanyId,
            'service_group_id' => $serviceGroupId
        );
        if ($method == self::GET_CUSTOMER_INVOICING_UNIT_ID) {
            $arguments['billing_group'] = $branchId;
        } else {
            $arguments['branch_id'] = $branchId;
        }

        $id = $this->_callRobotAPIMethod($method, $arguments);
        if (!array_key_exists($this->_applicationId, $this->_supplierInvoicingUnitIDCache)) {
            $this->_supplierInvoicingUnitIDCache[$this->_applicationId] = array($robotCompanyId => array());
        }
        if (!array_key_exists($robotCompanyId, $this->_supplierInvoicingUnitIDCache[$this->_applicationId])) {
            $this->_supplierInvoicingUnitIDCache[$this->_applicationId][$robotCompanyId] = array();
        }
        $this->_supplierInvoicingUnitIDCache[$this->_applicationId][$robotCompanyId] = $id;
        return $id;
    }

    protected function _updateRobotSupplierInvoicingUnitId(
        $companyId,
        $name,
        $branchId,
        $reason,
        $address = null,
        $city = null,
        $county = null,
        $post_code = null,
        $bank_name = null,
        $account_name = null,
        $account_number = null,
        $sort_code = null,
        $rollNumber = null,
        $country = null
    ) {
        $this->_checkSetup();
        $arguments = array(
            'application_id' => $this->_applicationId,
            'company_id' => $companyId,
            'name' => $name,
            'branch_id' => $branchId,
            'reason' => $reason,
            'bank_name' => $bank_name,
            'account_name' => $account_name,
            'account_number' => $account_number,
            'sort_code' => $sort_code,
            'address' => $address,
            'city' => $city,
            'county' => $county,
            'post_code' => $post_code,
            'country' => $country,
            'roll_number' => $rollNumber
        );
        return $this->_callRobotAPIMethod(self::UPDATE_SUPPLIER_INVOICING_UNIT, $arguments);
    }

    protected function _insertCustomerInvoice(
        $companyId,
        $billingGroup,
        $invoiceNumber,
        $gross,
        $net,
        $vat,
        $transactionDate,
        $clientWeekEndDate,
        $systemWeekEndDate,
        $po,
        $filename
    ) {
        $this->_checkSetup();
        return $this->_insertCustomerDocument(
            $companyId,
            $billingGroup,
            '',
            $invoiceNumber,
            $gross,
            $net,
            $vat,
            null,
            null,
            null,
            $transactionDate,
            $clientWeekEndDate,
            $systemWeekEndDate,
            $po,
            $filename,
            self::INSERT_CUSTOMER_INVOICE
        );
    }

    protected function _insertRequestForPayment(
        $companyId,
        $billingGroup,
        $invoiceNumber,
        $gross,
        $net,
        $vat,
        $transactionDate,
        $clientWeekEndDate,
        $systemWeekEndDate,
        $po,
        $filename
    ) {
        $this->_checkSetup();
        return $this->_insertCustomerDocument(
            $companyId,
            $billingGroup,
            '',
            $invoiceNumber,
            $gross,
            $net,
            $vat,
            null,
            null,
            null,
            $transactionDate,
            $clientWeekEndDate,
            $systemWeekEndDate,
            $po,
            $filename,
            self::INSERT_REQUEST_FOR_PAYMENT
        );
    }

    protected function _insertMarginInvoice(
        $companyId,
        $billingGroup,
        $invoiceNumber,
        $gross,
        $net,
        $vat,
        $transactionDate,
        $clientWeekEndDate,
        $systemWeekEndDate,
        $po,
        $filename
    ) {
        $this->_checkSetup();
        return $this->_insertCustomerDocument(
            $companyId,
            $billingGroup,
            '',
            $invoiceNumber,
            $gross,
            $net,
            $vat,
            null,
            null,
            null,
            $transactionDate,
            $clientWeekEndDate,
            $systemWeekEndDate,
            $po,
            $filename,
            self::INSERT_MARGIN_INVOICE
        );
    }

    /**
     * Builds array of key/values to send over cURL
     * with a instruction to create a single customer invoice
     *
     * Note - This is used by multiple invoice types
     *
     * @param int $companyId
     * @param string $billingGroup
     * @param string $billingGroupName
     * @param string $invoiceNumber
     * @param double $gross
     * @param double $net
     * @param double $vat
     * @param mixed|double|null $candidateCost
     * @param mixed|double|null $employerNI
     * @param mixed|double|null $wtr
     * @param int $transactionDate
     * @param string $clientWeekEndDate
     * @param string $systemWeekEndDate
     * @param string $po
     * @param string $filename
     * @param string $method
     * @param bool $nonInvoiceable
     * @param int $serviceGroupId
     * @param int $payslipAmount
     * @param string $note
     * @param mixed|double|null $pae
     * @return mixed
     */
    protected function _insertCustomerDocument(
        $companyId,
        $billingGroup,
        $billingGroupName,
        $invoiceNumber,
        $gross,
        $net,
        $vat,
        $candidateCost = null,
        $employerNI = null,
        $wtr = null,
        $transactionDate,
        $clientWeekEndDate,
        $systemWeekEndDate,
        $po,
        $filename,
        $method,
        $nonInvoiceable = false,
        $serviceGroupId = 1,
        $payslipAmount = 0,
        $note = "",
        $pae = null
    ) {
        $arguments = array(
            'application_id' => $this->_applicationId,
            'company_id' => $companyId,
            'billing_group' => $billingGroup,
            'billing_group_name' => $billingGroupName,
            'invoice_number' => $invoiceNumber,
            'gross' => $gross,
            'net' => $net,
            'vat_amount' => $vat,
            'candidate_cost' => $candidateCost,
            'employer_ni' => $employerNI,
            'wtr' => $wtr,
            'transaction_date' => $transactionDate,
            'client_week_end_date' => $clientWeekEndDate,
            'system_week_end_date' => $systemWeekEndDate,
            'po' => $po,
            'filename' => $filename,
            'non_invoiceable' => $nonInvoiceable,
            'service_group_id' => $serviceGroupId,
            'payslips_count' => $payslipAmount,
            'note' => $note,
            'pae' => $pae
        );
        return $this->_callRobotAPIMethod($method, $arguments);
    }

    protected function _insertSupplierInvoice(
        $companyId,
        $invoicingUnitId,
        $invoiceNumber,
        $gross,
        $net,
        $vat,
        $transactionDate,
        $clientWeekEndDate,
        $systemWeekEndDate,
        $agencyRef,
        $relatedInvoice,
        $billingGroup = 1,
        $grossNonPae = 0,
        $netNonPae = 0,
        $vatNonPae = 0
    ) {
        $this->_checkSetup();
        $arguments = array(
            'application_id' => $this->_applicationId,
            'company_id' => $companyId,
            'invoicing_unit_id' => $invoicingUnitId,
            'invoice_number' => $invoiceNumber,
            'gross' => $gross,
            'net' => $net,
            'vat_amount' => $vat,
            'transaction_date' => $transactionDate,
            'client_week_end_date' => $clientWeekEndDate,
            'system_week_end_date' => $systemWeekEndDate,
            'agency_ref' => $agencyRef,
            'related_invoice' => $relatedInvoice,
            'billing_group' => $billingGroup,
            'gross_non_pae' => $grossNonPae,
            'net_non_pae' => $netNonPae,
            'vat_non_pae' => $vatNonPae
        );
        return $this->_callRobotAPIMethod(self::INSERT_SUPPLIER_INVOICE, $arguments);
    }

    /**
     *
     * @param int $companyId
     * @param int $invoicingUnitId
     * @param int $billingUnitId
     * @param string $invoiceNumber
     * @param float $net
     * @param float $vat
     * @param string $transactionDate
     * @param string $systemWeekEndDate
     * @param string $clientWeekEndDate
     * @param int $settlementMethod
     * @param Array $offsetPreference
     * @param string $note
     * @return mixed
     */
    protected function _insertTempoInvoice(
        $companyId,
        $invoicingUnitId,
        $billingUnitId = 1,
        $invoiceNumber,
        $net,
        $vat,
        $transactionDate,
        $systemWeekEndDate,
        $clientWeekEndDate,
        $settlementMethod,
        $offsetPreference = array(),
        $note = ''
    ) {
        $this->_checkSetup();
        $arguments = array(
            'application_id' => $this->_applicationId,
            'company_id' => $companyId,
            'invoicing_unit_id' => $invoicingUnitId,
            'billing_group' => $billingUnitId,
            'invoice_number' => $invoiceNumber,
            'net' => $net,
            'vat_amount' => $vat,
            'gross' => ($net + $vat),
            'transaction_date' => $transactionDate,
            'system_week_end_date' => $systemWeekEndDate,
            'client_week_end_date' => $clientWeekEndDate,
            'reference' => '',
            'note' => $note,
            'settlement_method' => $settlementMethod,
            'offset_preference' => json_encode($offsetPreference)
        );
        return $this->_callRobotAPIMethod(self::INSERT_TEMPO_INVOICE, $arguments);
    }

    public function checkSupplierInvoiceExists($invoicingUnitId, $invoiceNumber)
    {
        $this->_checkSetup();
        $arguments = array(
            'application_id' => $this->_applicationId,
            'invoicing_unit_id' => $invoicingUnitId,
            'invoice_number' => $invoiceNumber
        );
        return $this->_callRobotAPIMethod(self::CHECK_SUPPLIER_INVOICE_EXISTS, $arguments);
    }

    public function raiseSupplierInvoice($invoicingUnitId, $invoiceNumber, $agencyRef)
    {
        $this->_checkSetup();
        $arguments = array(
            'application_id' => $this->_applicationId,
            'invoicing_unit_id' => $invoicingUnitId,
            'invoice_number' => $invoiceNumber,
            'agency_ref' => $agencyRef
        );
        return $this->_callRobotAPIMethod(self::RAISE_SUPPLIER_INVOICE, $arguments);
    }

    protected function _raiseCustomerCreditNote(
        $companyId,
        $invoicingUnitId,
        Robot_CreditNote $creditNote,
        $invoiceNum,
        $message
    ) {
        return $this->_raiseCreditNote(
            self::RAISE_CUSTOMER_CREDIT_NOTE,
            $companyId,
            $invoicingUnitId,
            $creditNote,
            $invoiceNum,
            $message
        );
    }

    protected function _raiseSupplierCreditNote(
        $companyId,
        $invoicingUnitId,
        Robot_CreditNote $creditNote,
        $invoiceNum,
        $message
    ) {
        return $this->_raiseCreditNote(
            self::RAISE_SUPPLIER_CREDIT_NOTE,
            $companyId,
            $invoicingUnitId,
            $creditNote,
            $invoiceNum,
            $message
        );
    }

    protected function _raiseProductCreditNote(
        $companyId,
        $invoicingUnitId,
        Robot_CreditNote $creditNote,
        $invoiceNum,
        $message
    ) {
        return $this->_raiseCreditNote(
            self::RAISE_CREDIT_OFF_PRODUCT_INVOICE,
            $companyId,
            $invoicingUnitId,
            $creditNote,
            $invoiceNum,
            $message
        );
    }

    protected function _raiseCreditNote(
        $method,
        $companyId,
        $invoicingUnitId,
        Robot_CreditNote $creditNote,
        $invoiceNum,
        $message
    ) {
        $this->_checkSetup();
        $arguments = array(
            'application_id' => $this->_applicationId,
            'company_id' => $companyId,
            'invoicing_unit_id' => $invoicingUnitId,
            'invoice_number' => $invoiceNum,
            'gross' => $creditNote->getGross(),
            'net' => $creditNote->getNet(),
            'vat' => $creditNote->getVat(),
            'transaction_date' => (method_exists($creditNote, 'getEffectiveDate')) ? strtotime($creditNote->getEffectiveDate()) : time(),
            'description' => $message
        );
        return $this->_callRobotAPIMethod($method, $arguments);
    }

    /**
     *
     * @param int $companyId
     * @param int $invoiceId
     * @return mixed
     */
    public function _getRobotPayrollInvoice($companyId, $invoiceId)
    {
        $this->_checkSetup();
        $arguments = array(
            'invoiceId' => (int)$invoiceId,
            'companyId' => (int)$companyId
        );
        return $this->_callRobotAPIMethod(self::GET_PAYROLL_INVOICE, $arguments);
    }

    protected function _cancelRobotPayrollInvoice($companyId, $invoiceId, $note = "")
    {
        $this->_checkSetup();
        $arguments = array(
            'invoiceId' => (int)$invoiceId,
            'companyId' => (int)$companyId,
            'note' => $note
        );
        return $this->_callRobotAPIMethod(self::CANCEL_PAYROLL_INVOICE, $arguments);
    }

    /**
     * Perform a test to the Robot Api
     *
     * @return string
     */
    public function test()
    {
        $this->_checkSetup();

        $testArray = array(
            'test1' => 'aaaaaa',
            'test2' => array('test2' => 'bbbbb'),
            'test3' => array('test5' => 'jdfjfjkdl', 'test6' => array('test7' => 'jhdfjhgf', 'test8' => ''))
        );

        return $this->_callRobotAPIMethod(self::TEST_API, $testArray);
    }

    /**
     * Trigger method for running payroll reports on Robot
     *
     * @param string $systemWeekEndDate
     * @return mixed
     */
    public function runPayrollReports($systemWeekEndDate)
    {
        $this->_checkSetup();
        $arguments = array(
            'system_week_end_date' => $systemWeekEndDate
        );
        return $this->_callRobotAPIMethod(self::RUN_PAYROLL_REPORTS, $arguments);
    }
}
