<?php

function smarty_modifier_neg_num_to_letter($num) {

        if($num==-1) {
        
                return "N";
        }
        elseif($num==-2) {

                return "A";
        }
        elseif($num==-3) {

                return "S";
        }
        elseif($num==-4) {

                return "H";
        }
        else {

                return $num;
        }
}

?>
