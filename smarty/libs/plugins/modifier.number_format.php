<?php

function smarty_modifier_number_format($number, $dec_places=2, $dec_sep='.', $thou_sep=',') {

        return number_format($number, $dec_places, $dec_sep, $thou_sep);
}

?>
