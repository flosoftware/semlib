<?php

function smarty_function_hier_as_crumbs($params, &$smarty) {

        $crumbs = '';

        if(!empty($params['gp_label'])) {

                $crumbs .= $params['gp_label'];
        }

        if(!empty($params['gp_code'])) {

                $crumbs .= ' ('.$params['gp_code'].') ';
        }

        if(!empty($params['gp_label']) || !empty($params['gp_code'])) {

                $crumbs .= ' >> ';
        }

        if(!empty($params['p_label'])) {

                $crumbs .= $params['p_label'];
        }

        if(!empty($params['p_code'])) {

                $crumbs .= ' ('.$params['p_code'].') ';
        }

        if(!empty($params['p_label']) || !empty($params['p_code'])) {

                $crumbs .= ' >> ';
        }

        if(!empty($params['c_label'])) {

                $crumbs .= $params['c_label'];
        }

        if(!empty($params['c_code'])) {

                $crumbs .= ' ('.$params['c_code'].') ';
        }

        return $crumbs;
}

?>
