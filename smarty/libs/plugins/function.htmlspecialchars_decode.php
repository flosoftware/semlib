<?php
/**
 * Smarty {htmlspecialchars_decode} function plugin
 *
 * Type:     function<br>
 * Name:     htmlspecialchars_decode<br>
 * Purpose:  Convert special HTML entities back to characters<br>
 * @author   Gregory Chiche
 */

function smarty_function_htmlspecialchars_decode($params, &$smarty) {

	$final_string = '';

	if(!empty($params['source'])) {
		
		$final_string = htmlspecialchars_decode($params['source'], ENT_NOQUOTES);
	}

	return $final_string;
} 

?>
