<?php
require_once __DIR__ . '/mysql_exception.class.php';
/*
phpinfo();
die();
*/
/*!
 * \file database.class.php
 */
/// @cond TEST_CLASS_EXIST
if (!class_exists('DB_Mysql', false)) {
	/// @endcond
	/*!
 * \class DB_Mysql
 * \brief database abstraction class
 *
 * performs all the standard mysql
 * functions and tasks when called upon.
 */

/**
 * WARNING - DO NOT THROW EXCEPTIONS IN THIS CLASS
 *
 * As this class is used by the session handler, any exceptions thrown
 * here will cause an 'Exception thrown in Unknown on line 0' error.
 *
 * These are not fun.
 *
 * WARNING
 */
	class DB_Mysql {

		protected $_openTransactions;

		protected $user;
		protected $pass;
		protected $dbhost;
		protected $dbport;
		protected $dbname;
		protected $_connected = false;
		protected $_mysqli;
		protected $_log;

		/**
		 * constructor sets up username, password etc and then makes a connection
		 */
		public function __construct($user, $pass, $dbhost, $dbname, $dbport = null, Zend_Log $log = null) {
			$this->user = $user;
			$this->pass = $pass;
			$this->dbhost = $dbhost;
			if(!empty($dbport)){
				$this->dbport = $dbport;
			}
			$this->dbname = $dbname;
			if (!is_null($log)) {
				$this->_log = $log;
			} else {
				$this->_log = null;
			}
		}

		public function filterFieldName($name){
			return preg_replace('/[^a-zA-Z0-9_]/i','',$name);
		}

		/**
		 * returns the db resource for use against php funtcions such a mysql_real_escape_string() etc
		 * needed because $this->dbh is protected
		 */
		public function get_dbh() {
			$this->connect();
			return $this->_mysqli;
		}

		/**
		 * return last id of item inserted into db
		 */
		public function get_last_insert_id() {
			$this->connect();
			return $this->_mysqli->insert_id;
		}

		/**
		 * makes a connection and selects a database
		 */
		protected function connect($connect_again = false) {
			if(!$this->_connected
				|| !($this->_mysqli instanceof mysqli)
				|| $connect_again){
				$this->_mysqli = new mysqli($this->dbhost, $this->user, $this->pass, null, $this->dbport);
				if ($this->_mysqli->connect_error) {
					throw new MysqlException('Unable to connect to MySQL: '.$this->_mysqli->connect_error);
				}
				if (!$this->_mysqli->select_db($this->dbname)) {
					throw new MysqlException('Unable to select database: ' . $this->dbname);
				}
				$this->_connected = true;
			}
		}

		public function start_transaction() {
			$this->connect();
			if ($this->_openTransactions == 0) {
				$this->_mysqli->autocommit(false);
			} else {
				$this->_mysqli->query('SAVEPOINT ETIPS_' . $this->_openTransactions);
			}
            $this->_openTransactions++;
		}

		public function commit_transaction() {
			$this->connect();
			if ($this->_openTransactions == 1) {
				$this->_mysqli->commit();
				$this->_mysqli->autocommit(true);
			} elseif($this->_openTransactions > 1) {
                $this->_mysqli->query('RELEASE SAVEPOINT ETIPS_' . ($this->_openTransactions - 1));
			} else {
				return false;
			}
            $this->_openTransactions--;
		}

        public function rollback_all_transactions() {
            while(false !== $this->rollback_transaction()) {
                ;
            }
        }

		public function rollback_transaction() {
			$this->connect();
			if ($this->_openTransactions == 1) {
				$this->_mysqli->rollback();
				$this->_mysqli->autocommit(true);
			} elseif($this->_openTransactions > 1) {
                $this->_mysqli->query('ROLLBACK TO SAVEPOINT ETIPS_' . ($this->_openTransactions - 1));
            } else {
                return false;
            }
            $this->_openTransactions--;
		}

		public function set_charset($charset) {
			return $this->_mysqli->set_charset($charset);
		}

		/**
		 *
		 * @param $query
		 * @return DB_MysqlStatement
		 */
		public function execute($query) {
			$this->connect();
			$ret = $this->_mysqli->query($query);
			if (!$ret) {
				$backtrace = debug_backtrace();
				$exceptionMessage = 'Error in Database Query: ';
				if (isset($backtrace[1]['file'])) {
					$exceptionMessage .= "\n"
	                    .'From: '.$backtrace[1]['file'].':'.$backtrace[1]['line']."\n"
	                    .'Error: '.$this->_mysqli->error."\n".'Query: '.$query;
				} else if (isset($backtrace[0]['file'])) {
					$exceptionMessage .= "\n"
	                    .'From: '.$backtrace[0]['file'].':'.$backtrace[0]['line']."\n"
	                    .'Error: '.$this->_mysqli->error."\n".'Query: '.$query;
				} else {
					$exceptionMessage .= 'No debug backtrace information available';
				}
				if (!is_null($this->_log)){
					$this->_log->err($query);
				}
				throw new Exception($exceptionMessage);
			} elseif ($ret === true) {
				return true;
			} else {
				$stmt = new DB_MysqlStatement($this->_mysqli, $query);
				$stmt->set_result($ret);
				return $stmt;
			}
		}

		/**
		 * return the number of affected rows by the last mysql statement
		 */
		public function get_mysql_affected_rows() {
			$this->connect();
			return $this->_mysqli->affected_rows;
		}

		public function real_escape_string($string){
			$this->connect();
			return $this->_mysqli->real_escape_string($string);
		}

		public function getError(){
			return $this->_mysqli->error;
		}

		public function getMysqlTransactionFlag() {
			return $this->mysql_transaction_flag;
		}

		protected function _getCurrentSavepoint() {
			return 'ETIPS_' . $this->_openTransactions;
		}

		/**
		 * Creates a new XA transaction.
		 *
		 * @param string $xaId
		 * @return string transaction id
		 */
		public function initialiseDistributedTransaction($xaId = null) {
		    $xaResume = '';
		    if(!is_null($xaId)) {
		        $xaResume = $xaId . ' JOIN';
		    } else {
		        $xaId = uniqid('XA',true) . time();
		    }
		    $this->get_dbh()->query('XA BEGIN ' . $xaResume);
		    return $xaId;
		}

        /**
         * Commits an XA transaction.
         *
         * @param string $xaId
         * @return string transaction id
         */
        public function commitDistributedTransaction($xaId) {
            if(empty($xaId)) {
                throw new Exception('You cannot commit a master transaction without an id');
            }
            $this->get_dbh()->query('XA END ' . $xaId);
            $this->get_dbh()->query('XA COMMIT ' . $xaId . ' ONE PHASE');
            return $xaId;
        }

        /**
         * Rolls back an XA transaction.
         *
         * @param string $xaId
         * @return string transaction id
         */
        public function rollbackDistributedTransaction($xaId) {
            if(empty($xaId)) {
                throw new Exception('You cannot rollback a master transaction without an id');
            }
            $this->get_dbh()->query('XA ROLLBACK ' . $xaId);
            return $xaId;
        }

		/**
         * section below is legacy code to bridge old mysql specific code
         */
        public function start_mysql_transaction(){
            $this->start_transaction();
        }

        public function commit_mysql_transaction(){
            $this->commit_transaction();
        }

        public function rollback_mysql_transaction(){
            $this->rollback_transaction();
        }

	} // end of class
} // end of test if (!class_exists...)
?>