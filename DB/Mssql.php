<?php



class DB_Mssql{
	
	protected $dbUser;
	protected $dbPass;
	protected $dbHost;
	protected $dbPort;
	protected $dbName;
	protected $pdoType;
	protected $_isConnected = false;
	protected $_mssql;
	protected $_log =  NULL;
	
	private $_stmt = NULL;
	
	/**
	 * Contructor sets up username, password etc and then makes a connection
	 */
	
	public function __construct($user, $pass, $host, $dbname, $dbport = null, Zend_Log $log = null){
		$this->dbUser = $user;
		$this->dbPass = $pass;
		$this->dbHost = $host;
		$this->dbName = $dbname;
		$this->pdoType = 'dblib';
		if(!is_null($dbport)){
			$this->dbPort = $dbport;
		}else{
			$this->dbPort = 1433;
		}
		if (!is_null($log)){
			$this->_log = $log;
		}
		$this->connect();
	}
	
	/**
	 * makes a connection and selects a database
	 */
	public function connect($connect_again = false){
		if(!$this->_isConnected || $connect_again){
			$config = array(
					'host'=>$this->dbHost,
					'port'=>$this->dbPort,
					'username'=>$this->dbUser,
					'password'=>$this->dbPass,
					'dbname'=>$this->dbName,
 					'pdoType'=>$this->pdoType
			);
			try{
				$this->_mssql = Zend_Db::factory("Pdo_Mssql",$config);
				$this->_isConnected = true;
			} catch(PDOException $e){
				echo "Failed to get DB handle: " . $e->getMessage() . "\n";
			}
		}
	}
	
	/**
	 * closes a connection
	 */
	public function closeConnection(){
		if($this->_isConnected){
			$this->_mssql->closeConnection();
			$this->_isConnected = false;
		}
	}
	
	public function execute($sql = null){
		$this->connect();
		try{
			/** @var $stmt Zend_Db_Statement_Pdo **/
 			$stmt = $this->_mssql->prepare($sql);
 			$stmt->execute();
 			$this->_stmt = $stmt;
 			return $this->_stmt;
		}catch(PDOException $e){
			throw new Exception("\n".$e->getMessage()."\n");
		}
	}
	
	public function fetchAssoc(){
		//$this->_mssql->setFetchMode(Zend_DB::FETCH_ASSOC);
		return $this->_stmt->fetchAll(Zend_DB::FETCH_ASSOC);
	}
}