<?php
/*!
 * \file statement.class.php
 */

/// @cond TEST_CLASS_EXIST
if (!class_exists ('DB_MysqlStatement')) {
	/// @endcond


	/*!
 * \class DB_MysqlStatement
 * \brief mysql result manipulation functions
 *
 * abstraction layer for php's most frequest mysql result manipulation functions
 * is created upon the successfull retrieval of a result set by database.class.php
 */
	class DB_MysqlStatement {

		protected $_result;
		protected $_query;
		protected $_mysqli;

		/**
		 * constructor establishes passed query
		 * database object
		 * and throws exception if the database object is not a resource
		 */
		public function __construct(mysqli $mysqli, $query) {
			$this->_query = $query;
			$this->_mysqli = $mysqli;
		}

		protected function _getResult(){
			if (!$this->_result) {
				throw new MysqlException ('No result object to fetch from');
			}
			return $this->_result;
		}

		/**
		 * makes a successfull result set a property of this class
		 */
		public function set_result(mysqli_result $result) {
			$this->_result = $result;
		}

		/**
		 * wrapper for the mysql_fetch_row function that throws an exception if it has no result set to use
		 */
		public function fetch_row() {
			$result = $this->_getResult();
			return $result->fetch_row();
		}

		/**
		 * wrapper for the mysql_fetch_assoc function
		 */
		public function fetch_assoc() {
			$result = $this->_getResult();
			return $result->fetch_assoc();
		}

		/**
		 * uses the fetch_assoc wrapper to return an array populated with results
		 * abstraction of the db and result set from the calling code
		 */
		public function fetchall_assoc() {
			$retval = array ();
			while (null !== ($row = $this->fetch_assoc())) {
				$retval[] = $row;
			}
			return $retval;
		}

        /**
         * uses the fetch_row wrapper to return an array populated with results
         * abstraction of the db and result set from the calling code
         */
        public function fetchall_row() {
            $retval = array ();
            while (null !== ($row = $this->fetch_row())) {
                $retval[] = $row;
            }
            return $retval;
        }

		/**
		 * returns a populated array with result set
		 * used in conjunction with any interfaces that use a paging facility
		 * to allow app to page results....returns results to a designated offset with mysql_data_seek
		 */
		public function fetchseek_assoc($row_offset, $rows_on_page) {
			$retval = array ();
			$num_rows = $this->fetch_num_rows();
			if ($num_rows > 0) {
				$result = $this->_getResult();
				$result->data_seek($row_offset);
				for($rc = 0; $rc < $rows_on_page && ($row = $this->fetch_assoc()); $rc++) {
					$retval[] = $row;
				}
			}
			return $retval;
		}

		/**
		 * wrapper for mysql_num_rows function
		 */
		public function fetch_num_rows() {
			$result = $this->_getResult();
			return $result->num_rows;
		}

		/**
		 * wrapper for getting field names
		 */
		public function get_field_names() {
			$result = $this->_getResult();
			$retval = array();
			$i = 0;
			while ($i < $result->field_count) {
				$robj = $result->fetch_field_direct($i);
				$retval[] = $robj->name;
				$i++;
			}
			return $retval;
		}

		public function reset_pointer() {
			$result = $this->_getResult();
			$result->data_seek(0);
		}

	} // end of class


} // end of test if (!class_exists...)


?>