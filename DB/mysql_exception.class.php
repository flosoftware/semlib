<?php
/*!
 * \file mysql_exception.class.php
 */
/// @cond TEST_CLASS_EXIST
if (!class_exists('MysqlException', false)) {
	/// @endcond
	/*!
 * \class MysqlException
 *
 * @brief  class for mysql exception handling
 *
 * i.e. if mysql errors problem then writes then to the page
 * probably should change in production so dont get any mysql_error print outs
 * extends built in Exception class
 * */
	class MysqlException extends Exception {
		public $backtrace;

		public function __construct($message = "", $code = 0) {
			if ($message === "") {
				$message = mysql_error();
			}
			if ($code === 0) {
				$code = mysql_errno();
			}
			parent::__construct($message,$code);
			$this->backtrace = debug_backtrace();
		}
	} // end of class
} // end of test if (!class_exists...)
?>