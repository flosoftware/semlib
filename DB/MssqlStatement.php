<?php

//require_once 'Zend/DB/Statement/Pdo.php';


class DB_MssqlStatement extends Zend_Db_Statement_Pdo{
	
	protected $_result;
	
	/**
	 * Fetches a row from the result set.
	 *
	 * @param int $style  OPTIONAL Fetch mode for this fetch operation.
	 * @param int $cursor OPTIONAL Absolute, relative, or other.
	 * @param int $offset OPTIONAL Number for absolute or relative cursors.
	 * @return mixed Array, object, or scalar depending on fetch mode.
	 * @throws Zend_Db_Statement_Exception
	 */
	public function fetchAssoc($style = null, $cursor = null, $offset = null){
		return $this->fetch($style,$cursor, $offset);
	}
	
}